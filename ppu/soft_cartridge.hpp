#include <stdint.h>
#include <iostream>

using namespace std;

class Soft_cartridge {
public:
	Soft_cartridge();
	~Soft_cartridge();

	uint8_t read(uint16_t addr);

private:
	uint8_t *chr_rom; 

};
