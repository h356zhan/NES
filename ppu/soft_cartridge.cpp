#include "soft_cartridge.hpp"
#include <stdio.h>


Soft_cartridge::Soft_cartridge() {	
	FILE *f = fopen("rom.nes", "r");
	//fseek(f, , SEEK_SET);
	uint8_t header[16];
	fread(header, 1, 16, f);
	uint8_t flag6 = header[6];
	uint8_t trainer_size = (flag6 >> 2) & 0x1;
	uint8_t chr_size = header[5];
	uint8_t prg_size = header[4];

	fseek(f, 0, SEEK_SET);
	fseek(f, 16+(trainer_size==1?512: 0)+prg_size*16*1024, SEEK_SET);

	chr_rom = (uint8_t*)malloc(sizeof(uint8_t) * 8192*chr_size);

	fread(chr_rom, 1, 8192*chr_size, f);

	fclose(f);
}

Soft_cartridge::~Soft_cartridge() {
	free(chr_rom);
}

uint8_t Soft_cartridge::read(uint16_t addr) {
	return chr_rom[addr];
}

