verilator_flags=-Wno-CASEX -Wno-WIDTH -Wno-UNOPTFLAT -Wno-CASEINCOMPLETE
ppu_flags= -LDFLAGS -lSDL2

bus_sim: bus_ppu_ver
	make -j -C obj_dir -f Vbus.mk Vbus

bus_ppu_ver:
	verilator --cc bus.v mos6502.v rom_00.v --exe sim/bus_sim.cc ppu/ui.cpp ppu/ppu.cpp ppu/soft_cartridge.cpp $(verilator_flags) $(ppu_flags)

bus_ver:
	verilator --cc bus.v mos6502.v rom_00.v --exe sim/bus_sim.cc $(verilator_flags)

cpu_sim: cpu_ver
	make -j -C obj_dir -f Vmos6502.mk Vmos6502

cpu_ver:
	verilator --cc mos6502.v --exe sim/cpu_sim.cc $(verilator_flags)

.PHONY: clean 

clean:
	rm -rf obj_dir
