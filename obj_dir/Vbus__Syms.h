// Verilated -*- C++ -*-
// DESCRIPTION: Verilator output: Symbol table internal header
//
// Internal details; most calling programs do not need this header

#ifndef _Vbus__Syms_H_
#define _Vbus__Syms_H_

#include "verilated_heavy.h"

// INCLUDE MODULE CLASSES
#include "Vbus.h"

// SYMS CLASS
class Vbus__Syms : public VerilatedSyms {
  public:
    
    // LOCAL STATE
    const char* __Vm_namep;
    bool __Vm_didInit;
    
    // SUBCELL STATE
    Vbus*                          TOPp;
    
    // CREATORS
    Vbus__Syms(Vbus* topp, const char* namep);
    ~Vbus__Syms() {}
    
    // METHODS
    inline const char* name() { return __Vm_namep; }
    
} VL_ATTR_ALIGNED(64);

#endif // guard
