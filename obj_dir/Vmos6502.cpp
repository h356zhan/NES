// Verilated -*- C++ -*-
// DESCRIPTION: Verilator output: Design implementation internals
// See Vmos6502.h for the primary calling header

#include "Vmos6502.h"          // For This
#include "Vmos6502__Syms.h"


//--------------------
// STATIC VARIABLES

VL_ST_SIG8(Vmos6502::__Vtable1_mos6502__DOT__wr_reg[1024],0,0);

//--------------------

VL_CTOR_IMP(Vmos6502) {
    Vmos6502__Syms* __restrict vlSymsp = __VlSymsp = new Vmos6502__Syms(this, name());
    Vmos6502* __restrict vlTOPp VL_ATTR_UNUSED = vlSymsp->TOPp;
    // Reset internal values
    
    // Reset structure values
    _ctor_var_reset();
}

void Vmos6502::__Vconfigure(Vmos6502__Syms* vlSymsp, bool first) {
    if (0 && first) {}  // Prevent unused
    this->__VlSymsp = vlSymsp;
}

Vmos6502::~Vmos6502() {
    delete __VlSymsp; __VlSymsp=NULL;
}

//--------------------


void Vmos6502::eval() {
    VL_DEBUG_IF(VL_DBG_MSGF("+++++TOP Evaluate Vmos6502::eval\n"); );
    Vmos6502__Syms* __restrict vlSymsp = this->__VlSymsp;  // Setup global symbol table
    Vmos6502* __restrict vlTOPp VL_ATTR_UNUSED = vlSymsp->TOPp;
#ifdef VL_DEBUG
    // Debug assertions
    _eval_debug_assertions();
#endif // VL_DEBUG
    // Initialize
    if (VL_UNLIKELY(!vlSymsp->__Vm_didInit)) _eval_initial_loop(vlSymsp);
    // Evaluate till stable
    int __VclockLoop = 0;
    QData __Vchange = 1;
    do {
	VL_DEBUG_IF(VL_DBG_MSGF("+ Clock loop\n"););
	_eval(vlSymsp);
	if (VL_UNLIKELY(++__VclockLoop > 100)) {
	    // About to fail, so enable debug to see what's not settling.
	    // Note you must run make with OPT=-DVL_DEBUG for debug prints.
	    int __Vsaved_debug = Verilated::debug();
	    Verilated::debug(1);
	    __Vchange = _change_request(vlSymsp);
	    Verilated::debug(__Vsaved_debug);
	    VL_FATAL_MT(__FILE__,__LINE__,__FILE__,"Verilated model didn't converge");
	} else {
	    __Vchange = _change_request(vlSymsp);
	}
    } while (VL_UNLIKELY(__Vchange));
}

void Vmos6502::_eval_initial_loop(Vmos6502__Syms* __restrict vlSymsp) {
    vlSymsp->__Vm_didInit = true;
    _eval_initial(vlSymsp);
    // Evaluate till stable
    int __VclockLoop = 0;
    QData __Vchange = 1;
    do {
	_eval_settle(vlSymsp);
	_eval(vlSymsp);
	if (VL_UNLIKELY(++__VclockLoop > 100)) {
	    // About to fail, so enable debug to see what's not settling.
	    // Note you must run make with OPT=-DVL_DEBUG for debug prints.
	    int __Vsaved_debug = Verilated::debug();
	    Verilated::debug(1);
	    __Vchange = _change_request(vlSymsp);
	    Verilated::debug(__Vsaved_debug);
	    VL_FATAL_MT(__FILE__,__LINE__,__FILE__,"Verilated model didn't DC converge");
	} else {
	    __Vchange = _change_request(vlSymsp);
	}
    } while (VL_UNLIKELY(__Vchange));
}

//--------------------
// Internal Methods

VL_INLINE_OPT void Vmos6502::_sequent__TOP__1(Vmos6502__Syms* __restrict vlSymsp) {
    VL_DEBUG_IF(VL_DBG_MSGF("+    Vmos6502::_sequent__TOP__1\n"); );
    Vmos6502* __restrict vlTOPp VL_ATTR_UNUSED = vlSymsp->TOPp;
    // Variables
    VL_SIG8(__Vdly__mos6502__DOT__din_hold,7,0);
    VL_SIG8(__Vdly__mos6502__DOT__ir_hold_valid,0,0);
    VL_SIG8(__Vdly__mos6502__DOT__ir_hold,7,0);
    VL_SIG8(__Vdlyvdim0__mos6502__DOT__AXYS__v0,1,0);
    VL_SIG8(__Vdlyvval__mos6502__DOT__AXYS__v0,7,0);
    VL_SIG8(__Vdlyvset__mos6502__DOT__AXYS__v0,0,0);
    VL_SIG8(__Vdly__mos6502__DOT__adc_sbc,0,0);
    VL_SIG8(__Vdly__mos6502__DOT__D,0,0);
    // Body
    __Vdlyvset__mos6502__DOT__AXYS__v0 = 0U;
    vlTOPp->__Vdly__mos6502__DOT__nmi_hold = vlTOPp->mos6502__DOT__nmi_hold;
    vlTOPp->__Vdly__mos6502__DOT__nmi_edge = vlTOPp->mos6502__DOT__nmi_edge;
    __Vdly__mos6502__DOT__ir_hold = vlTOPp->mos6502__DOT__ir_hold;
    __Vdly__mos6502__DOT__ir_hold_valid = vlTOPp->mos6502__DOT__ir_hold_valid;
    __Vdly__mos6502__DOT__din_hold = vlTOPp->mos6502__DOT__din_hold;
    __Vdly__mos6502__DOT__adc_sbc = vlTOPp->mos6502__DOT__adc_sbc;
    __Vdly__mos6502__DOT__D = vlTOPp->mos6502__DOT__D;
    vlTOPp->__Vdly__mos6502__DOT__V = vlTOPp->mos6502__DOT__V;
    vlTOPp->__Vdly__mos6502__DOT__Z = vlTOPp->mos6502__DOT__Z;
    vlTOPp->__Vdly__mos6502__DOT__N = vlTOPp->mos6502__DOT__N;
    vlTOPp->__Vdly__mos6502__DOT__C = vlTOPp->mos6502__DOT__C;
    // ALWAYS at mos6502.v:161
    VL_WRITEF("--------------------\n");
    VL_WRITEF("State: %b\n",8,vlTOPp->mos6502__DOT__state);
    VL_WRITEF("ready: %b, irq: %b, nmi: %b, reset: %b\n",
	      1,vlTOPp->ready,1,(IData)(vlTOPp->irq),
	      1,vlTOPp->nmi,1,(IData)(vlTOPp->reset));
    VL_WRITEF("Data In: %x\n",8,vlTOPp->din);
    VL_WRITEF("wr_en: %b, addr: %x, Data Out: %x\n",
	      1,vlTOPp->wr_en,16,(IData)(vlTOPp->addr),
	      8,vlTOPp->dout);
    VL_WRITEF("PC: %x, IR: %x %b\n",16,vlTOPp->mos6502__DOT__pc,
	      8,(IData)(vlTOPp->mos6502__DOT__ir),8,
	      vlTOPp->mos6502__DOT__ir);
    VL_WRITEF("A: %x, X: %x, Y: %x, S: %x\n",8,vlTOPp->mos6502__DOT__AXYS
	      [0U],8,vlTOPp->mos6502__DOT__AXYS[1U],
	      8,vlTOPp->mos6502__DOT__AXYS[2U],8,vlTOPp->mos6502__DOT__AXYS
	      [3U]);
    VL_WRITEF("====================\n");
    // ALWAYS at mos6502/data_in.v:1
    if (vlTOPp->ready) {
	__Vdly__mos6502__DOT__din_hold = vlTOPp->din;
    }
    if (vlTOPp->reset) {
	__Vdly__mos6502__DOT__ir_hold_valid = 0U;
    } else {
	if (vlTOPp->ready) {
	    if (((0x98U == (IData)(vlTOPp->mos6502__DOT__state)) 
		 | (0x90U == (IData)(vlTOPp->mos6502__DOT__state)))) {
		__Vdly__mos6502__DOT__ir_hold = vlTOPp->mos6502__DOT__din_mux;
		__Vdly__mos6502__DOT__ir_hold_valid = 1U;
	    } else {
		if ((8U == (IData)(vlTOPp->mos6502__DOT__state))) {
		    __Vdly__mos6502__DOT__ir_hold_valid = 0U;
		}
	    }
	}
    }
    VL_WRITEF("%b %b %b %b %x %x\n",1,vlTOPp->mos6502__DOT__I,
	      1,(IData)(vlTOPp->irq),1,vlTOPp->mos6502__DOT__nmi_edge,
	      1,(IData)(vlTOPp->mos6502__DOT__ir_hold_valid),
	      8,vlTOPp->mos6502__DOT__ir_hold,8,(IData)(vlTOPp->mos6502__DOT__din_mux));
    VL_WRITEF("%x = %b %x %x\n",8,vlTOPp->mos6502__DOT__din_mux,
	      1,(IData)(vlTOPp->ready),8,vlTOPp->din,
	      8,(IData)(vlTOPp->mos6502__DOT__din_hold));
    // ALWAYS at mos6502/alu_control.v:4
    vlTOPp->mos6502__DOT__adj_bcd = ((IData)(vlTOPp->mos6502__DOT__adc_sbc) 
				     & (IData)(vlTOPp->mos6502__DOT__D));
    if (((IData)(vlTOPp->ready) & ((8U == (IData)(vlTOPp->mos6502__DOT__state)) 
				   | (0x60U == (IData)(vlTOPp->mos6502__DOT__state))))) {
	__Vdly__mos6502__DOT__adc_sbc = (0x61U == (0x63U 
						   & (IData)(vlTOPp->mos6502__DOT__ir)));
    }
    if (((IData)(vlTOPp->ready) & ((8U == (IData)(vlTOPp->mos6502__DOT__state)) 
				   | (0x60U == (IData)(vlTOPp->mos6502__DOT__state))))) {
	vlTOPp->mos6502__DOT__adc_bcd = ((0x61U == 
					  (0xe3U & (IData)(vlTOPp->mos6502__DOT__ir))) 
					 & (IData)(vlTOPp->mos6502__DOT__D));
    }
    if (((IData)(vlTOPp->ready) & (8U == (IData)(vlTOPp->mos6502__DOT__state)))) {
	vlTOPp->mos6502__DOT__shift_right = (0x42U 
					     == (0xc3U 
						 & (IData)(vlTOPp->mos6502__DOT__ir)));
    }
    if ((0x20U == (IData)(vlTOPp->mos6502__DOT__state))) {
	vlTOPp->__Vdly__mos6502__DOT__N = vlTOPp->mos6502__DOT__alu_N;
	vlTOPp->__Vdly__mos6502__DOT__Z = (1U & (~ (IData)(
							   (0U 
							    != (IData)(vlTOPp->mos6502__DOT__alu_out)))));
	if (vlTOPp->mos6502__DOT__shift) {
	    vlTOPp->__Vdly__mos6502__DOT__C = vlTOPp->mos6502__DOT__carry_out;
	}
    } else {
	if ((0x82U == (IData)(vlTOPp->mos6502__DOT__state))) {
	    vlTOPp->__Vdly__mos6502__DOT__N = (1U & 
					       ((IData)(vlTOPp->mos6502__DOT__din_mux) 
						>> 7U));
	    vlTOPp->__Vdly__mos6502__DOT__V = (1U & 
					       ((IData)(vlTOPp->mos6502__DOT__din_mux) 
						>> 6U));
	    __Vdly__mos6502__DOT__D = (1U & ((IData)(vlTOPp->mos6502__DOT__din_mux) 
					     >> 3U));
	    vlTOPp->__Vdly__mos6502__DOT__Z = (1U & 
					       ((IData)(vlTOPp->mos6502__DOT__din_mux) 
						>> 1U));
	    vlTOPp->__Vdly__mos6502__DOT__C = (1U & (IData)(vlTOPp->mos6502__DOT__din_mux));
	} else {
	    if ((8U == (IData)(vlTOPp->mos6502__DOT__state))) {
		if (vlTOPp->mos6502__DOT__adc_sbc) {
		    vlTOPp->__Vdly__mos6502__DOT__V 
			= (1U & (((((IData)(vlTOPp->mos6502__DOT__alu_A) 
				    ^ (IData)(vlTOPp->mos6502__DOT__alu_B)) 
				   >> 7U) ^ (IData)(vlTOPp->mos6502__DOT__carry_out)) 
				 ^ (IData)(vlTOPp->mos6502__DOT__alu_N)));
		}
		if (vlTOPp->mos6502__DOT__clv) {
		    vlTOPp->__Vdly__mos6502__DOT__V = 0U;
		}
		if (vlTOPp->mos6502__DOT__sed) {
		    __Vdly__mos6502__DOT__D = 1U;
		}
		if (vlTOPp->mos6502__DOT__cld) {
		    __Vdly__mos6502__DOT__D = 0U;
		}
		if (vlTOPp->mos6502__DOT__plp) {
		    vlTOPp->__Vdly__mos6502__DOT__N 
			= (1U & ((IData)(vlTOPp->mos6502__DOT__alu_out) 
				 >> 7U));
		    vlTOPp->__Vdly__mos6502__DOT__V 
			= (1U & ((IData)(vlTOPp->mos6502__DOT__alu_out) 
				 >> 6U));
		    __Vdly__mos6502__DOT__D = (1U & 
					       ((IData)(vlTOPp->mos6502__DOT__alu_out) 
						>> 3U));
		    vlTOPp->__Vdly__mos6502__DOT__Z 
			= (1U & ((IData)(vlTOPp->mos6502__DOT__alu_out) 
				 >> 1U));
		}
		if ((((IData)(vlTOPp->mos6502__DOT__ld_reg) 
		      & (3U != (IData)(vlTOPp->mos6502__DOT__reg_idx))) 
		     | (IData)(vlTOPp->mos6502__DOT__compare))) {
		    vlTOPp->__Vdly__mos6502__DOT__N 
			= vlTOPp->mos6502__DOT__alu_N;
		}
		if (((((IData)(vlTOPp->mos6502__DOT__ld_reg) 
		       & (3U != (IData)(vlTOPp->mos6502__DOT__reg_idx))) 
		      | (IData)(vlTOPp->mos6502__DOT__compare)) 
		     | (IData)(vlTOPp->mos6502__DOT__bit_ins))) {
		    vlTOPp->__Vdly__mos6502__DOT__Z 
			= (1U & (~ (IData)((0U != (IData)(vlTOPp->mos6502__DOT__alu_out)))));
		}
		if ((1U & (~ (IData)(vlTOPp->mos6502__DOT__write_back)))) {
		    if ((((IData)(vlTOPp->mos6502__DOT__adc_sbc) 
			  | (IData)(vlTOPp->mos6502__DOT__shift)) 
			 | (IData)(vlTOPp->mos6502__DOT__compare))) {
			vlTOPp->__Vdly__mos6502__DOT__C 
			    = vlTOPp->mos6502__DOT__carry_out;
		    } else {
			if (vlTOPp->mos6502__DOT__plp) {
			    vlTOPp->__Vdly__mos6502__DOT__C 
				= (1U & (IData)(vlTOPp->mos6502__DOT__alu_out));
			} else {
			    if (vlTOPp->mos6502__DOT__sec) {
				vlTOPp->__Vdly__mos6502__DOT__C = 1U;
			    } else {
				if (vlTOPp->mos6502__DOT__clc) {
				    vlTOPp->__Vdly__mos6502__DOT__C = 0U;
				}
			    }
			}
		    }
		}
	    } else {
		if ((0U == (IData)(vlTOPp->mos6502__DOT__state))) {
		    if (vlTOPp->mos6502__DOT__bit_ins) {
			vlTOPp->__Vdly__mos6502__DOT__N 
			    = (1U & ((IData)(vlTOPp->mos6502__DOT__din_mux) 
				     >> 7U));
			vlTOPp->__Vdly__mos6502__DOT__V 
			    = (1U & ((IData)(vlTOPp->mos6502__DOT__din_mux) 
				     >> 6U));
		    }
		}
	    }
	}
    }
    vlTOPp->mos6502__DOT__ir_hold_valid = __Vdly__mos6502__DOT__ir_hold_valid;
    vlTOPp->mos6502__DOT__ir_hold = __Vdly__mos6502__DOT__ir_hold;
    vlTOPp->mos6502__DOT__din_hold = __Vdly__mos6502__DOT__din_hold;
    vlTOPp->mos6502__DOT__adc_sbc = __Vdly__mos6502__DOT__adc_sbc;
    vlTOPp->mos6502__DOT__D = __Vdly__mos6502__DOT__D;
    // ALWAYS at mos6502/pc.v:45
    if (vlTOPp->ready) {
	vlTOPp->mos6502__DOT__pc = (0xffffU & ((IData)(vlTOPp->mos6502__DOT__pc_mux) 
					       + (IData)(vlTOPp->mos6502__DOT__pc_inc)));
    }
    // ALWAYS at mos6502/alu_control.v:4
    if ((0x63U == (IData)(vlTOPp->mos6502__DOT__state))) {
	vlTOPp->mos6502__DOT__I = 1U;
    } else {
	if ((0x82U == (IData)(vlTOPp->mos6502__DOT__state))) {
	    vlTOPp->mos6502__DOT__I = (1U & ((IData)(vlTOPp->mos6502__DOT__din_mux) 
					     >> 2U));
	} else {
	    if ((0x10U == (IData)(vlTOPp->mos6502__DOT__state))) {
		if (vlTOPp->mos6502__DOT__sei) {
		    vlTOPp->mos6502__DOT__I = 1U;
		}
		if (vlTOPp->mos6502__DOT__cli) {
		    vlTOPp->mos6502__DOT__I = 0U;
		}
	    } else {
		if ((8U == (IData)(vlTOPp->mos6502__DOT__state))) {
		    if (vlTOPp->mos6502__DOT__plp) {
			vlTOPp->mos6502__DOT__I = (1U 
						   & ((IData)(vlTOPp->mos6502__DOT__alu_out) 
						      >> 2U));
		    }
		}
	    }
	}
    }
    // ALWAYS at mos6502/register.v:1
    if (((IData)(vlTOPp->ready) & (IData)(vlTOPp->mos6502__DOT__wr_reg))) {
	__Vdlyvval__mos6502__DOT__AXYS__v0 = ((0x68U 
					       == (IData)(vlTOPp->mos6502__DOT__state))
					       ? (IData)(vlTOPp->mos6502__DOT__din_mux)
					       : ((0xf0U 
						   & ((((IData)(vlTOPp->mos6502__DOT__alu_out) 
							>> 4U) 
						       + (IData)(vlTOPp->mos6502__DOT__adj)) 
						      << 4U)) 
						  | (0xfU 
						     & ((IData)(vlTOPp->mos6502__DOT__alu_out) 
							+ (IData)(vlTOPp->mos6502__DOT__adj)))));
	__Vdlyvset__mos6502__DOT__AXYS__v0 = 1U;
	__Vdlyvdim0__mos6502__DOT__AXYS__v0 = vlTOPp->mos6502__DOT__reg_idx;
    }
    if (((IData)(vlTOPp->ready) & (8U == (IData)(vlTOPp->mos6502__DOT__state)))) {
	vlTOPp->mos6502__DOT__ld_reg = (1U & ((0x80U 
					       & (IData)(vlTOPp->mos6502__DOT__ir))
					       ? ((0x40U 
						   & (IData)(vlTOPp->mos6502__DOT__ir))
						   ? 
						  ((0x20U 
						    & (IData)(vlTOPp->mos6502__DOT__ir))
						    ? 
						   ((0x10U 
						     & (IData)(vlTOPp->mos6502__DOT__ir))
						     ? 
						    ((~ 
						      ((IData)(vlTOPp->mos6502__DOT__ir) 
						       >> 1U)) 
						     & (IData)(vlTOPp->mos6502__DOT__ir))
						     : 
						    ((8U 
						      & (IData)(vlTOPp->mos6502__DOT__ir))
						      ? 
						     ((4U 
						       & (IData)(vlTOPp->mos6502__DOT__ir))
						       ? 
						      ((~ 
							((IData)(vlTOPp->mos6502__DOT__ir) 
							 >> 1U)) 
						       & (IData)(vlTOPp->mos6502__DOT__ir))
						       : 
						      (~ 
						       ((IData)(vlTOPp->mos6502__DOT__ir) 
							>> 1U)))
						      : 
						     ((~ 
						       ((IData)(vlTOPp->mos6502__DOT__ir) 
							>> 1U)) 
						      & (IData)(vlTOPp->mos6502__DOT__ir))))
						    : 
						   ((~ 
						     ((IData)(vlTOPp->mos6502__DOT__ir) 
						      >> 4U)) 
						    & (((IData)(vlTOPp->mos6502__DOT__ir) 
							>> 3U) 
						       & ((~ 
							   ((IData)(vlTOPp->mos6502__DOT__ir) 
							    >> 2U)) 
							  & (~ (IData)(vlTOPp->mos6502__DOT__ir))))))
						   : 
						  ((0x20U 
						    & (IData)(vlTOPp->mos6502__DOT__ir))
						    ? 
						   ((0x10U 
						     & (IData)(vlTOPp->mos6502__DOT__ir))
						     ? 
						    ((8U 
						      & (IData)(vlTOPp->mos6502__DOT__ir))
						      ? 
						     ((4U 
						       & (IData)(vlTOPp->mos6502__DOT__ir))
						       ? 
						      ((~ 
							((IData)(vlTOPp->mos6502__DOT__ir) 
							 >> 1U)) 
						       | (~ (IData)(vlTOPp->mos6502__DOT__ir)))
						       : 
						      ((2U 
							& (IData)(vlTOPp->mos6502__DOT__ir))
						        ? 
						       (~ (IData)(vlTOPp->mos6502__DOT__ir))
						        : (IData)(vlTOPp->mos6502__DOT__ir)))
						      : 
						     ((4U 
						       & (IData)(vlTOPp->mos6502__DOT__ir))
						       ? 
						      ((~ 
							((IData)(vlTOPp->mos6502__DOT__ir) 
							 >> 1U)) 
						       | (~ (IData)(vlTOPp->mos6502__DOT__ir)))
						       : 
						      ((~ 
							((IData)(vlTOPp->mos6502__DOT__ir) 
							 >> 1U)) 
						       & (IData)(vlTOPp->mos6502__DOT__ir))))
						     : 
						    ((~ 
						      ((IData)(vlTOPp->mos6502__DOT__ir) 
						       >> 1U)) 
						     | (~ (IData)(vlTOPp->mos6502__DOT__ir))))
						    : 
						   (((IData)(vlTOPp->mos6502__DOT__ir) 
						     >> 3U) 
						    & ((~ 
							((IData)(vlTOPp->mos6502__DOT__ir) 
							 >> 2U)) 
						       & (~ (IData)(vlTOPp->mos6502__DOT__ir))))))
					       : ((0x10U 
						   & (IData)(vlTOPp->mos6502__DOT__ir))
						   ? 
						  ((~ 
						    ((IData)(vlTOPp->mos6502__DOT__ir) 
						     >> 1U)) 
						   & (IData)(vlTOPp->mos6502__DOT__ir))
						   : 
						  ((8U 
						    & (IData)(vlTOPp->mos6502__DOT__ir))
						    ? 
						   ((4U 
						     & (IData)(vlTOPp->mos6502__DOT__ir))
						     ? 
						    ((~ 
						      ((IData)(vlTOPp->mos6502__DOT__ir) 
						       >> 1U)) 
						     & (IData)(vlTOPp->mos6502__DOT__ir))
						     : 
						    ((~ 
						      ((IData)(vlTOPp->mos6502__DOT__ir) 
						       >> 1U)) 
						     | (~ (IData)(vlTOPp->mos6502__DOT__ir))))
						    : 
						   ((~ 
						     ((IData)(vlTOPp->mos6502__DOT__ir) 
						      >> 1U)) 
						    & (IData)(vlTOPp->mos6502__DOT__ir))))));
    }
    // ALWAYSPOST at mos6502/register.v:3
    if (__Vdlyvset__mos6502__DOT__AXYS__v0) {
	vlTOPp->mos6502__DOT__AXYS[__Vdlyvdim0__mos6502__DOT__AXYS__v0] 
	    = __Vdlyvval__mos6502__DOT__AXYS__v0;
    }
}

VL_INLINE_OPT void Vmos6502::_sequent__TOP__2(Vmos6502__Syms* __restrict vlSymsp) {
    VL_DEBUG_IF(VL_DBG_MSGF("+    Vmos6502::_sequent__TOP__2\n"); );
    Vmos6502* __restrict vlTOPp VL_ATTR_UNUSED = vlSymsp->TOPp;
    // Body
    vlTOPp->__Vdly__mos6502__DOT__state = vlTOPp->mos6502__DOT__state;
    // ALWAYS at mos6502/state_machine.v:1
    if (vlTOPp->reset) {
	vlTOPp->__Vdly__mos6502__DOT__state = 0x60U;
    } else {
	if (vlTOPp->ready) {
	    vlTOPp->__Vdly__mos6502__DOT__state = (
						   (0x80U 
						    & (IData)(vlTOPp->mos6502__DOT__state))
						    ? 
						   ((0x40U 
						     & (IData)(vlTOPp->mos6502__DOT__state))
						     ? 0x60U
						     : 
						    ((0x20U 
						      & (IData)(vlTOPp->mos6502__DOT__state))
						      ? 0x60U
						      : 
						     ((0x10U 
						       & (IData)(vlTOPp->mos6502__DOT__state))
						       ? 
						      ((8U 
							& (IData)(vlTOPp->mos6502__DOT__state))
						        ? 
						       ((4U 
							 & (IData)(vlTOPp->mos6502__DOT__state))
							 ? 0x60U
							 : 
							((2U 
							  & (IData)(vlTOPp->mos6502__DOT__state))
							  ? 
							 ((1U 
							   & (IData)(vlTOPp->mos6502__DOT__state))
							   ? 0x60U
							   : 8U)
							  : 
							 ((1U 
							   & (IData)(vlTOPp->mos6502__DOT__state))
							   ? 0x9aU
							   : 0x99U)))
						        : 
						       ((4U 
							 & (IData)(vlTOPp->mos6502__DOT__state))
							 ? 0x60U
							 : 
							((2U 
							  & (IData)(vlTOPp->mos6502__DOT__state))
							  ? 0x60U
							  : 
							 ((1U 
							   & (IData)(vlTOPp->mos6502__DOT__state))
							   ? 8U
							   : 0x91U))))
						       : 
						      ((8U 
							& (IData)(vlTOPp->mos6502__DOT__state))
						        ? 
						       ((4U 
							 & (IData)(vlTOPp->mos6502__DOT__state))
							 ? 0x60U
							 : 
							((2U 
							  & (IData)(vlTOPp->mos6502__DOT__state))
							  ? 
							 ((1U 
							   & (IData)(vlTOPp->mos6502__DOT__state))
							   ? 0U
							   : 0x8bU)
							  : 
							 ((1U 
							   & (IData)(vlTOPp->mos6502__DOT__state))
							   ? 0x8aU
							   : 0x89U)))
						        : 
						       ((4U 
							 & (IData)(vlTOPp->mos6502__DOT__state))
							 ? 
							((2U 
							  & (IData)(vlTOPp->mos6502__DOT__state))
							  ? 0x60U
							  : 
							 ((1U 
							   & (IData)(vlTOPp->mos6502__DOT__state))
							   ? 0x60U
							   : 8U))
							 : 
							((2U 
							  & (IData)(vlTOPp->mos6502__DOT__state))
							  ? 
							 ((1U 
							   & (IData)(vlTOPp->mos6502__DOT__state))
							   ? 0x84U
							   : 0x83U)
							  : 
							 ((1U 
							   & (IData)(vlTOPp->mos6502__DOT__state))
							   ? 0x82U
							   : 0x81U)))))))
						    : 
						   ((0x40U 
						     & (IData)(vlTOPp->mos6502__DOT__state))
						     ? 
						    ((0x20U 
						      & (IData)(vlTOPp->mos6502__DOT__state))
						      ? 
						     ((0x10U 
						       & (IData)(vlTOPp->mos6502__DOT__state))
						       ? 
						      ((8U 
							& (IData)(vlTOPp->mos6502__DOT__state))
						        ? 
						       ((4U 
							 & (IData)(vlTOPp->mos6502__DOT__state))
							 ? 0x60U
							 : 
							((2U 
							  & (IData)(vlTOPp->mos6502__DOT__state))
							  ? 0x60U
							  : 
							 ((1U 
							   & (IData)(vlTOPp->mos6502__DOT__state))
							   ? 0x70U
							   : 0x79U)))
						        : 
						       ((4U 
							 & (IData)(vlTOPp->mos6502__DOT__state))
							 ? 0x60U
							 : 
							((2U 
							  & (IData)(vlTOPp->mos6502__DOT__state))
							  ? 0x60U
							  : 
							 ((1U 
							   & (IData)(vlTOPp->mos6502__DOT__state))
							   ? 8U
							   : 0x71U))))
						       : 
						      ((8U 
							& (IData)(vlTOPp->mos6502__DOT__state))
						        ? 
						       ((4U 
							 & (IData)(vlTOPp->mos6502__DOT__state))
							 ? 0x60U
							 : 
							((2U 
							  & (IData)(vlTOPp->mos6502__DOT__state))
							  ? 
							 ((1U 
							   & (IData)(vlTOPp->mos6502__DOT__state))
							   ? 0U
							   : 0x6bU)
							  : 
							 ((1U 
							   & (IData)(vlTOPp->mos6502__DOT__state))
							   ? 0x6aU
							   : 0x69U)))
						        : 
						       ((4U 
							 & (IData)(vlTOPp->mos6502__DOT__state))
							 ? 0x60U
							 : 
							((2U 
							  & (IData)(vlTOPp->mos6502__DOT__state))
							  ? 
							 ((1U 
							   & (IData)(vlTOPp->mos6502__DOT__state))
							   ? 0x70U
							   : 0x63U)
							  : 
							 ((1U 
							   & (IData)(vlTOPp->mos6502__DOT__state))
							   ? 0x62U
							   : 0x61U)))))
						      : 
						     ((0x10U 
						       & (IData)(vlTOPp->mos6502__DOT__state))
						       ? 
						      ((8U 
							& (IData)(vlTOPp->mos6502__DOT__state))
						        ? 
						       ((4U 
							 & (IData)(vlTOPp->mos6502__DOT__state))
							 ? 0x60U
							 : 
							((2U 
							  & (IData)(vlTOPp->mos6502__DOT__state))
							  ? 
							 ((1U 
							   & (IData)(vlTOPp->mos6502__DOT__state))
							   ? 0x60U
							   : 8U)
							  : 
							 ((1U 
							   & (IData)(vlTOPp->mos6502__DOT__state))
							   ? 
							  (((IData)(vlTOPp->mos6502__DOT__carry_out) 
							    ^ (IData)(vlTOPp->mos6502__DOT__backwards))
							    ? 0x5aU
							    : 8U)
							   : 
							  ((IData)(vlTOPp->mos6502__DOT__cond_true)
							    ? 0x59U
							    : 8U))))
						        : 
						       ((4U 
							 & (IData)(vlTOPp->mos6502__DOT__state))
							 ? 0x60U
							 : 
							((2U 
							  & (IData)(vlTOPp->mos6502__DOT__state))
							  ? 0x60U
							  : 
							 ((1U 
							   & (IData)(vlTOPp->mos6502__DOT__state))
							   ? 
							  ((IData)(vlTOPp->mos6502__DOT__write_back)
							    ? 0x18U
							    : 0U)
							   : 0x51U))))
						       : 
						      ((8U 
							& (IData)(vlTOPp->mos6502__DOT__state))
						        ? 
						       ((4U 
							 & (IData)(vlTOPp->mos6502__DOT__state))
							 ? 0x60U
							 : 
							((2U 
							  & (IData)(vlTOPp->mos6502__DOT__state))
							  ? 0x60U
							  : 
							 ((1U 
							   & (IData)(vlTOPp->mos6502__DOT__state))
							   ? 0x60U
							   : 
							  ((IData)(vlTOPp->mos6502__DOT__write_back)
							    ? 0x18U
							    : 0U))))
						        : 
						       ((4U 
							 & (IData)(vlTOPp->mos6502__DOT__state))
							 ? 0x60U
							 : 
							((2U 
							  & (IData)(vlTOPp->mos6502__DOT__state))
							  ? 
							 ((1U 
							   & (IData)(vlTOPp->mos6502__DOT__state))
							   ? 0U
							   : 
							  (((IData)(vlTOPp->mos6502__DOT__carry_out) 
							    | (IData)(vlTOPp->mos6502__DOT__store))
							    ? 0x43U
							    : 0U))
							  : 
							 ((1U 
							   & (IData)(vlTOPp->mos6502__DOT__state))
							   ? 0x42U
							   : 0x41U))))))
						     : 
						    ((0x20U 
						      & (IData)(vlTOPp->mos6502__DOT__state))
						      ? 
						     ((0x10U 
						       & (IData)(vlTOPp->mos6502__DOT__state))
						       ? 
						      ((8U 
							& (IData)(vlTOPp->mos6502__DOT__state))
						        ? 
						       ((4U 
							 & (IData)(vlTOPp->mos6502__DOT__state))
							 ? 0x60U
							 : 
							((2U 
							  & (IData)(vlTOPp->mos6502__DOT__state))
							  ? 
							 ((1U 
							   & (IData)(vlTOPp->mos6502__DOT__state))
							   ? 0U
							   : 0x3bU)
							  : 
							 ((1U 
							   & (IData)(vlTOPp->mos6502__DOT__state))
							   ? 0x3aU
							   : 0x39U)))
						        : 
						       ((4U 
							 & (IData)(vlTOPp->mos6502__DOT__state))
							 ? 0x60U
							 : 
							((2U 
							  & (IData)(vlTOPp->mos6502__DOT__state))
							  ? 
							 ((1U 
							   & (IData)(vlTOPp->mos6502__DOT__state))
							   ? 0x60U
							   : 
							  ((IData)(vlTOPp->mos6502__DOT__write_back)
							    ? 0x18U
							    : 0U))
							  : 
							 ((1U 
							   & (IData)(vlTOPp->mos6502__DOT__state))
							   ? 
							  ((((IData)(vlTOPp->mos6502__DOT__carry_out) 
							     | (IData)(vlTOPp->mos6502__DOT__store)) 
							    | (IData)(vlTOPp->mos6502__DOT__write_back))
							    ? 0x32U
							    : 0U)
							   : 0x31U))))
						       : 
						      ((8U 
							& (IData)(vlTOPp->mos6502__DOT__state))
						        ? 
						       ((4U 
							 & (IData)(vlTOPp->mos6502__DOT__state))
							 ? 0x60U
							 : 
							((2U 
							  & (IData)(vlTOPp->mos6502__DOT__state))
							  ? 0x60U
							  : 
							 ((1U 
							   & (IData)(vlTOPp->mos6502__DOT__state))
							   ? 
							  ((IData)(vlTOPp->mos6502__DOT__write_back)
							    ? 0x18U
							    : 0U)
							   : 0x29U)))
						        : 
						       ((4U 
							 & (IData)(vlTOPp->mos6502__DOT__state))
							 ? 0x60U
							 : 
							((2U 
							  & (IData)(vlTOPp->mos6502__DOT__state))
							  ? 0x60U
							  : 
							 ((1U 
							   & (IData)(vlTOPp->mos6502__DOT__state))
							   ? 0x60U
							   : 0U)))))
						      : 
						     ((0x10U 
						       & (IData)(vlTOPp->mos6502__DOT__state))
						       ? 
						      ((8U 
							& (IData)(vlTOPp->mos6502__DOT__state))
						        ? 
						       ((4U 
							 & (IData)(vlTOPp->mos6502__DOT__state))
							 ? 0x60U
							 : 
							((2U 
							  & (IData)(vlTOPp->mos6502__DOT__state))
							  ? 0x60U
							  : 
							 ((1U 
							   & (IData)(vlTOPp->mos6502__DOT__state))
							   ? 0x60U
							   : 0x20U)))
						        : 
						       ((4U 
							 & (IData)(vlTOPp->mos6502__DOT__state))
							 ? 0x60U
							 : 
							((2U 
							  & (IData)(vlTOPp->mos6502__DOT__state))
							  ? 0x60U
							  : 
							 ((1U 
							   & (IData)(vlTOPp->mos6502__DOT__state))
							   ? 0x60U
							   : 8U))))
						       : 
						      ((8U 
							& (IData)(vlTOPp->mos6502__DOT__state))
						        ? 
						       ((4U 
							 & (IData)(vlTOPp->mos6502__DOT__state))
							 ? 0x60U
							 : 
							((2U 
							  & (IData)(vlTOPp->mos6502__DOT__state))
							  ? 0x60U
							  : 
							 ((1U 
							   & (IData)(vlTOPp->mos6502__DOT__state))
							   ? 0x60U
							   : 
							  ((0x80U 
							    & (IData)(vlTOPp->mos6502__DOT__ir))
							    ? 
							   ((0x10U 
							     & (IData)(vlTOPp->mos6502__DOT__ir))
							     ? 
							    ((8U 
							      & (IData)(vlTOPp->mos6502__DOT__ir))
							      ? 
							     ((4U 
							       & (IData)(vlTOPp->mos6502__DOT__ir))
							       ? 0x30U
							       : 
							      ((2U 
								& (IData)(vlTOPp->mos6502__DOT__ir))
							        ? 
							       ((1U 
								 & (IData)(vlTOPp->mos6502__DOT__ir))
								 ? 0x60U
								 : 0x10U)
							        : 
							       ((1U 
								 & (IData)(vlTOPp->mos6502__DOT__ir))
								 ? 0x30U
								 : 0x10U)))
							      : 
							     ((4U 
							       & (IData)(vlTOPp->mos6502__DOT__ir))
							       ? 0x50U
							       : 
							      ((2U 
								& (IData)(vlTOPp->mos6502__DOT__ir))
							        ? 0x60U
							        : 
							       ((1U 
								 & (IData)(vlTOPp->mos6502__DOT__ir))
								 ? 0x40U
								 : 0x58U))))
							     : 
							    ((8U 
							      & (IData)(vlTOPp->mos6502__DOT__ir))
							      ? 
							     ((4U 
							       & (IData)(vlTOPp->mos6502__DOT__ir))
							       ? 
							      ((2U 
								& (IData)(vlTOPp->mos6502__DOT__ir))
							        ? 
							       ((1U 
								 & (IData)(vlTOPp->mos6502__DOT__ir))
								 ? 0x60U
								 : 0x28U)
							        : 0x28U)
							       : 
							      ((2U 
								& (IData)(vlTOPp->mos6502__DOT__ir))
							        ? 
							       ((1U 
								 & (IData)(vlTOPp->mos6502__DOT__ir))
								 ? 0x60U
								 : 0x10U)
							        : 
							       ((1U 
								 & (IData)(vlTOPp->mos6502__DOT__ir))
								 ? 0U
								 : 0x10U)))
							      : 
							     ((4U 
							       & (IData)(vlTOPp->mos6502__DOT__ir))
							       ? 0x48U
							       : 
							      ((2U 
								& (IData)(vlTOPp->mos6502__DOT__ir))
							        ? 
							       ((1U 
								 & (IData)(vlTOPp->mos6502__DOT__ir))
								 ? 0x60U
								 : 0U)
							        : 
							       ((1U 
								 & (IData)(vlTOPp->mos6502__DOT__ir))
								 ? 0x38U
								 : 0U)))))
							    : 
							   ((0x40U 
							     & (IData)(vlTOPp->mos6502__DOT__ir))
							     ? 
							    ((0x20U 
							      & (IData)(vlTOPp->mos6502__DOT__ir))
							      ? 
							     ((0x10U 
							       & (IData)(vlTOPp->mos6502__DOT__ir))
							       ? 
							      ((8U 
								& (IData)(vlTOPp->mos6502__DOT__ir))
							        ? 
							       ((4U 
								 & (IData)(vlTOPp->mos6502__DOT__ir))
								 ? 0x30U
								 : 
								((2U 
								  & (IData)(vlTOPp->mos6502__DOT__ir))
								  ? 
								 ((1U 
								   & (IData)(vlTOPp->mos6502__DOT__ir))
								   ? 0x60U
								   : 0x10U)
								  : 
								 ((1U 
								   & (IData)(vlTOPp->mos6502__DOT__ir))
								   ? 0x30U
								   : 0x10U)))
							        : 
							       ((4U 
								 & (IData)(vlTOPp->mos6502__DOT__ir))
								 ? 0x50U
								 : 
								((2U 
								  & (IData)(vlTOPp->mos6502__DOT__ir))
								  ? 0x60U
								  : 
								 ((1U 
								   & (IData)(vlTOPp->mos6502__DOT__ir))
								   ? 0x40U
								   : 0x58U))))
							       : 
							      ((8U 
								& (IData)(vlTOPp->mos6502__DOT__ir))
							        ? 
							       ((4U 
								 & (IData)(vlTOPp->mos6502__DOT__ir))
								 ? 
								((2U 
								  & (IData)(vlTOPp->mos6502__DOT__ir))
								  ? 
								 ((1U 
								   & (IData)(vlTOPp->mos6502__DOT__ir))
								   ? 0x60U
								   : 0x28U)
								  : 
								 ((1U 
								   & (IData)(vlTOPp->mos6502__DOT__ir))
								   ? 0x28U
								   : 0x78U))
								 : 
								((2U 
								  & (IData)(vlTOPp->mos6502__DOT__ir))
								  ? 
								 ((1U 
								   & (IData)(vlTOPp->mos6502__DOT__ir))
								   ? 0x60U
								   : 0x10U)
								  : 
								 ((1U 
								   & (IData)(vlTOPp->mos6502__DOT__ir))
								   ? 0U
								   : 0x98U)))
							        : 
							       ((4U 
								 & (IData)(vlTOPp->mos6502__DOT__ir))
								 ? 0x48U
								 : 
								((2U 
								  & (IData)(vlTOPp->mos6502__DOT__ir))
								  ? 0x60U
								  : 
								 ((1U 
								   & (IData)(vlTOPp->mos6502__DOT__ir))
								   ? 0x38U
								   : 0x88U)))))
							      : 
							     ((0x10U 
							       & (IData)(vlTOPp->mos6502__DOT__ir))
							       ? 
							      ((8U 
								& (IData)(vlTOPp->mos6502__DOT__ir))
							        ? 
							       ((4U 
								 & (IData)(vlTOPp->mos6502__DOT__ir))
								 ? 0x30U
								 : 
								((2U 
								  & (IData)(vlTOPp->mos6502__DOT__ir))
								  ? 
								 ((1U 
								   & (IData)(vlTOPp->mos6502__DOT__ir))
								   ? 0x60U
								   : 0x10U)
								  : 
								 ((1U 
								   & (IData)(vlTOPp->mos6502__DOT__ir))
								   ? 0x30U
								   : 0x10U)))
							        : 
							       ((4U 
								 & (IData)(vlTOPp->mos6502__DOT__ir))
								 ? 0x50U
								 : 
								((2U 
								  & (IData)(vlTOPp->mos6502__DOT__ir))
								  ? 0x60U
								  : 
								 ((1U 
								   & (IData)(vlTOPp->mos6502__DOT__ir))
								   ? 0x40U
								   : 0x58U))))
							       : 
							      ((8U 
								& (IData)(vlTOPp->mos6502__DOT__ir))
							        ? 
							       ((4U 
								 & (IData)(vlTOPp->mos6502__DOT__ir))
								 ? 
								((2U 
								  & (IData)(vlTOPp->mos6502__DOT__ir))
								  ? 
								 ((1U 
								   & (IData)(vlTOPp->mos6502__DOT__ir))
								   ? 0x60U
								   : 0x28U)
								  : 
								 ((1U 
								   & (IData)(vlTOPp->mos6502__DOT__ir))
								   ? 0x28U
								   : 0x70U))
								 : 
								((2U 
								  & (IData)(vlTOPp->mos6502__DOT__ir))
								  ? 
								 ((1U 
								   & (IData)(vlTOPp->mos6502__DOT__ir))
								   ? 0x60U
								   : 0x10U)
								  : 
								 ((1U 
								   & (IData)(vlTOPp->mos6502__DOT__ir))
								   ? 0U
								   : 0x90U)))
							        : 
							       ((4U 
								 & (IData)(vlTOPp->mos6502__DOT__ir))
								 ? 0x48U
								 : 
								((2U 
								  & (IData)(vlTOPp->mos6502__DOT__ir))
								  ? 0x60U
								  : 
								 ((1U 
								   & (IData)(vlTOPp->mos6502__DOT__ir))
								   ? 0x38U
								   : 0x80U))))))
							     : 
							    ((0x20U 
							      & (IData)(vlTOPp->mos6502__DOT__ir))
							      ? 
							     ((0x10U 
							       & (IData)(vlTOPp->mos6502__DOT__ir))
							       ? 
							      ((8U 
								& (IData)(vlTOPp->mos6502__DOT__ir))
							        ? 
							       ((4U 
								 & (IData)(vlTOPp->mos6502__DOT__ir))
								 ? 0x30U
								 : 
								((2U 
								  & (IData)(vlTOPp->mos6502__DOT__ir))
								  ? 
								 ((1U 
								   & (IData)(vlTOPp->mos6502__DOT__ir))
								   ? 0x60U
								   : 0x10U)
								  : 
								 ((1U 
								   & (IData)(vlTOPp->mos6502__DOT__ir))
								   ? 0x30U
								   : 0x10U)))
							        : 
							       ((4U 
								 & (IData)(vlTOPp->mos6502__DOT__ir))
								 ? 0x50U
								 : 
								((2U 
								  & (IData)(vlTOPp->mos6502__DOT__ir))
								  ? 0x60U
								  : 
								 ((1U 
								   & (IData)(vlTOPp->mos6502__DOT__ir))
								   ? 0x40U
								   : 0x58U))))
							       : 
							      ((8U 
								& (IData)(vlTOPp->mos6502__DOT__ir))
							        ? 
							       ((4U 
								 & (IData)(vlTOPp->mos6502__DOT__ir))
								 ? 
								((2U 
								  & (IData)(vlTOPp->mos6502__DOT__ir))
								  ? 
								 ((1U 
								   & (IData)(vlTOPp->mos6502__DOT__ir))
								   ? 0x60U
								   : 0x28U)
								  : 0x28U)
								 : 
								((2U 
								  & (IData)(vlTOPp->mos6502__DOT__ir))
								  ? 
								 ((1U 
								   & (IData)(vlTOPp->mos6502__DOT__ir))
								   ? 0x60U
								   : 0x10U)
								  : 
								 ((1U 
								   & (IData)(vlTOPp->mos6502__DOT__ir))
								   ? 0U
								   : 0x98U)))
							        : 
							       ((4U 
								 & (IData)(vlTOPp->mos6502__DOT__ir))
								 ? 0x48U
								 : 
								((2U 
								  & (IData)(vlTOPp->mos6502__DOT__ir))
								  ? 0x60U
								  : 
								 ((1U 
								   & (IData)(vlTOPp->mos6502__DOT__ir))
								   ? 0x38U
								   : 0x68U)))))
							      : 
							     ((0x10U 
							       & (IData)(vlTOPp->mos6502__DOT__ir))
							       ? 
							      ((8U 
								& (IData)(vlTOPp->mos6502__DOT__ir))
							        ? 
							       ((4U 
								 & (IData)(vlTOPp->mos6502__DOT__ir))
								 ? 0x30U
								 : 
								((2U 
								  & (IData)(vlTOPp->mos6502__DOT__ir))
								  ? 
								 ((1U 
								   & (IData)(vlTOPp->mos6502__DOT__ir))
								   ? 0x60U
								   : 0x10U)
								  : 
								 ((1U 
								   & (IData)(vlTOPp->mos6502__DOT__ir))
								   ? 0x30U
								   : 0x10U)))
							        : 
							       ((4U 
								 & (IData)(vlTOPp->mos6502__DOT__ir))
								 ? 0x50U
								 : 
								((2U 
								  & (IData)(vlTOPp->mos6502__DOT__ir))
								  ? 0x60U
								  : 
								 ((1U 
								   & (IData)(vlTOPp->mos6502__DOT__ir))
								   ? 0x40U
								   : 0x58U))))
							       : 
							      ((8U 
								& (IData)(vlTOPp->mos6502__DOT__ir))
							        ? 
							       ((4U 
								 & (IData)(vlTOPp->mos6502__DOT__ir))
								 ? 
								((2U 
								  & (IData)(vlTOPp->mos6502__DOT__ir))
								  ? 
								 ((1U 
								   & (IData)(vlTOPp->mos6502__DOT__ir))
								   ? 0x60U
								   : 0x28U)
								  : 
								 ((1U 
								   & (IData)(vlTOPp->mos6502__DOT__ir))
								   ? 0x28U
								   : 0x60U))
								 : 
								((2U 
								  & (IData)(vlTOPp->mos6502__DOT__ir))
								  ? 
								 ((1U 
								   & (IData)(vlTOPp->mos6502__DOT__ir))
								   ? 0x60U
								   : 0x10U)
								  : 
								 ((1U 
								   & (IData)(vlTOPp->mos6502__DOT__ir))
								   ? 0U
								   : 0x90U)))
							        : 
							       ((4U 
								 & (IData)(vlTOPp->mos6502__DOT__ir))
								 ? 0x48U
								 : 
								((2U 
								  & (IData)(vlTOPp->mos6502__DOT__ir))
								  ? 0x60U
								  : 
								 ((1U 
								   & (IData)(vlTOPp->mos6502__DOT__ir))
								   ? 0x38U
								   : 0x60U)))))))))))
						        : 
						       ((4U 
							 & (IData)(vlTOPp->mos6502__DOT__state))
							 ? 0x60U
							 : 
							((2U 
							  & (IData)(vlTOPp->mos6502__DOT__state))
							  ? 0x60U
							  : 
							 ((1U 
							   & (IData)(vlTOPp->mos6502__DOT__state))
							   ? 0x60U
							   : 8U))))))));
	}
    }
}

void Vmos6502::_settle__TOP__3(Vmos6502__Syms* __restrict vlSymsp) {
    VL_DEBUG_IF(VL_DBG_MSGF("+    Vmos6502::_settle__TOP__3\n"); );
    Vmos6502* __restrict vlTOPp VL_ATTR_UNUSED = vlSymsp->TOPp;
    // Body
    vlTOPp->mos6502__DOT__din_mux = ((IData)(vlTOPp->ready)
				      ? (IData)(vlTOPp->din)
				      : (IData)(vlTOPp->mos6502__DOT__din_hold));
    // ALWAYS at mos6502/control_signal.v:118
    vlTOPp->mos6502__DOT__adj = ((4U == (((IData)(vlTOPp->mos6502__DOT__adj_bcd) 
					  << 2U) | 
					 (((IData)(vlTOPp->mos6502__DOT__adc_bcd) 
					   << 1U) | (IData)(vlTOPp->mos6502__DOT__HC))))
				  ? 0xaU : ((7U == 
					     (((IData)(vlTOPp->mos6502__DOT__adj_bcd) 
					       << 2U) 
					      | (((IData)(vlTOPp->mos6502__DOT__adc_bcd) 
						  << 1U) 
						 | (IData)(vlTOPp->mos6502__DOT__HC))))
					     ? 6U : 0U));
    // ALWAYS at mos6502/register.v:22
    vlTOPp->__Vtableidx1 = (((IData)(vlTOPp->mos6502__DOT__plp) 
			     << 9U) | (((IData)(vlTOPp->mos6502__DOT__ld_reg) 
					<< 8U) | (IData)(vlTOPp->mos6502__DOT__state)));
    vlTOPp->mos6502__DOT__wr_reg = vlTOPp->__Vtable1_mos6502__DOT__wr_reg
	[vlTOPp->__Vtableidx1];
    vlTOPp->mos6502__DOT__alu_bcd = ((IData)(vlTOPp->mos6502__DOT__adc_bcd) 
				     & (0U == (IData)(vlTOPp->mos6502__DOT__state)));
    vlTOPp->mos6502__DOT__sr_flags = (0x30U | (((IData)(vlTOPp->mos6502__DOT__N) 
						<< 7U) 
					       | (((IData)(vlTOPp->mos6502__DOT__V) 
						   << 6U) 
						  | (((IData)(vlTOPp->mos6502__DOT__D) 
						      << 3U) 
						     | (((IData)(vlTOPp->mos6502__DOT__I) 
							 << 2U) 
							| (((IData)(vlTOPp->mos6502__DOT__Z) 
							    << 1U) 
							   | (IData)(vlTOPp->mos6502__DOT__C)))))));
    // ALWAYS at mos6502/alu_control.v:83
    vlTOPp->mos6502__DOT__alu_shift_right = ((((0U 
						== (IData)(vlTOPp->mos6502__DOT__state)) 
					       | (0x10U 
						  == (IData)(vlTOPp->mos6502__DOT__state))) 
					      | (0x18U 
						 == (IData)(vlTOPp->mos6502__DOT__state))) 
					     & (IData)(vlTOPp->mos6502__DOT__shift_right));
    // ALWAYS at mos6502/alu_control.v:83
    vlTOPp->mos6502__DOT__alu_op = ((0x80U & (IData)(vlTOPp->mos6502__DOT__state))
				     ? ((0x40U & (IData)(vlTOPp->mos6502__DOT__state))
					 ? 3U : ((0x20U 
						  & (IData)(vlTOPp->mos6502__DOT__state))
						  ? 3U
						  : 
						 ((0x10U 
						   & (IData)(vlTOPp->mos6502__DOT__state))
						   ? 
						  ((8U 
						    & (IData)(vlTOPp->mos6502__DOT__state))
						    ? 3U
						    : 
						   ((4U 
						     & (IData)(vlTOPp->mos6502__DOT__state))
						     ? 3U
						     : 
						    ((2U 
						      & (IData)(vlTOPp->mos6502__DOT__state))
						      ? 3U
						      : 
						     ((1U 
						       & (IData)(vlTOPp->mos6502__DOT__state))
						       ? 7U
						       : 3U))))
						   : 3U)))
				     : ((0x40U & (IData)(vlTOPp->mos6502__DOT__state))
					 ? ((0x20U 
					     & (IData)(vlTOPp->mos6502__DOT__state))
					     ? ((0x10U 
						 & (IData)(vlTOPp->mos6502__DOT__state))
						 ? 3U
						 : 
						((8U 
						  & (IData)(vlTOPp->mos6502__DOT__state))
						  ? 
						 ((4U 
						   & (IData)(vlTOPp->mos6502__DOT__state))
						   ? 3U
						   : 
						  ((2U 
						    & (IData)(vlTOPp->mos6502__DOT__state))
						    ? 3U
						    : 7U))
						  : 
						 ((4U 
						   & (IData)(vlTOPp->mos6502__DOT__state))
						   ? 3U
						   : 
						  ((2U 
						    & (IData)(vlTOPp->mos6502__DOT__state))
						    ? 
						   ((1U 
						     & (IData)(vlTOPp->mos6502__DOT__state))
						     ? 3U
						     : 7U)
						    : 7U))))
					     : ((0x10U 
						 & (IData)(vlTOPp->mos6502__DOT__state))
						 ? 
						((8U 
						  & (IData)(vlTOPp->mos6502__DOT__state))
						  ? 
						 ((4U 
						   & (IData)(vlTOPp->mos6502__DOT__state))
						   ? 3U
						   : 
						  ((2U 
						    & (IData)(vlTOPp->mos6502__DOT__state))
						    ? 3U
						    : 
						   ((1U 
						     & (IData)(vlTOPp->mos6502__DOT__state))
						     ? 
						    ((IData)(vlTOPp->mos6502__DOT__backwards)
						      ? 7U
						      : 3U)
						     : 3U)))
						  : 3U)
						 : 3U))
					 : ((0x20U 
					     & (IData)(vlTOPp->mos6502__DOT__state))
					     ? 3U : 
					    ((0x10U 
					      & (IData)(vlTOPp->mos6502__DOT__state))
					      ? ((8U 
						  & (IData)(vlTOPp->mos6502__DOT__state))
						  ? 
						 ((4U 
						   & (IData)(vlTOPp->mos6502__DOT__state))
						   ? 3U
						   : 
						  ((2U 
						    & (IData)(vlTOPp->mos6502__DOT__state))
						    ? 3U
						    : 
						   ((1U 
						     & (IData)(vlTOPp->mos6502__DOT__state))
						     ? 3U
						     : (IData)(vlTOPp->mos6502__DOT__op))))
						  : 
						 ((4U 
						   & (IData)(vlTOPp->mos6502__DOT__state))
						   ? 3U
						   : 
						  ((2U 
						    & (IData)(vlTOPp->mos6502__DOT__state))
						    ? 3U
						    : 
						   ((1U 
						     & (IData)(vlTOPp->mos6502__DOT__state))
						     ? 3U
						     : (IData)(vlTOPp->mos6502__DOT__op)))))
					      : ((8U 
						  & (IData)(vlTOPp->mos6502__DOT__state))
						  ? 3U
						  : 
						 ((4U 
						   & (IData)(vlTOPp->mos6502__DOT__state))
						   ? 3U
						   : 
						  ((2U 
						    & (IData)(vlTOPp->mos6502__DOT__state))
						    ? 3U
						    : 
						   ((1U 
						     & (IData)(vlTOPp->mos6502__DOT__state))
						     ? 3U
						     : (IData)(vlTOPp->mos6502__DOT__op)))))))));
    // ALWAYS at mos6502/alu_control.v:83
    vlTOPp->mos6502__DOT__carry_in = (1U & ((0x80U 
					     & (IData)(vlTOPp->mos6502__DOT__state))
					     ? ((~ 
						 ((IData)(vlTOPp->mos6502__DOT__state) 
						  >> 6U)) 
						& ((~ 
						    ((IData)(vlTOPp->mos6502__DOT__state) 
						     >> 5U)) 
						   & ((~ 
						       ((IData)(vlTOPp->mos6502__DOT__state) 
							>> 4U)) 
						      & ((8U 
							  & (IData)(vlTOPp->mos6502__DOT__state))
							  ? 
							 ((~ 
							   ((IData)(vlTOPp->mos6502__DOT__state) 
							    >> 2U)) 
							  & (~ 
							     ((IData)(vlTOPp->mos6502__DOT__state) 
							      >> 1U)))
							  : 
							 ((~ 
							   ((IData)(vlTOPp->mos6502__DOT__state) 
							    >> 2U)) 
							  & ((~ 
							      ((IData)(vlTOPp->mos6502__DOT__state) 
							       >> 1U)) 
							     | (~ (IData)(vlTOPp->mos6502__DOT__state))))))))
					     : ((0x40U 
						 & (IData)(vlTOPp->mos6502__DOT__state))
						 ? 
						((~ 
						  ((IData)(vlTOPp->mos6502__DOT__state) 
						   >> 5U)) 
						 & ((0x10U 
						     & (IData)(vlTOPp->mos6502__DOT__state))
						     ? 
						    (((IData)(vlTOPp->mos6502__DOT__state) 
						      >> 3U) 
						     & ((~ 
							 ((IData)(vlTOPp->mos6502__DOT__state) 
							  >> 2U)) 
							& ((~ 
							    ((IData)(vlTOPp->mos6502__DOT__state) 
							     >> 1U)) 
							   & ((IData)(vlTOPp->mos6502__DOT__state) 
							      & (IData)(vlTOPp->mos6502__DOT__carry_out)))))
						     : 
						    ((~ 
						      ((IData)(vlTOPp->mos6502__DOT__state) 
						       >> 3U)) 
						     & ((~ 
							 ((IData)(vlTOPp->mos6502__DOT__state) 
							  >> 2U)) 
							& ((2U 
							    & (IData)(vlTOPp->mos6502__DOT__state))
							    ? 
							   ((~ (IData)(vlTOPp->mos6502__DOT__state)) 
							    & (IData)(vlTOPp->mos6502__DOT__carry_out))
							    : 
							   (~ (IData)(vlTOPp->mos6502__DOT__state)))))))
						 : 
						((0x20U 
						  & (IData)(vlTOPp->mos6502__DOT__state))
						  ? 
						 (((IData)(vlTOPp->mos6502__DOT__state) 
						   >> 4U) 
						  & ((8U 
						      & (IData)(vlTOPp->mos6502__DOT__state))
						      ? 
						     ((~ 
						       ((IData)(vlTOPp->mos6502__DOT__state) 
							>> 2U)) 
						      & ((~ 
							  ((IData)(vlTOPp->mos6502__DOT__state) 
							   >> 1U)) 
							 & (IData)(vlTOPp->mos6502__DOT__state)))
						      : 
						     ((~ 
						       ((IData)(vlTOPp->mos6502__DOT__state) 
							>> 2U)) 
						      & ((~ 
							  ((IData)(vlTOPp->mos6502__DOT__state) 
							   >> 1U)) 
							 & ((IData)(vlTOPp->mos6502__DOT__state) 
							    & (IData)(vlTOPp->mos6502__DOT__carry_out))))))
						  : 
						 ((0x10U 
						   & (IData)(vlTOPp->mos6502__DOT__state))
						   ? 
						  ((~ 
						    ((IData)(vlTOPp->mos6502__DOT__state) 
						     >> 2U)) 
						   & ((~ 
						       ((IData)(vlTOPp->mos6502__DOT__state) 
							>> 1U)) 
						      & ((~ (IData)(vlTOPp->mos6502__DOT__state)) 
							 & ((IData)(vlTOPp->mos6502__DOT__rotate)
							     ? (IData)(vlTOPp->mos6502__DOT__C)
							     : 
							    ((IData)(vlTOPp->mos6502__DOT__shift)
							      ? 0U
							      : (IData)(vlTOPp->mos6502__DOT__inc))))))
						   : 
						  ((~ 
						    ((IData)(vlTOPp->mos6502__DOT__state) 
						     >> 3U)) 
						   & ((~ 
						       ((IData)(vlTOPp->mos6502__DOT__state) 
							>> 2U)) 
						      & ((~ 
							  ((IData)(vlTOPp->mos6502__DOT__state) 
							   >> 1U)) 
							 & ((~ (IData)(vlTOPp->mos6502__DOT__state)) 
							    & ((IData)(vlTOPp->mos6502__DOT__rotate)
							        ? (IData)(vlTOPp->mos6502__DOT__C)
							        : 
							       ((IData)(vlTOPp->mos6502__DOT__compare)
								 ? 1U
								 : 
								(((IData)(vlTOPp->mos6502__DOT__shift) 
								  | (IData)(vlTOPp->mos6502__DOT__load_only))
								  ? 0U
								  : (IData)(vlTOPp->mos6502__DOT__C)))))))))))));
    // ALWAYS at mos6502/control_signal.v:118
    vlTOPp->mos6502__DOT__reg_idx = ((0x80U & (IData)(vlTOPp->mos6502__DOT__state))
				      ? ((0x40U & (IData)(vlTOPp->mos6502__DOT__state))
					  ? (IData)(vlTOPp->mos6502__DOT__src_reg)
					  : ((0x20U 
					      & (IData)(vlTOPp->mos6502__DOT__state))
					      ? (IData)(vlTOPp->mos6502__DOT__src_reg)
					      : ((0x10U 
						  & (IData)(vlTOPp->mos6502__DOT__state))
						  ? 
						 ((8U 
						   & (IData)(vlTOPp->mos6502__DOT__state))
						   ? 
						  ((4U 
						    & (IData)(vlTOPp->mos6502__DOT__state))
						    ? (IData)(vlTOPp->mos6502__DOT__src_reg)
						    : 
						   ((2U 
						     & (IData)(vlTOPp->mos6502__DOT__state))
						     ? (IData)(vlTOPp->mos6502__DOT__src_reg)
						     : 3U))
						   : 
						  ((4U 
						    & (IData)(vlTOPp->mos6502__DOT__state))
						    ? (IData)(vlTOPp->mos6502__DOT__src_reg)
						    : 
						   ((2U 
						     & (IData)(vlTOPp->mos6502__DOT__state))
						     ? (IData)(vlTOPp->mos6502__DOT__src_reg)
						     : 
						    ((1U 
						      & (IData)(vlTOPp->mos6502__DOT__state))
						      ? 3U
						      : (IData)(vlTOPp->mos6502__DOT__src_reg)))))
						  : 
						 ((8U 
						   & (IData)(vlTOPp->mos6502__DOT__state))
						   ? 
						  ((4U 
						    & (IData)(vlTOPp->mos6502__DOT__state))
						    ? (IData)(vlTOPp->mos6502__DOT__src_reg)
						    : 
						   ((1U 
						     & (IData)(vlTOPp->mos6502__DOT__state))
						     ? (IData)(vlTOPp->mos6502__DOT__src_reg)
						     : 3U))
						   : 
						  ((4U 
						    & (IData)(vlTOPp->mos6502__DOT__state))
						    ? (IData)(vlTOPp->mos6502__DOT__src_reg)
						    : 
						   ((2U 
						     & (IData)(vlTOPp->mos6502__DOT__state))
						     ? 
						    ((1U 
						      & (IData)(vlTOPp->mos6502__DOT__state))
						      ? 3U
						      : (IData)(vlTOPp->mos6502__DOT__src_reg))
						     : 
						    ((1U 
						      & (IData)(vlTOPp->mos6502__DOT__state))
						      ? (IData)(vlTOPp->mos6502__DOT__src_reg)
						      : 3U)))))))
				      : ((0x40U & (IData)(vlTOPp->mos6502__DOT__state))
					  ? ((0x20U 
					      & (IData)(vlTOPp->mos6502__DOT__state))
					      ? ((0x10U 
						  & (IData)(vlTOPp->mos6502__DOT__state))
						  ? (IData)(vlTOPp->mos6502__DOT__src_reg)
						  : 
						 ((8U 
						   & (IData)(vlTOPp->mos6502__DOT__state))
						   ? 
						  ((4U 
						    & (IData)(vlTOPp->mos6502__DOT__state))
						    ? (IData)(vlTOPp->mos6502__DOT__src_reg)
						    : 
						   ((1U 
						     & (IData)(vlTOPp->mos6502__DOT__state))
						     ? (IData)(vlTOPp->mos6502__DOT__src_reg)
						     : 3U))
						   : 
						  ((4U 
						    & (IData)(vlTOPp->mos6502__DOT__state))
						    ? (IData)(vlTOPp->mos6502__DOT__src_reg)
						    : 
						   ((2U 
						     & (IData)(vlTOPp->mos6502__DOT__state))
						     ? 
						    ((1U 
						      & (IData)(vlTOPp->mos6502__DOT__state))
						      ? 3U
						      : (IData)(vlTOPp->mos6502__DOT__src_reg))
						     : 
						    ((1U 
						      & (IData)(vlTOPp->mos6502__DOT__state))
						      ? (IData)(vlTOPp->mos6502__DOT__src_reg)
						      : 3U)))))
					      : ((8U 
						  & (IData)(vlTOPp->mos6502__DOT__state))
						  ? (IData)(vlTOPp->mos6502__DOT__src_reg)
						  : 
						 ((4U 
						   & (IData)(vlTOPp->mos6502__DOT__state))
						   ? (IData)(vlTOPp->mos6502__DOT__src_reg)
						   : 
						  ((2U 
						    & (IData)(vlTOPp->mos6502__DOT__state))
						    ? (IData)(vlTOPp->mos6502__DOT__src_reg)
						    : 
						   ((1U 
						     & (IData)(vlTOPp->mos6502__DOT__state))
						     ? (IData)(vlTOPp->mos6502__DOT__src_reg)
						     : 
						    ((IData)(vlTOPp->mos6502__DOT__reg_y)
						      ? 2U
						      : 1U))))))
					  : ((0x20U 
					      & (IData)(vlTOPp->mos6502__DOT__state))
					      ? ((0x10U 
						  & (IData)(vlTOPp->mos6502__DOT__state))
						  ? 
						 ((4U 
						   & (IData)(vlTOPp->mos6502__DOT__state))
						   ? (IData)(vlTOPp->mos6502__DOT__src_reg)
						   : 
						  ((2U 
						    & (IData)(vlTOPp->mos6502__DOT__state))
						    ? (IData)(vlTOPp->mos6502__DOT__src_reg)
						    : 
						   ((1U 
						     & (IData)(vlTOPp->mos6502__DOT__state))
						     ? (IData)(vlTOPp->mos6502__DOT__src_reg)
						     : 
						    ((IData)(vlTOPp->mos6502__DOT__reg_y)
						      ? 2U
						      : 1U))))
						  : (IData)(vlTOPp->mos6502__DOT__src_reg))
					      : ((0x10U 
						  & (IData)(vlTOPp->mos6502__DOT__state))
						  ? (IData)(vlTOPp->mos6502__DOT__src_reg)
						  : 
						 ((8U 
						   & (IData)(vlTOPp->mos6502__DOT__state))
						   ? 
						  ((4U 
						    & (IData)(vlTOPp->mos6502__DOT__state))
						    ? (IData)(vlTOPp->mos6502__DOT__src_reg)
						    : 
						   ((2U 
						     & (IData)(vlTOPp->mos6502__DOT__state))
						     ? (IData)(vlTOPp->mos6502__DOT__src_reg)
						     : 
						    ((1U 
						      & (IData)(vlTOPp->mos6502__DOT__state))
						      ? (IData)(vlTOPp->mos6502__DOT__src_reg)
						      : (IData)(vlTOPp->mos6502__DOT__dst_reg))))
						   : (IData)(vlTOPp->mos6502__DOT__src_reg))))));
    vlTOPp->mos6502__DOT__ir = ((((~ (IData)(vlTOPp->mos6502__DOT__I)) 
				  & (IData)(vlTOPp->irq)) 
				 | (IData)(vlTOPp->mos6502__DOT__nmi_edge))
				 ? 0U : ((IData)(vlTOPp->mos6502__DOT__ir_hold_valid)
					  ? (IData)(vlTOPp->mos6502__DOT__ir_hold)
					  : (IData)(vlTOPp->mos6502__DOT__din_mux)));
    // ALWAYS at mos6502/pc.v:3
    if ((0x80U & (IData)(vlTOPp->mos6502__DOT__state))) {
	if ((0x40U & (IData)(vlTOPp->mos6502__DOT__state))) {
	    vlTOPp->mos6502__DOT__pc_mux = vlTOPp->mos6502__DOT__pc;
	    vlTOPp->mos6502__DOT__pc_inc = 0U;
	} else {
	    if ((0x20U & (IData)(vlTOPp->mos6502__DOT__state))) {
		vlTOPp->mos6502__DOT__pc_mux = vlTOPp->mos6502__DOT__pc;
		vlTOPp->mos6502__DOT__pc_inc = 0U;
	    } else {
		if ((0x10U & (IData)(vlTOPp->mos6502__DOT__state))) {
		    vlTOPp->mos6502__DOT__pc_mux = vlTOPp->mos6502__DOT__pc;
		    vlTOPp->mos6502__DOT__pc_inc = 0U;
		} else {
		    if ((8U & (IData)(vlTOPp->mos6502__DOT__state))) {
			if ((4U & (IData)(vlTOPp->mos6502__DOT__state))) {
			    vlTOPp->mos6502__DOT__pc_mux 
				= vlTOPp->mos6502__DOT__pc;
			    vlTOPp->mos6502__DOT__pc_inc = 0U;
			} else {
			    if ((2U & (IData)(vlTOPp->mos6502__DOT__state))) {
				if ((1U & (IData)(vlTOPp->mos6502__DOT__state))) {
				    vlTOPp->mos6502__DOT__pc_mux 
					= (((IData)(vlTOPp->mos6502__DOT__din_mux) 
					    << 8U) 
					   | (IData)(vlTOPp->mos6502__DOT__alu_out));
				    vlTOPp->mos6502__DOT__pc_inc = 1U;
				} else {
				    vlTOPp->mos6502__DOT__pc_mux 
					= vlTOPp->mos6502__DOT__pc;
				    vlTOPp->mos6502__DOT__pc_inc = 0U;
				}
			    } else {
				vlTOPp->mos6502__DOT__pc_mux 
				    = vlTOPp->mos6502__DOT__pc;
				vlTOPp->mos6502__DOT__pc_inc = 0U;
			    }
			}
		    } else {
			if ((4U & (IData)(vlTOPp->mos6502__DOT__state))) {
			    if ((2U & (IData)(vlTOPp->mos6502__DOT__state))) {
				vlTOPp->mos6502__DOT__pc_mux 
				    = vlTOPp->mos6502__DOT__pc;
				vlTOPp->mos6502__DOT__pc_inc = 0U;
			    } else {
				if ((1U & (IData)(vlTOPp->mos6502__DOT__state))) {
				    vlTOPp->mos6502__DOT__pc_mux 
					= vlTOPp->mos6502__DOT__pc;
				    vlTOPp->mos6502__DOT__pc_inc = 0U;
				} else {
				    vlTOPp->mos6502__DOT__pc_mux 
					= (((IData)(vlTOPp->mos6502__DOT__din_mux) 
					    << 8U) 
					   | (IData)(vlTOPp->mos6502__DOT__alu_out));
				    vlTOPp->mos6502__DOT__pc_inc = 1U;
				}
			    }
			} else {
			    vlTOPp->mos6502__DOT__pc_mux 
				= vlTOPp->mos6502__DOT__pc;
			    vlTOPp->mos6502__DOT__pc_inc = 0U;
			}
		    }
		}
	    }
	}
    } else {
	if ((0x40U & (IData)(vlTOPp->mos6502__DOT__state))) {
	    if ((0x20U & (IData)(vlTOPp->mos6502__DOT__state))) {
		if ((0x10U & (IData)(vlTOPp->mos6502__DOT__state))) {
		    if ((4U & (IData)(vlTOPp->mos6502__DOT__state))) {
			vlTOPp->mos6502__DOT__pc_mux 
			    = vlTOPp->mos6502__DOT__pc;
			vlTOPp->mos6502__DOT__pc_inc = 0U;
		    } else {
			if ((2U & (IData)(vlTOPp->mos6502__DOT__state))) {
			    vlTOPp->mos6502__DOT__pc_mux 
				= vlTOPp->mos6502__DOT__pc;
			    vlTOPp->mos6502__DOT__pc_inc = 0U;
			} else {
			    if ((1U & (IData)(vlTOPp->mos6502__DOT__state))) {
				vlTOPp->mos6502__DOT__pc_mux 
				    = (((IData)(vlTOPp->mos6502__DOT__din_mux) 
					<< 8U) | (IData)(vlTOPp->mos6502__DOT__alu_out));
				vlTOPp->mos6502__DOT__pc_inc = 1U;
			    } else {
				vlTOPp->mos6502__DOT__pc_mux 
				    = vlTOPp->mos6502__DOT__pc;
				vlTOPp->mos6502__DOT__pc_inc = 0U;
			    }
			}
		    }
		} else {
		    if ((8U & (IData)(vlTOPp->mos6502__DOT__state))) {
			if ((4U & (IData)(vlTOPp->mos6502__DOT__state))) {
			    vlTOPp->mos6502__DOT__pc_mux 
				= vlTOPp->mos6502__DOT__pc;
			    vlTOPp->mos6502__DOT__pc_inc = 0U;
			} else {
			    if ((2U & (IData)(vlTOPp->mos6502__DOT__state))) {
				if ((1U & (IData)(vlTOPp->mos6502__DOT__state))) {
				    vlTOPp->mos6502__DOT__pc_mux 
					= (((IData)(vlTOPp->mos6502__DOT__din_mux) 
					    << 8U) 
					   | (IData)(vlTOPp->mos6502__DOT__alu_out));
				    vlTOPp->mos6502__DOT__pc_inc = 0U;
				} else {
				    vlTOPp->mos6502__DOT__pc_mux 
					= vlTOPp->mos6502__DOT__pc;
				    vlTOPp->mos6502__DOT__pc_inc = 0U;
				}
			    } else {
				vlTOPp->mos6502__DOT__pc_mux 
				    = vlTOPp->mos6502__DOT__pc;
				vlTOPp->mos6502__DOT__pc_inc = 0U;
			    }
			}
		    } else {
			if ((4U & (IData)(vlTOPp->mos6502__DOT__state))) {
			    vlTOPp->mos6502__DOT__pc_mux 
				= vlTOPp->mos6502__DOT__pc;
			    vlTOPp->mos6502__DOT__pc_inc = 0U;
			} else {
			    if ((2U & (IData)(vlTOPp->mos6502__DOT__state))) {
				if ((1U & (IData)(vlTOPp->mos6502__DOT__state))) {
				    vlTOPp->mos6502__DOT__pc_mux 
					= vlTOPp->mos6502__DOT__pc;
				    vlTOPp->mos6502__DOT__pc_inc = 1U;
				} else {
				    vlTOPp->mos6502__DOT__pc_mux 
					= ((IData)(vlTOPp->mos6502__DOT__res)
					    ? 0xfffcU
					    : ((IData)(vlTOPp->mos6502__DOT__nmi_edge)
					        ? 0xfffaU
					        : 0xfffeU));
				    vlTOPp->mos6502__DOT__pc_inc = 0U;
				}
			    } else {
				vlTOPp->mos6502__DOT__pc_mux 
				    = vlTOPp->mos6502__DOT__pc;
				vlTOPp->mos6502__DOT__pc_inc = 0U;
			    }
			}
		    }
		}
	    } else {
		if ((0x10U & (IData)(vlTOPp->mos6502__DOT__state))) {
		    if ((8U & (IData)(vlTOPp->mos6502__DOT__state))) {
			if ((4U & (IData)(vlTOPp->mos6502__DOT__state))) {
			    vlTOPp->mos6502__DOT__pc_mux 
				= vlTOPp->mos6502__DOT__pc;
			    vlTOPp->mos6502__DOT__pc_inc = 0U;
			} else {
			    if ((2U & (IData)(vlTOPp->mos6502__DOT__state))) {
				if ((1U & (IData)(vlTOPp->mos6502__DOT__state))) {
				    vlTOPp->mos6502__DOT__pc_mux 
					= vlTOPp->mos6502__DOT__pc;
				    vlTOPp->mos6502__DOT__pc_inc = 0U;
				} else {
				    vlTOPp->mos6502__DOT__pc_mux 
					= (((IData)(vlTOPp->mos6502__DOT__alu_out) 
					    << 8U) 
					   | (0xffU 
					      & (IData)(vlTOPp->mos6502__DOT__pc)));
				    vlTOPp->mos6502__DOT__pc_inc = 1U;
				}
			    } else {
				if ((1U & (IData)(vlTOPp->mos6502__DOT__state))) {
				    vlTOPp->mos6502__DOT__pc_mux 
					= (((IData)(vlTOPp->mos6502__DOT__addr_h) 
					    << 8U) 
					   | (IData)(vlTOPp->mos6502__DOT__alu_out));
				    vlTOPp->mos6502__DOT__pc_inc 
					= (1U & ((IData)(vlTOPp->mos6502__DOT__carry_out) 
						 ^ 
						 (~ (IData)(vlTOPp->mos6502__DOT__backwards))));
				} else {
				    vlTOPp->mos6502__DOT__pc_mux 
					= vlTOPp->mos6502__DOT__pc;
				    vlTOPp->mos6502__DOT__pc_inc = 1U;
				}
			    }
			}
		    } else {
			vlTOPp->mos6502__DOT__pc_mux 
			    = vlTOPp->mos6502__DOT__pc;
			vlTOPp->mos6502__DOT__pc_inc = 0U;
		    }
		} else {
		    vlTOPp->mos6502__DOT__pc_mux = vlTOPp->mos6502__DOT__pc;
		    vlTOPp->mos6502__DOT__pc_inc = 0U;
		}
	    }
	} else {
	    if ((0x20U & (IData)(vlTOPp->mos6502__DOT__state))) {
		if ((0x10U & (IData)(vlTOPp->mos6502__DOT__state))) {
		    if ((8U & (IData)(vlTOPp->mos6502__DOT__state))) {
			vlTOPp->mos6502__DOT__pc_mux 
			    = vlTOPp->mos6502__DOT__pc;
			vlTOPp->mos6502__DOT__pc_inc = 0U;
		    } else {
			if ((4U & (IData)(vlTOPp->mos6502__DOT__state))) {
			    vlTOPp->mos6502__DOT__pc_mux 
				= vlTOPp->mos6502__DOT__pc;
			    vlTOPp->mos6502__DOT__pc_inc = 0U;
			} else {
			    if ((2U & (IData)(vlTOPp->mos6502__DOT__state))) {
				vlTOPp->mos6502__DOT__pc_mux 
				    = vlTOPp->mos6502__DOT__pc;
				vlTOPp->mos6502__DOT__pc_inc = 0U;
			    } else {
				if ((1U & (IData)(vlTOPp->mos6502__DOT__state))) {
				    vlTOPp->mos6502__DOT__pc_mux 
					= vlTOPp->mos6502__DOT__pc;
				    vlTOPp->mos6502__DOT__pc_inc = 0U;
				} else {
				    vlTOPp->mos6502__DOT__pc_mux 
					= vlTOPp->mos6502__DOT__pc;
				    vlTOPp->mos6502__DOT__pc_inc = 1U;
				}
			    }
			}
		    }
		} else {
		    if ((8U & (IData)(vlTOPp->mos6502__DOT__state))) {
			if ((4U & (IData)(vlTOPp->mos6502__DOT__state))) {
			    vlTOPp->mos6502__DOT__pc_mux 
				= vlTOPp->mos6502__DOT__pc;
			    vlTOPp->mos6502__DOT__pc_inc = 0U;
			} else {
			    if ((2U & (IData)(vlTOPp->mos6502__DOT__state))) {
				vlTOPp->mos6502__DOT__pc_mux 
				    = vlTOPp->mos6502__DOT__pc;
				vlTOPp->mos6502__DOT__pc_inc = 0U;
			    } else {
				if ((1U & (IData)(vlTOPp->mos6502__DOT__state))) {
				    vlTOPp->mos6502__DOT__pc_mux 
					= vlTOPp->mos6502__DOT__pc;
				    vlTOPp->mos6502__DOT__pc_inc = 0U;
				} else {
				    vlTOPp->mos6502__DOT__pc_mux 
					= vlTOPp->mos6502__DOT__pc;
				    vlTOPp->mos6502__DOT__pc_inc = 1U;
				}
			    }
			}
		    } else {
			vlTOPp->mos6502__DOT__pc_mux 
			    = vlTOPp->mos6502__DOT__pc;
			vlTOPp->mos6502__DOT__pc_inc = 0U;
		    }
		}
	    } else {
		if ((0x10U & (IData)(vlTOPp->mos6502__DOT__state))) {
		    vlTOPp->mos6502__DOT__pc_mux = vlTOPp->mos6502__DOT__pc;
		    vlTOPp->mos6502__DOT__pc_inc = 0U;
		} else {
		    if ((8U & (IData)(vlTOPp->mos6502__DOT__state))) {
			if ((4U & (IData)(vlTOPp->mos6502__DOT__state))) {
			    vlTOPp->mos6502__DOT__pc_mux 
				= vlTOPp->mos6502__DOT__pc;
			    vlTOPp->mos6502__DOT__pc_inc = 0U;
			} else {
			    if ((2U & (IData)(vlTOPp->mos6502__DOT__state))) {
				vlTOPp->mos6502__DOT__pc_mux 
				    = vlTOPp->mos6502__DOT__pc;
				vlTOPp->mos6502__DOT__pc_inc = 0U;
			    } else {
				if ((1U & (IData)(vlTOPp->mos6502__DOT__state))) {
				    vlTOPp->mos6502__DOT__pc_mux 
					= vlTOPp->mos6502__DOT__pc;
				    vlTOPp->mos6502__DOT__pc_inc = 0U;
				} else {
				    if ((((~ (IData)(vlTOPp->mos6502__DOT__I)) 
					  & (IData)(vlTOPp->irq)) 
					 | (IData)(vlTOPp->mos6502__DOT__nmi_edge))) {
					vlTOPp->mos6502__DOT__pc_mux 
					    = (((IData)(vlTOPp->mos6502__DOT__addr_h) 
						<< 8U) 
					       | (IData)(vlTOPp->mos6502__DOT__addr_l));
					vlTOPp->mos6502__DOT__pc_inc = 0U;
				    } else {
					vlTOPp->mos6502__DOT__pc_mux 
					    = vlTOPp->mos6502__DOT__pc;
					vlTOPp->mos6502__DOT__pc_inc = 1U;
				    }
				}
			    }
			}
		    } else {
			if ((4U & (IData)(vlTOPp->mos6502__DOT__state))) {
			    vlTOPp->mos6502__DOT__pc_mux 
				= vlTOPp->mos6502__DOT__pc;
			    vlTOPp->mos6502__DOT__pc_inc = 0U;
			} else {
			    if ((2U & (IData)(vlTOPp->mos6502__DOT__state))) {
				vlTOPp->mos6502__DOT__pc_mux 
				    = vlTOPp->mos6502__DOT__pc;
				vlTOPp->mos6502__DOT__pc_inc = 0U;
			    } else {
				if ((1U & (IData)(vlTOPp->mos6502__DOT__state))) {
				    vlTOPp->mos6502__DOT__pc_mux 
					= vlTOPp->mos6502__DOT__pc;
				    vlTOPp->mos6502__DOT__pc_inc = 0U;
				} else {
				    vlTOPp->mos6502__DOT__pc_mux 
					= vlTOPp->mos6502__DOT__pc;
				    vlTOPp->mos6502__DOT__pc_inc = 1U;
				}
			    }
			}
		    }
		}
	    }
	}
    }
    // ALWAYS at mos6502/alu_control.v:83
    vlTOPp->mos6502__DOT__alu_B = (0xffU & ((0x80U 
					     & (IData)(vlTOPp->mos6502__DOT__state))
					     ? ((0x40U 
						 & (IData)(vlTOPp->mos6502__DOT__state))
						 ? (IData)(vlTOPp->mos6502__DOT__din_mux)
						 : 
						((0x20U 
						  & (IData)(vlTOPp->mos6502__DOT__state))
						  ? (IData)(vlTOPp->mos6502__DOT__din_mux)
						  : 
						 ((0x10U 
						   & (IData)(vlTOPp->mos6502__DOT__state))
						   ? 
						  ((8U 
						    & (IData)(vlTOPp->mos6502__DOT__state))
						    ? 
						   ((4U 
						     & (IData)(vlTOPp->mos6502__DOT__state))
						     ? (IData)(vlTOPp->mos6502__DOT__din_mux)
						     : 
						    ((2U 
						      & (IData)(vlTOPp->mos6502__DOT__state))
						      ? (IData)(vlTOPp->mos6502__DOT__din_mux)
						      : 
						     ((1U 
						       & (IData)(vlTOPp->mos6502__DOT__state))
						       ? (IData)(vlTOPp->mos6502__DOT__din_mux)
						       : 0U)))
						    : 
						   ((4U 
						     & (IData)(vlTOPp->mos6502__DOT__state))
						     ? (IData)(vlTOPp->mos6502__DOT__din_mux)
						     : 
						    ((2U 
						      & (IData)(vlTOPp->mos6502__DOT__state))
						      ? (IData)(vlTOPp->mos6502__DOT__din_mux)
						      : 0U)))
						   : 
						  ((8U 
						    & (IData)(vlTOPp->mos6502__DOT__state))
						    ? 
						   ((4U 
						     & (IData)(vlTOPp->mos6502__DOT__state))
						     ? (IData)(vlTOPp->mos6502__DOT__din_mux)
						     : 
						    ((2U 
						      & (IData)(vlTOPp->mos6502__DOT__state))
						      ? (IData)(vlTOPp->mos6502__DOT__din_mux)
						      : 0U))
						    : 
						   ((4U 
						     & (IData)(vlTOPp->mos6502__DOT__state))
						     ? (IData)(vlTOPp->mos6502__DOT__din_mux)
						     : 
						    ((2U 
						      & (IData)(vlTOPp->mos6502__DOT__state))
						      ? 
						     ((1U 
						       & (IData)(vlTOPp->mos6502__DOT__state))
						       ? (IData)(vlTOPp->mos6502__DOT__din_mux)
						       : 0U)
						      : 0U))))))
					     : ((0x40U 
						 & (IData)(vlTOPp->mos6502__DOT__state))
						 ? 
						((0x20U 
						  & (IData)(vlTOPp->mos6502__DOT__state))
						  ? 
						 ((0x10U 
						   & (IData)(vlTOPp->mos6502__DOT__state))
						   ? (IData)(vlTOPp->mos6502__DOT__din_mux)
						   : 
						  ((4U 
						    & (IData)(vlTOPp->mos6502__DOT__state))
						    ? (IData)(vlTOPp->mos6502__DOT__din_mux)
						    : 
						   ((2U 
						     & (IData)(vlTOPp->mos6502__DOT__state))
						     ? 
						    ((1U 
						      & (IData)(vlTOPp->mos6502__DOT__state))
						      ? (IData)(vlTOPp->mos6502__DOT__din_mux)
						      : 0U)
						     : 0U)))
						  : 
						 ((0x10U 
						   & (IData)(vlTOPp->mos6502__DOT__state))
						   ? 
						  ((8U 
						    & (IData)(vlTOPp->mos6502__DOT__state))
						    ? 
						   ((4U 
						     & (IData)(vlTOPp->mos6502__DOT__state))
						     ? (IData)(vlTOPp->mos6502__DOT__din_mux)
						     : 
						    ((2U 
						      & (IData)(vlTOPp->mos6502__DOT__state))
						      ? (IData)(vlTOPp->mos6502__DOT__din_mux)
						      : 
						     ((1U 
						       & (IData)(vlTOPp->mos6502__DOT__state))
						       ? 0U
						       : (IData)(vlTOPp->mos6502__DOT__pc))))
						    : (IData)(vlTOPp->mos6502__DOT__din_mux))
						   : (IData)(vlTOPp->mos6502__DOT__din_mux)))
						 : 
						((0x20U 
						  & (IData)(vlTOPp->mos6502__DOT__state))
						  ? 
						 ((0x10U 
						   & (IData)(vlTOPp->mos6502__DOT__state))
						   ? 
						  ((8U 
						    & (IData)(vlTOPp->mos6502__DOT__state))
						    ? 
						   ((4U 
						     & (IData)(vlTOPp->mos6502__DOT__state))
						     ? (IData)(vlTOPp->mos6502__DOT__din_mux)
						     : 
						    ((2U 
						      & (IData)(vlTOPp->mos6502__DOT__state))
						      ? (IData)(vlTOPp->mos6502__DOT__din_mux)
						      : 
						     ((1U 
						       & (IData)(vlTOPp->mos6502__DOT__state))
						       ? 0U
						       : (IData)(vlTOPp->mos6502__DOT__din_mux))))
						    : (IData)(vlTOPp->mos6502__DOT__din_mux))
						   : 
						  ((8U 
						    & (IData)(vlTOPp->mos6502__DOT__state))
						    ? 
						   ((4U 
						     & (IData)(vlTOPp->mos6502__DOT__state))
						     ? (IData)(vlTOPp->mos6502__DOT__din_mux)
						     : 
						    ((2U 
						      & (IData)(vlTOPp->mos6502__DOT__state))
						      ? (IData)(vlTOPp->mos6502__DOT__din_mux)
						      : 
						     ((1U 
						       & (IData)(vlTOPp->mos6502__DOT__state))
						       ? 0U
						       : (IData)(vlTOPp->mos6502__DOT__din_mux))))
						    : (IData)(vlTOPp->mos6502__DOT__din_mux)))
						  : 
						 ((0x10U 
						   & (IData)(vlTOPp->mos6502__DOT__state))
						   ? 
						  ((4U 
						    & (IData)(vlTOPp->mos6502__DOT__state))
						    ? (IData)(vlTOPp->mos6502__DOT__din_mux)
						    : 
						   ((2U 
						     & (IData)(vlTOPp->mos6502__DOT__state))
						     ? (IData)(vlTOPp->mos6502__DOT__din_mux)
						     : 
						    ((1U 
						      & (IData)(vlTOPp->mos6502__DOT__state))
						      ? (IData)(vlTOPp->mos6502__DOT__din_mux)
						      : 0U)))
						   : 
						  ((8U 
						    & (IData)(vlTOPp->mos6502__DOT__state))
						    ? 
						   ((4U 
						     & (IData)(vlTOPp->mos6502__DOT__state))
						     ? (IData)(vlTOPp->mos6502__DOT__din_mux)
						     : 
						    ((2U 
						      & (IData)(vlTOPp->mos6502__DOT__state))
						      ? (IData)(vlTOPp->mos6502__DOT__din_mux)
						      : 
						     ((1U 
						       & (IData)(vlTOPp->mos6502__DOT__state))
						       ? (IData)(vlTOPp->mos6502__DOT__din_mux)
						       : 0U)))
						    : (IData)(vlTOPp->mos6502__DOT__din_mux)))))));
    vlTOPp->mos6502__DOT__reg_val = vlTOPp->mos6502__DOT__AXYS
	[vlTOPp->mos6502__DOT__reg_idx];
    // ALWAYS at mos6502/data_out.v:1
    vlTOPp->wr_en = (1U & ((0x80U & (IData)(vlTOPp->mos6502__DOT__state))
			    ? ((~ ((IData)(vlTOPp->mos6502__DOT__state) 
				   >> 6U)) & ((~ ((IData)(vlTOPp->mos6502__DOT__state) 
						  >> 5U)) 
					      & (((IData)(vlTOPp->mos6502__DOT__state) 
						  >> 4U) 
						 & ((~ 
						     ((IData)(vlTOPp->mos6502__DOT__state) 
						      >> 3U)) 
						    & ((~ 
							((IData)(vlTOPp->mos6502__DOT__state) 
							 >> 2U)) 
						       & ((~ 
							   ((IData)(vlTOPp->mos6502__DOT__state) 
							    >> 1U)) 
							  & (IData)(vlTOPp->mos6502__DOT__state)))))))
			    : ((0x40U & (IData)(vlTOPp->mos6502__DOT__state))
			        ? ((0x20U & (IData)(vlTOPp->mos6502__DOT__state))
				    ? ((~ ((IData)(vlTOPp->mos6502__DOT__state) 
					   >> 4U)) 
				       & ((8U & (IData)(vlTOPp->mos6502__DOT__state))
					   ? ((~ ((IData)(vlTOPp->mos6502__DOT__state) 
						  >> 2U)) 
					      & (~ 
						 ((IData)(vlTOPp->mos6502__DOT__state) 
						  >> 1U)))
					   : ((~ ((IData)(vlTOPp->mos6502__DOT__state) 
						  >> 2U)) 
					      & ((~ 
						  ((IData)(vlTOPp->mos6502__DOT__state) 
						   >> 1U)) 
						 | (~ (IData)(vlTOPp->mos6502__DOT__state))))))
				    : ((0x10U & (IData)(vlTOPp->mos6502__DOT__state))
				        ? ((~ ((IData)(vlTOPp->mos6502__DOT__state) 
					       >> 3U)) 
					   & ((~ ((IData)(vlTOPp->mos6502__DOT__state) 
						  >> 2U)) 
					      & ((~ 
						  ((IData)(vlTOPp->mos6502__DOT__state) 
						   >> 1U)) 
						 & ((IData)(vlTOPp->mos6502__DOT__state) 
						    & (IData)(vlTOPp->mos6502__DOT__store)))))
				        : ((8U & (IData)(vlTOPp->mos6502__DOT__state))
					    ? ((~ ((IData)(vlTOPp->mos6502__DOT__state) 
						   >> 2U)) 
					       & ((~ 
						   ((IData)(vlTOPp->mos6502__DOT__state) 
						    >> 1U)) 
						  & ((~ (IData)(vlTOPp->mos6502__DOT__state)) 
						     & (IData)(vlTOPp->mos6502__DOT__store))))
					    : ((~ ((IData)(vlTOPp->mos6502__DOT__state) 
						   >> 2U)) 
					       & (((IData)(vlTOPp->mos6502__DOT__state) 
						   >> 1U) 
						  & ((IData)(vlTOPp->mos6502__DOT__state) 
						     & (IData)(vlTOPp->mos6502__DOT__store)))))))
			        : (((IData)(vlTOPp->mos6502__DOT__state) 
				    >> 5U) & ((0x10U 
					       & (IData)(vlTOPp->mos6502__DOT__state))
					       ? ((8U 
						   & (IData)(vlTOPp->mos6502__DOT__state))
						   ? 
						  ((~ 
						    ((IData)(vlTOPp->mos6502__DOT__state) 
						     >> 2U)) 
						   & (((IData)(vlTOPp->mos6502__DOT__state) 
						       >> 1U) 
						      & ((IData)(vlTOPp->mos6502__DOT__state) 
							 & (IData)(vlTOPp->mos6502__DOT__store))))
						   : 
						  ((~ 
						    ((IData)(vlTOPp->mos6502__DOT__state) 
						     >> 2U)) 
						   & (((IData)(vlTOPp->mos6502__DOT__state) 
						       >> 1U) 
						      & ((~ (IData)(vlTOPp->mos6502__DOT__state)) 
							 & (IData)(vlTOPp->mos6502__DOT__store)))))
					       : ((8U 
						   & (IData)(vlTOPp->mos6502__DOT__state))
						   ? 
						  ((~ 
						    ((IData)(vlTOPp->mos6502__DOT__state) 
						     >> 2U)) 
						   & ((~ 
						       ((IData)(vlTOPp->mos6502__DOT__state) 
							>> 1U)) 
						      & ((IData)(vlTOPp->mos6502__DOT__state) 
							 & (IData)(vlTOPp->mos6502__DOT__store))))
						   : 
						  ((~ 
						    ((IData)(vlTOPp->mos6502__DOT__state) 
						     >> 2U)) 
						   & ((~ 
						       ((IData)(vlTOPp->mos6502__DOT__state) 
							>> 1U)) 
						      & (~ (IData)(vlTOPp->mos6502__DOT__state))))))))));
    vlTOPp->dout = (0xffU & ((0x80U & (IData)(vlTOPp->mos6502__DOT__state))
			      ? ((0x40U & (IData)(vlTOPp->mos6502__DOT__state))
				  ? (IData)(vlTOPp->mos6502__DOT__reg_val)
				  : ((0x20U & (IData)(vlTOPp->mos6502__DOT__state))
				      ? (IData)(vlTOPp->mos6502__DOT__reg_val)
				      : ((0x10U & (IData)(vlTOPp->mos6502__DOT__state))
					  ? ((8U & (IData)(vlTOPp->mos6502__DOT__state))
					      ? (IData)(vlTOPp->mos6502__DOT__reg_val)
					      : ((4U 
						  & (IData)(vlTOPp->mos6502__DOT__state))
						  ? (IData)(vlTOPp->mos6502__DOT__reg_val)
						  : 
						 ((2U 
						   & (IData)(vlTOPp->mos6502__DOT__state))
						   ? (IData)(vlTOPp->mos6502__DOT__reg_val)
						   : 
						  ((1U 
						    & (IData)(vlTOPp->mos6502__DOT__state))
						    ? 
						   ((IData)(vlTOPp->mos6502__DOT__php)
						     ? (IData)(vlTOPp->mos6502__DOT__sr_flags)
						     : (IData)(vlTOPp->mos6502__DOT__alu_out))
						    : (IData)(vlTOPp->mos6502__DOT__reg_val)))))
					  : (IData)(vlTOPp->mos6502__DOT__reg_val))))
			      : ((0x40U & (IData)(vlTOPp->mos6502__DOT__state))
				  ? ((0x20U & (IData)(vlTOPp->mos6502__DOT__state))
				      ? ((0x10U & (IData)(vlTOPp->mos6502__DOT__state))
					  ? (IData)(vlTOPp->mos6502__DOT__reg_val)
					  : ((8U & (IData)(vlTOPp->mos6502__DOT__state))
					      ? ((4U 
						  & (IData)(vlTOPp->mos6502__DOT__state))
						  ? (IData)(vlTOPp->mos6502__DOT__reg_val)
						  : 
						 ((2U 
						   & (IData)(vlTOPp->mos6502__DOT__state))
						   ? (IData)(vlTOPp->mos6502__DOT__reg_val)
						   : 
						  ((1U 
						    & (IData)(vlTOPp->mos6502__DOT__state))
						    ? (IData)(vlTOPp->mos6502__DOT__pc)
						    : 
						   ((IData)(vlTOPp->mos6502__DOT__pc) 
						    >> 8U))))
					      : ((4U 
						  & (IData)(vlTOPp->mos6502__DOT__state))
						  ? (IData)(vlTOPp->mos6502__DOT__reg_val)
						  : 
						 ((2U 
						   & (IData)(vlTOPp->mos6502__DOT__state))
						   ? 
						  ((1U 
						    & (IData)(vlTOPp->mos6502__DOT__state))
						    ? (IData)(vlTOPp->mos6502__DOT__reg_val)
						    : 
						   (((IData)(vlTOPp->irq) 
						     | (IData)(vlTOPp->mos6502__DOT__nmi_edge))
						     ? 
						    (0xefU 
						     & (IData)(vlTOPp->mos6502__DOT__sr_flags))
						     : (IData)(vlTOPp->mos6502__DOT__sr_flags)))
						   : 
						  ((1U 
						    & (IData)(vlTOPp->mos6502__DOT__state))
						    ? (IData)(vlTOPp->mos6502__DOT__pc)
						    : 
						   ((IData)(vlTOPp->mos6502__DOT__pc) 
						    >> 8U))))))
				      : (IData)(vlTOPp->mos6502__DOT__reg_val))
				  : ((0x20U & (IData)(vlTOPp->mos6502__DOT__state))
				      ? ((0x10U & (IData)(vlTOPp->mos6502__DOT__state))
					  ? (IData)(vlTOPp->mos6502__DOT__reg_val)
					  : ((8U & (IData)(vlTOPp->mos6502__DOT__state))
					      ? (IData)(vlTOPp->mos6502__DOT__reg_val)
					      : ((4U 
						  & (IData)(vlTOPp->mos6502__DOT__state))
						  ? (IData)(vlTOPp->mos6502__DOT__reg_val)
						  : 
						 ((2U 
						   & (IData)(vlTOPp->mos6502__DOT__state))
						   ? (IData)(vlTOPp->mos6502__DOT__reg_val)
						   : 
						  ((1U 
						    & (IData)(vlTOPp->mos6502__DOT__state))
						    ? (IData)(vlTOPp->mos6502__DOT__reg_val)
						    : (IData)(vlTOPp->mos6502__DOT__alu_out))))))
				      : (IData)(vlTOPp->mos6502__DOT__reg_val)))));
    vlTOPp->addr = ((0x80U & (IData)(vlTOPp->mos6502__DOT__state))
		     ? ((0x40U & (IData)(vlTOPp->mos6502__DOT__state))
			 ? (IData)(vlTOPp->mos6502__DOT__pc)
			 : ((0x20U & (IData)(vlTOPp->mos6502__DOT__state))
			     ? (IData)(vlTOPp->mos6502__DOT__pc)
			     : ((0x10U & (IData)(vlTOPp->mos6502__DOT__state))
				 ? ((8U & (IData)(vlTOPp->mos6502__DOT__state))
				     ? ((4U & (IData)(vlTOPp->mos6502__DOT__state))
					 ? (IData)(vlTOPp->mos6502__DOT__pc)
					 : ((2U & (IData)(vlTOPp->mos6502__DOT__state))
					     ? (IData)(vlTOPp->mos6502__DOT__pc)
					     : ((1U 
						 & (IData)(vlTOPp->mos6502__DOT__state))
						 ? 
						(0x100U 
						 | (IData)(vlTOPp->mos6502__DOT__alu_out))
						 : (IData)(vlTOPp->mos6502__DOT__pc))))
				     : ((4U & (IData)(vlTOPp->mos6502__DOT__state))
					 ? (IData)(vlTOPp->mos6502__DOT__pc)
					 : ((2U & (IData)(vlTOPp->mos6502__DOT__state))
					     ? (IData)(vlTOPp->mos6502__DOT__pc)
					     : ((1U 
						 & (IData)(vlTOPp->mos6502__DOT__state))
						 ? 
						(0x100U 
						 | (IData)(vlTOPp->mos6502__DOT__reg_val))
						 : (IData)(vlTOPp->mos6502__DOT__pc)))))
				 : ((8U & (IData)(vlTOPp->mos6502__DOT__state))
				     ? ((4U & (IData)(vlTOPp->mos6502__DOT__state))
					 ? (IData)(vlTOPp->mos6502__DOT__pc)
					 : ((2U & (IData)(vlTOPp->mos6502__DOT__state))
					     ? ((1U 
						 & (IData)(vlTOPp->mos6502__DOT__state))
						 ? (IData)(vlTOPp->mos6502__DOT__pc)
						 : 
						(0x100U 
						 | (IData)(vlTOPp->mos6502__DOT__alu_out)))
					     : ((1U 
						 & (IData)(vlTOPp->mos6502__DOT__state))
						 ? 
						(0x100U 
						 | (IData)(vlTOPp->mos6502__DOT__alu_out))
						 : 
						(0x100U 
						 | (IData)(vlTOPp->mos6502__DOT__reg_val)))))
				     : ((4U & (IData)(vlTOPp->mos6502__DOT__state))
					 ? ((2U & (IData)(vlTOPp->mos6502__DOT__state))
					     ? (IData)(vlTOPp->mos6502__DOT__pc)
					     : ((1U 
						 & (IData)(vlTOPp->mos6502__DOT__state))
						 ? (IData)(vlTOPp->mos6502__DOT__pc)
						 : 
						(((IData)(vlTOPp->mos6502__DOT__din_mux) 
						  << 8U) 
						 | (IData)(vlTOPp->mos6502__DOT__alu_out))))
					 : ((2U & (IData)(vlTOPp->mos6502__DOT__state))
					     ? (0x100U 
						| (IData)(vlTOPp->mos6502__DOT__alu_out))
					     : ((1U 
						 & (IData)(vlTOPp->mos6502__DOT__state))
						 ? 
						(0x100U 
						 | (IData)(vlTOPp->mos6502__DOT__alu_out))
						 : 
						(0x100U 
						 | (IData)(vlTOPp->mos6502__DOT__reg_val)))))))))
		     : ((0x40U & (IData)(vlTOPp->mos6502__DOT__state))
			 ? ((0x20U & (IData)(vlTOPp->mos6502__DOT__state))
			     ? ((0x10U & (IData)(vlTOPp->mos6502__DOT__state))
				 ? ((4U & (IData)(vlTOPp->mos6502__DOT__state))
				     ? (IData)(vlTOPp->mos6502__DOT__pc)
				     : ((2U & (IData)(vlTOPp->mos6502__DOT__state))
					 ? (IData)(vlTOPp->mos6502__DOT__pc)
					 : ((1U & (IData)(vlTOPp->mos6502__DOT__state))
					     ? (((IData)(vlTOPp->mos6502__DOT__din_mux) 
						 << 8U) 
						| (IData)(vlTOPp->mos6502__DOT__alu_out))
					     : (IData)(vlTOPp->mos6502__DOT__pc))))
				 : ((8U & (IData)(vlTOPp->mos6502__DOT__state))
				     ? ((4U & (IData)(vlTOPp->mos6502__DOT__state))
					 ? (IData)(vlTOPp->mos6502__DOT__pc)
					 : ((2U & (IData)(vlTOPp->mos6502__DOT__state))
					     ? (IData)(vlTOPp->mos6502__DOT__pc)
					     : ((1U 
						 & (IData)(vlTOPp->mos6502__DOT__state))
						 ? 
						(0x100U 
						 | (IData)(vlTOPp->mos6502__DOT__alu_out))
						 : 
						(0x100U 
						 | (IData)(vlTOPp->mos6502__DOT__reg_val)))))
				     : ((4U & (IData)(vlTOPp->mos6502__DOT__state))
					 ? (IData)(vlTOPp->mos6502__DOT__pc)
					 : ((2U & (IData)(vlTOPp->mos6502__DOT__state))
					     ? ((1U 
						 & (IData)(vlTOPp->mos6502__DOT__state))
						 ? (IData)(vlTOPp->mos6502__DOT__pc)
						 : 
						(0x100U 
						 | (IData)(vlTOPp->mos6502__DOT__alu_out)))
					     : ((1U 
						 & (IData)(vlTOPp->mos6502__DOT__state))
						 ? 
						(0x100U 
						 | (IData)(vlTOPp->mos6502__DOT__alu_out))
						 : 
						(0x100U 
						 | (IData)(vlTOPp->mos6502__DOT__reg_val)))))))
			     : ((0x10U & (IData)(vlTOPp->mos6502__DOT__state))
				 ? ((8U & (IData)(vlTOPp->mos6502__DOT__state))
				     ? ((4U & (IData)(vlTOPp->mos6502__DOT__state))
					 ? (IData)(vlTOPp->mos6502__DOT__pc)
					 : ((2U & (IData)(vlTOPp->mos6502__DOT__state))
					     ? ((1U 
						 & (IData)(vlTOPp->mos6502__DOT__state))
						 ? (IData)(vlTOPp->mos6502__DOT__pc)
						 : 
						(((IData)(vlTOPp->mos6502__DOT__alu_out) 
						  << 8U) 
						 | (IData)(vlTOPp->mos6502__DOT__addr_l)))
					     : ((1U 
						 & (IData)(vlTOPp->mos6502__DOT__state))
						 ? 
						(((IData)(vlTOPp->mos6502__DOT__addr_h) 
						  << 8U) 
						 | (IData)(vlTOPp->mos6502__DOT__alu_out))
						 : (IData)(vlTOPp->mos6502__DOT__pc))))
				     : ((4U & (IData)(vlTOPp->mos6502__DOT__state))
					 ? (IData)(vlTOPp->mos6502__DOT__pc)
					 : ((2U & (IData)(vlTOPp->mos6502__DOT__state))
					     ? (IData)(vlTOPp->mos6502__DOT__pc)
					     : ((1U 
						 & (IData)(vlTOPp->mos6502__DOT__state))
						 ? (IData)(vlTOPp->mos6502__DOT__alu_out)
						 : (IData)(vlTOPp->mos6502__DOT__pc)))))
				 : ((8U & (IData)(vlTOPp->mos6502__DOT__state))
				     ? ((4U & (IData)(vlTOPp->mos6502__DOT__state))
					 ? (IData)(vlTOPp->mos6502__DOT__pc)
					 : ((2U & (IData)(vlTOPp->mos6502__DOT__state))
					     ? (IData)(vlTOPp->mos6502__DOT__pc)
					     : ((1U 
						 & (IData)(vlTOPp->mos6502__DOT__state))
						 ? (IData)(vlTOPp->mos6502__DOT__pc)
						 : (IData)(vlTOPp->mos6502__DOT__din_mux))))
				     : ((4U & (IData)(vlTOPp->mos6502__DOT__state))
					 ? (IData)(vlTOPp->mos6502__DOT__pc)
					 : ((2U & (IData)(vlTOPp->mos6502__DOT__state))
					     ? ((1U 
						 & (IData)(vlTOPp->mos6502__DOT__state))
						 ? 
						(((IData)(vlTOPp->mos6502__DOT__alu_out) 
						  << 8U) 
						 | (IData)(vlTOPp->mos6502__DOT__addr_l))
						 : 
						(((IData)(vlTOPp->mos6502__DOT__din_mux) 
						  << 8U) 
						 | (IData)(vlTOPp->mos6502__DOT__alu_out)))
					     : ((1U 
						 & (IData)(vlTOPp->mos6502__DOT__state))
						 ? (IData)(vlTOPp->mos6502__DOT__alu_out)
						 : (IData)(vlTOPp->mos6502__DOT__din_mux)))))))
			 : ((0x20U & (IData)(vlTOPp->mos6502__DOT__state))
			     ? ((0x10U & (IData)(vlTOPp->mos6502__DOT__state))
				 ? ((8U & (IData)(vlTOPp->mos6502__DOT__state))
				     ? ((4U & (IData)(vlTOPp->mos6502__DOT__state))
					 ? (IData)(vlTOPp->mos6502__DOT__pc)
					 : ((2U & (IData)(vlTOPp->mos6502__DOT__state))
					     ? ((1U 
						 & (IData)(vlTOPp->mos6502__DOT__state))
						 ? 
						(((IData)(vlTOPp->mos6502__DOT__din_mux) 
						  << 8U) 
						 | (IData)(vlTOPp->mos6502__DOT__alu_out))
						 : (IData)(vlTOPp->mos6502__DOT__alu_out))
					     : ((1U 
						 & (IData)(vlTOPp->mos6502__DOT__state))
						 ? (IData)(vlTOPp->mos6502__DOT__alu_out)
						 : (IData)(vlTOPp->mos6502__DOT__pc))))
				     : ((4U & (IData)(vlTOPp->mos6502__DOT__state))
					 ? (IData)(vlTOPp->mos6502__DOT__pc)
					 : ((2U & (IData)(vlTOPp->mos6502__DOT__state))
					     ? ((1U 
						 & (IData)(vlTOPp->mos6502__DOT__state))
						 ? (IData)(vlTOPp->mos6502__DOT__pc)
						 : 
						(((IData)(vlTOPp->mos6502__DOT__alu_out) 
						  << 8U) 
						 | (IData)(vlTOPp->mos6502__DOT__addr_l)))
					     : ((1U 
						 & (IData)(vlTOPp->mos6502__DOT__state))
						 ? 
						(((IData)(vlTOPp->mos6502__DOT__din_mux) 
						  << 8U) 
						 | (IData)(vlTOPp->mos6502__DOT__alu_out))
						 : (IData)(vlTOPp->mos6502__DOT__pc)))))
				 : ((8U & (IData)(vlTOPp->mos6502__DOT__state))
				     ? ((4U & (IData)(vlTOPp->mos6502__DOT__state))
					 ? (IData)(vlTOPp->mos6502__DOT__pc)
					 : ((2U & (IData)(vlTOPp->mos6502__DOT__state))
					     ? (IData)(vlTOPp->mos6502__DOT__pc)
					     : ((1U 
						 & (IData)(vlTOPp->mos6502__DOT__state))
						 ? 
						(((IData)(vlTOPp->mos6502__DOT__din_mux) 
						  << 8U) 
						 | (IData)(vlTOPp->mos6502__DOT__alu_out))
						 : (IData)(vlTOPp->mos6502__DOT__pc))))
				     : ((4U & (IData)(vlTOPp->mos6502__DOT__state))
					 ? (IData)(vlTOPp->mos6502__DOT__pc)
					 : ((2U & (IData)(vlTOPp->mos6502__DOT__state))
					     ? (IData)(vlTOPp->mos6502__DOT__pc)
					     : ((1U 
						 & (IData)(vlTOPp->mos6502__DOT__state))
						 ? (IData)(vlTOPp->mos6502__DOT__pc)
						 : 
						(((IData)(vlTOPp->mos6502__DOT__addr_h) 
						  << 8U) 
						 | (IData)(vlTOPp->mos6502__DOT__addr_l)))))))
			     : ((0x10U & (IData)(vlTOPp->mos6502__DOT__state))
				 ? ((4U & (IData)(vlTOPp->mos6502__DOT__state))
				     ? (IData)(vlTOPp->mos6502__DOT__pc)
				     : ((2U & (IData)(vlTOPp->mos6502__DOT__state))
					 ? (IData)(vlTOPp->mos6502__DOT__pc)
					 : ((1U & (IData)(vlTOPp->mos6502__DOT__state))
					     ? (IData)(vlTOPp->mos6502__DOT__pc)
					     : (((IData)(vlTOPp->mos6502__DOT__addr_h) 
						 << 8U) 
						| (IData)(vlTOPp->mos6502__DOT__addr_l)))))
				 : (IData)(vlTOPp->mos6502__DOT__pc)))));
    // ALWAYS at mos6502/alu_control.v:83
    vlTOPp->mos6502__DOT__alu_A = (0xffU & ((0x80U 
					     & (IData)(vlTOPp->mos6502__DOT__state))
					     ? ((0x40U 
						 & (IData)(vlTOPp->mos6502__DOT__state))
						 ? 0U
						 : 
						((0x20U 
						  & (IData)(vlTOPp->mos6502__DOT__state))
						  ? 0U
						  : 
						 ((0x10U 
						   & (IData)(vlTOPp->mos6502__DOT__state))
						   ? 
						  ((8U 
						    & (IData)(vlTOPp->mos6502__DOT__state))
						    ? 
						   ((4U 
						     & (IData)(vlTOPp->mos6502__DOT__state))
						     ? 0U
						     : 
						    ((2U 
						      & (IData)(vlTOPp->mos6502__DOT__state))
						      ? 0U
						      : 
						     ((1U 
						       & (IData)(vlTOPp->mos6502__DOT__state))
						       ? 0U
						       : (IData)(vlTOPp->mos6502__DOT__reg_val))))
						    : 
						   ((4U 
						     & (IData)(vlTOPp->mos6502__DOT__state))
						     ? 0U
						     : 
						    ((2U 
						      & (IData)(vlTOPp->mos6502__DOT__state))
						      ? 0U
						      : (IData)(vlTOPp->mos6502__DOT__reg_val))))
						   : 
						  ((8U 
						    & (IData)(vlTOPp->mos6502__DOT__state))
						    ? 
						   ((4U 
						     & (IData)(vlTOPp->mos6502__DOT__state))
						     ? 0U
						     : 
						    ((2U 
						      & (IData)(vlTOPp->mos6502__DOT__state))
						      ? 0U
						      : 
						     ((1U 
						       & (IData)(vlTOPp->mos6502__DOT__state))
						       ? (IData)(vlTOPp->mos6502__DOT__alu_out)
						       : (IData)(vlTOPp->mos6502__DOT__reg_val))))
						    : 
						   ((4U 
						     & (IData)(vlTOPp->mos6502__DOT__state))
						     ? 0U
						     : 
						    ((2U 
						      & (IData)(vlTOPp->mos6502__DOT__state))
						      ? 
						     ((1U 
						       & (IData)(vlTOPp->mos6502__DOT__state))
						       ? 0U
						       : (IData)(vlTOPp->mos6502__DOT__alu_out))
						      : 
						     ((1U 
						       & (IData)(vlTOPp->mos6502__DOT__state))
						       ? (IData)(vlTOPp->mos6502__DOT__alu_out)
						       : (IData)(vlTOPp->mos6502__DOT__reg_val))))))))
					     : ((0x40U 
						 & (IData)(vlTOPp->mos6502__DOT__state))
						 ? 
						((0x20U 
						  & (IData)(vlTOPp->mos6502__DOT__state))
						  ? 
						 ((0x10U 
						   & (IData)(vlTOPp->mos6502__DOT__state))
						   ? 0U
						   : 
						  ((8U 
						    & (IData)(vlTOPp->mos6502__DOT__state))
						    ? 
						   ((4U 
						     & (IData)(vlTOPp->mos6502__DOT__state))
						     ? 0U
						     : 
						    ((2U 
						      & (IData)(vlTOPp->mos6502__DOT__state))
						      ? 
						     ((1U 
						       & (IData)(vlTOPp->mos6502__DOT__state))
						       ? 0U
						       : (IData)(vlTOPp->mos6502__DOT__reg_val))
						      : 
						     ((1U 
						       & (IData)(vlTOPp->mos6502__DOT__state))
						       ? (IData)(vlTOPp->mos6502__DOT__alu_out)
						       : (IData)(vlTOPp->mos6502__DOT__reg_val))))
						    : 
						   ((4U 
						     & (IData)(vlTOPp->mos6502__DOT__state))
						     ? 0U
						     : 
						    ((2U 
						      & (IData)(vlTOPp->mos6502__DOT__state))
						      ? 
						     ((1U 
						       & (IData)(vlTOPp->mos6502__DOT__state))
						       ? 0U
						       : (IData)(vlTOPp->mos6502__DOT__alu_out))
						      : 
						     ((1U 
						       & (IData)(vlTOPp->mos6502__DOT__state))
						       ? (IData)(vlTOPp->mos6502__DOT__alu_out)
						       : (IData)(vlTOPp->mos6502__DOT__reg_val))))))
						  : 
						 ((0x10U 
						   & (IData)(vlTOPp->mos6502__DOT__state))
						   ? 
						  ((8U 
						    & (IData)(vlTOPp->mos6502__DOT__state))
						    ? 
						   ((4U 
						     & (IData)(vlTOPp->mos6502__DOT__state))
						     ? 0U
						     : 
						    ((2U 
						      & (IData)(vlTOPp->mos6502__DOT__state))
						      ? 0U
						      : 
						     ((1U 
						       & (IData)(vlTOPp->mos6502__DOT__state))
						       ? (IData)(vlTOPp->mos6502__DOT__addr_h)
						       : (IData)(vlTOPp->mos6502__DOT__din_mux))))
						    : 
						   ((4U 
						     & (IData)(vlTOPp->mos6502__DOT__state))
						     ? 0U
						     : 
						    ((2U 
						      & (IData)(vlTOPp->mos6502__DOT__state))
						      ? 0U
						      : 
						     ((1U 
						       & (IData)(vlTOPp->mos6502__DOT__state))
						       ? 0U
						       : (IData)(vlTOPp->mos6502__DOT__reg_val)))))
						   : 
						  ((8U 
						    & (IData)(vlTOPp->mos6502__DOT__state))
						    ? 0U
						    : 
						   ((4U 
						     & (IData)(vlTOPp->mos6502__DOT__state))
						     ? 0U
						     : 
						    ((2U 
						      & (IData)(vlTOPp->mos6502__DOT__state))
						      ? 0U
						      : 
						     ((1U 
						       & (IData)(vlTOPp->mos6502__DOT__state))
						       ? (IData)(vlTOPp->mos6502__DOT__reg_val)
						       : 0U))))))
						 : 
						((0x20U 
						  & (IData)(vlTOPp->mos6502__DOT__state))
						  ? 
						 ((0x10U 
						   & (IData)(vlTOPp->mos6502__DOT__state))
						   ? 
						  ((8U 
						    & (IData)(vlTOPp->mos6502__DOT__state))
						    ? 
						   ((4U 
						     & (IData)(vlTOPp->mos6502__DOT__state))
						     ? 0U
						     : 
						    ((2U 
						      & (IData)(vlTOPp->mos6502__DOT__state))
						      ? 0U
						      : 
						     ((1U 
						       & (IData)(vlTOPp->mos6502__DOT__state))
						       ? (IData)(vlTOPp->mos6502__DOT__alu_out)
						       : (IData)(vlTOPp->mos6502__DOT__reg_val))))
						    : 
						   ((4U 
						     & (IData)(vlTOPp->mos6502__DOT__state))
						     ? 0U
						     : 
						    ((2U 
						      & (IData)(vlTOPp->mos6502__DOT__state))
						      ? 0U
						      : 
						     ((1U 
						       & (IData)(vlTOPp->mos6502__DOT__state))
						       ? 0U
						       : (IData)(vlTOPp->mos6502__DOT__reg_val)))))
						   : 0U)
						  : 
						 ((0x10U 
						   & (IData)(vlTOPp->mos6502__DOT__state))
						   ? 
						  ((8U 
						    & (IData)(vlTOPp->mos6502__DOT__state))
						    ? 
						   ((4U 
						     & (IData)(vlTOPp->mos6502__DOT__state))
						     ? 0U
						     : 
						    ((2U 
						      & (IData)(vlTOPp->mos6502__DOT__state))
						      ? 0U
						      : 
						     ((1U 
						       & (IData)(vlTOPp->mos6502__DOT__state))
						       ? 0U
						       : (IData)(vlTOPp->mos6502__DOT__din_mux))))
						    : 
						   ((4U 
						     & (IData)(vlTOPp->mos6502__DOT__state))
						     ? 0U
						     : 
						    ((2U 
						      & (IData)(vlTOPp->mos6502__DOT__state))
						      ? 0U
						      : 
						     ((1U 
						       & (IData)(vlTOPp->mos6502__DOT__state))
						       ? 0U
						       : (IData)(vlTOPp->mos6502__DOT__reg_val)))))
						   : 
						  ((8U 
						    & (IData)(vlTOPp->mos6502__DOT__state))
						    ? 0U
						    : 
						   ((4U 
						     & (IData)(vlTOPp->mos6502__DOT__state))
						     ? 0U
						     : 
						    ((2U 
						      & (IData)(vlTOPp->mos6502__DOT__state))
						      ? 0U
						      : 
						     ((1U 
						       & (IData)(vlTOPp->mos6502__DOT__state))
						       ? 0U
						       : 
						      ((IData)(vlTOPp->mos6502__DOT__load_only)
						        ? 0U
						        : (IData)(vlTOPp->mos6502__DOT__reg_val)))))))))));
    // ALWAYS at mos6502/alu.v:10
    if (vlTOPp->mos6502__DOT__alu_shift_right) {
	vlTOPp->mos6502__DOT__al_A = ((0x100U & ((IData)(vlTOPp->mos6502__DOT__alu_A) 
						 << 8U)) 
				      | (((IData)(vlTOPp->mos6502__DOT__carry_in) 
					  << 7U) | 
					 (0x7fU & ((IData)(vlTOPp->mos6502__DOT__alu_A) 
						   >> 1U))));
    }
    vlTOPp->mos6502__DOT__al_A = ((2U & (IData)(vlTOPp->mos6502__DOT__alu_op))
				   ? ((1U & (IData)(vlTOPp->mos6502__DOT__alu_op))
				       ? (IData)(vlTOPp->mos6502__DOT__alu_A)
				       : ((IData)(vlTOPp->mos6502__DOT__alu_A) 
					  ^ (IData)(vlTOPp->mos6502__DOT__alu_B)))
				   : ((1U & (IData)(vlTOPp->mos6502__DOT__alu_op))
				       ? ((IData)(vlTOPp->mos6502__DOT__alu_A) 
					  & (IData)(vlTOPp->mos6502__DOT__alu_B))
				       : ((IData)(vlTOPp->mos6502__DOT__alu_A) 
					  | (IData)(vlTOPp->mos6502__DOT__alu_B))));
    vlTOPp->mos6502__DOT__al_B = (0xffU & ((8U & (IData)(vlTOPp->mos6502__DOT__alu_op))
					    ? ((4U 
						& (IData)(vlTOPp->mos6502__DOT__alu_op))
					        ? 0U
					        : (IData)(vlTOPp->mos6502__DOT__al_A))
					    : ((4U 
						& (IData)(vlTOPp->mos6502__DOT__alu_op))
					        ? (~ (IData)(vlTOPp->mos6502__DOT__alu_B))
					        : (IData)(vlTOPp->mos6502__DOT__alu_B))));
    vlTOPp->mos6502__DOT__al_O_l = (0x1fU & (((0xfU 
					       & (IData)(vlTOPp->mos6502__DOT__al_A)) 
					      + (0xfU 
						 & (IData)(vlTOPp->mos6502__DOT__al_B))) 
					     + (1U 
						& (((IData)(vlTOPp->mos6502__DOT__alu_shift_right) 
						    | (3U 
						       == 
						       (3U 
							& ((IData)(vlTOPp->mos6502__DOT__alu_op) 
							   >> 2U))))
						    ? 0U
						    : (IData)(vlTOPp->mos6502__DOT__carry_in)))));
    vlTOPp->mos6502__DOT__al_O_h = (0x1fU & ((((IData)(vlTOPp->mos6502__DOT__al_A) 
					       >> 4U) 
					      + (0xfU 
						 & ((IData)(vlTOPp->mos6502__DOT__al_B) 
						    >> 4U))) 
					     + (IData)(vlTOPp->mos6502__DOT__adder_HC)));
    vlTOPp->mos6502__DOT__al_O = (((IData)(vlTOPp->mos6502__DOT__al_O_h) 
				   << 4U) | (0xfU & (IData)(vlTOPp->mos6502__DOT__al_O_l)));
    vlTOPp->mos6502__DOT__adder_HC = (1U & (((IData)(vlTOPp->mos6502__DOT__alu_bcd) 
					     & (5U 
						<= 
						(7U 
						 & ((IData)(vlTOPp->mos6502__DOT__al_O_l) 
						    >> 1U)))) 
					    | ((IData)(vlTOPp->mos6502__DOT__al_O_l) 
					       >> 4U)));
}

VL_INLINE_OPT void Vmos6502::_sequent__TOP__4(Vmos6502__Syms* __restrict vlSymsp) {
    VL_DEBUG_IF(VL_DBG_MSGF("+    Vmos6502::_sequent__TOP__4\n"); );
    Vmos6502* __restrict vlTOPp VL_ATTR_UNUSED = vlSymsp->TOPp;
    // Body
    // ALWAYS at mos6502/alu.v:32
    if (vlTOPp->ready) {
	vlTOPp->mos6502__DOT__alu_out = (0xffU & (IData)(vlTOPp->mos6502__DOT__al_O));
	vlTOPp->mos6502__DOT__carry_out = (1U & (((IData)(vlTOPp->mos6502__DOT__al_O) 
						  >> 8U) 
						 | ((IData)(vlTOPp->mos6502__DOT__alu_bcd) 
						    & (5U 
						       <= 
						       (7U 
							& ((IData)(vlTOPp->mos6502__DOT__al_O_h) 
							   >> 1U))))));
	vlTOPp->mos6502__DOT__alu_N = (1U & ((IData)(vlTOPp->mos6502__DOT__al_O) 
					     >> 7U));
	vlTOPp->mos6502__DOT__HC = vlTOPp->mos6502__DOT__adder_HC;
    }
    // ALWAYS at mos6502/control_signal.v:1
    vlTOPp->__Vdly__mos6502__DOT__nmi_hold = vlTOPp->nmi;
    if (((IData)(vlTOPp->mos6502__DOT__nmi_edge) & 
	 (0x63U == (IData)(vlTOPp->mos6502__DOT__state)))) {
	vlTOPp->__Vdly__mos6502__DOT__nmi_edge = 0U;
    } else {
	if (((IData)(vlTOPp->nmi) & (~ (IData)(vlTOPp->mos6502__DOT__nmi_hold)))) {
	    vlTOPp->__Vdly__mos6502__DOT__nmi_edge = 1U;
	}
    }
    if (vlTOPp->ready) {
	vlTOPp->mos6502__DOT__cond_true = (1U & ((0x80U 
						  & (IData)(vlTOPp->mos6502__DOT__ir))
						  ? 
						 ((0x40U 
						   & (IData)(vlTOPp->mos6502__DOT__ir))
						   ? 
						  ((0x20U 
						    & (IData)(vlTOPp->mos6502__DOT__ir))
						    ? (IData)(vlTOPp->mos6502__DOT__Z)
						    : 
						   (~ (IData)(vlTOPp->mos6502__DOT__Z)))
						   : 
						  ((0x20U 
						    & (IData)(vlTOPp->mos6502__DOT__ir))
						    ? (IData)(vlTOPp->mos6502__DOT__C)
						    : 
						   (~ (IData)(vlTOPp->mos6502__DOT__C))))
						  : 
						 ((0x40U 
						   & (IData)(vlTOPp->mos6502__DOT__ir))
						   ? 
						  ((0x20U 
						    & (IData)(vlTOPp->mos6502__DOT__ir))
						    ? (IData)(vlTOPp->mos6502__DOT__V)
						    : 
						   (~ (IData)(vlTOPp->mos6502__DOT__V)))
						   : 
						  ((0x20U 
						    & (IData)(vlTOPp->mos6502__DOT__ir))
						    ? (IData)(vlTOPp->mos6502__DOT__N)
						    : 
						   (~ (IData)(vlTOPp->mos6502__DOT__N))))));
	vlTOPp->mos6502__DOT__backwards = (1U & ((IData)(vlTOPp->mos6502__DOT__din_mux) 
						 >> 7U));
    }
    if (((8U == (IData)(vlTOPp->mos6502__DOT__state)) 
	 & (IData)(vlTOPp->ready))) {
	vlTOPp->mos6502__DOT__php = (8U == (IData)(vlTOPp->mos6502__DOT__ir));
	vlTOPp->mos6502__DOT__clc = (0x18U == (IData)(vlTOPp->mos6502__DOT__ir));
	vlTOPp->mos6502__DOT__plp = (0x28U == (IData)(vlTOPp->mos6502__DOT__ir));
	vlTOPp->mos6502__DOT__sec = (0x38U == (IData)(vlTOPp->mos6502__DOT__ir));
	vlTOPp->mos6502__DOT__cli = (0x58U == (IData)(vlTOPp->mos6502__DOT__ir));
	vlTOPp->mos6502__DOT__sei = (0x78U == (IData)(vlTOPp->mos6502__DOT__ir));
	vlTOPp->mos6502__DOT__clv = (0xb8U == (IData)(vlTOPp->mos6502__DOT__ir));
	vlTOPp->mos6502__DOT__cld = (0xd8U == (IData)(vlTOPp->mos6502__DOT__ir));
	vlTOPp->mos6502__DOT__sed = (0xf8U == (IData)(vlTOPp->mos6502__DOT__ir));
	vlTOPp->mos6502__DOT__op = ((0x80U & (IData)(vlTOPp->mos6502__DOT__ir))
				     ? ((0x40U & (IData)(vlTOPp->mos6502__DOT__ir))
					 ? ((0x20U 
					     & (IData)(vlTOPp->mos6502__DOT__ir))
					     ? ((0x10U 
						 & (IData)(vlTOPp->mos6502__DOT__ir))
						 ? 
						((2U 
						  & (IData)(vlTOPp->mos6502__DOT__ir))
						  ? 3U
						  : 
						 ((1U 
						   & (IData)(vlTOPp->mos6502__DOT__ir))
						   ? 7U
						   : 3U))
						 : 
						((8U 
						  & (IData)(vlTOPp->mos6502__DOT__ir))
						  ? 
						 ((4U 
						   & (IData)(vlTOPp->mos6502__DOT__ir))
						   ? 
						  ((2U 
						    & (IData)(vlTOPp->mos6502__DOT__ir))
						    ? 3U
						    : 7U)
						   : 
						  ((2U 
						    & (IData)(vlTOPp->mos6502__DOT__ir))
						    ? 3U
						    : 
						   ((1U 
						     & (IData)(vlTOPp->mos6502__DOT__ir))
						     ? 7U
						     : 3U)))
						  : 
						 ((2U 
						   & (IData)(vlTOPp->mos6502__DOT__ir))
						   ? 3U
						   : 7U)))
					     : ((0x10U 
						 & (IData)(vlTOPp->mos6502__DOT__ir))
						 ? 
						((4U 
						  & (IData)(vlTOPp->mos6502__DOT__ir))
						  ? 
						 ((2U 
						   & (IData)(vlTOPp->mos6502__DOT__ir))
						   ? 
						  ((1U 
						    & (IData)(vlTOPp->mos6502__DOT__ir))
						    ? 3U
						    : 7U)
						   : 
						  ((1U 
						    & (IData)(vlTOPp->mos6502__DOT__ir))
						    ? 7U
						    : 3U))
						  : 
						 ((2U 
						   & (IData)(vlTOPp->mos6502__DOT__ir))
						   ? 3U
						   : 
						  ((1U 
						    & (IData)(vlTOPp->mos6502__DOT__ir))
						    ? 7U
						    : 3U)))
						 : 
						((8U 
						  & (IData)(vlTOPp->mos6502__DOT__ir))
						  ? 
						 ((4U 
						   & (IData)(vlTOPp->mos6502__DOT__ir))
						   ? 
						  ((2U 
						    & (IData)(vlTOPp->mos6502__DOT__ir))
						    ? 
						   ((1U 
						     & (IData)(vlTOPp->mos6502__DOT__ir))
						     ? 3U
						     : 7U)
						    : 7U)
						   : 
						  ((2U 
						    & (IData)(vlTOPp->mos6502__DOT__ir))
						    ? 
						   ((1U 
						     & (IData)(vlTOPp->mos6502__DOT__ir))
						     ? 3U
						     : 7U)
						    : 
						   ((1U 
						     & (IData)(vlTOPp->mos6502__DOT__ir))
						     ? 7U
						     : 3U)))
						  : 
						 ((4U 
						   & (IData)(vlTOPp->mos6502__DOT__ir))
						   ? 
						  ((2U 
						    & (IData)(vlTOPp->mos6502__DOT__ir))
						    ? 
						   ((1U 
						     & (IData)(vlTOPp->mos6502__DOT__ir))
						     ? 3U
						     : 7U)
						    : 7U)
						   : 
						  ((2U 
						    & (IData)(vlTOPp->mos6502__DOT__ir))
						    ? 3U
						    : 7U)))))
					 : ((0x20U 
					     & (IData)(vlTOPp->mos6502__DOT__ir))
					     ? 3U : 
					    ((0x10U 
					      & (IData)(vlTOPp->mos6502__DOT__ir))
					      ? 3U : 
					     ((8U & (IData)(vlTOPp->mos6502__DOT__ir))
					       ? ((4U 
						   & (IData)(vlTOPp->mos6502__DOT__ir))
						   ? 3U
						   : 
						  ((2U 
						    & (IData)(vlTOPp->mos6502__DOT__ir))
						    ? 3U
						    : 
						   ((1U 
						     & (IData)(vlTOPp->mos6502__DOT__ir))
						     ? 3U
						     : 7U)))
					       : 3U))))
				     : ((0x40U & (IData)(vlTOPp->mos6502__DOT__ir))
					 ? ((0x20U 
					     & (IData)(vlTOPp->mos6502__DOT__ir))
					     ? ((2U 
						 & (IData)(vlTOPp->mos6502__DOT__ir))
						 ? 
						((1U 
						  & (IData)(vlTOPp->mos6502__DOT__ir))
						  ? 3U
						  : 0xfU)
						 : 3U)
					     : ((2U 
						 & (IData)(vlTOPp->mos6502__DOT__ir))
						 ? 
						((1U 
						  & (IData)(vlTOPp->mos6502__DOT__ir))
						  ? 3U
						  : 0xfU)
						 : 
						((1U 
						  & (IData)(vlTOPp->mos6502__DOT__ir))
						  ? 
						 (0xcU 
						  | (3U 
						     & ((IData)(vlTOPp->mos6502__DOT__ir) 
							>> 5U)))
						  : 3U)))
					 : ((0x20U 
					     & (IData)(vlTOPp->mos6502__DOT__ir))
					     ? ((0x10U 
						 & (IData)(vlTOPp->mos6502__DOT__ir))
						 ? 
						((2U 
						  & (IData)(vlTOPp->mos6502__DOT__ir))
						  ? 
						 ((1U 
						   & (IData)(vlTOPp->mos6502__DOT__ir))
						   ? 3U
						   : 0xbU)
						  : 
						 ((1U 
						   & (IData)(vlTOPp->mos6502__DOT__ir))
						   ? 
						  (0xcU 
						   | (3U 
						      & ((IData)(vlTOPp->mos6502__DOT__ir) 
							 >> 5U)))
						   : 3U))
						 : 
						((4U 
						  & (IData)(vlTOPp->mos6502__DOT__ir))
						  ? 
						 ((2U 
						   & (IData)(vlTOPp->mos6502__DOT__ir))
						   ? 
						  ((1U 
						    & (IData)(vlTOPp->mos6502__DOT__ir))
						    ? 3U
						    : 0xbU)
						   : 
						  ((1U 
						    & (IData)(vlTOPp->mos6502__DOT__ir))
						    ? 
						   (0xcU 
						    | (3U 
						       & ((IData)(vlTOPp->mos6502__DOT__ir) 
							  >> 5U)))
						    : 0xdU))
						  : 
						 ((2U 
						   & (IData)(vlTOPp->mos6502__DOT__ir))
						   ? 
						  ((1U 
						    & (IData)(vlTOPp->mos6502__DOT__ir))
						    ? 3U
						    : 0xbU)
						   : 
						  ((1U 
						    & (IData)(vlTOPp->mos6502__DOT__ir))
						    ? 
						   (0xcU 
						    | (3U 
						       & ((IData)(vlTOPp->mos6502__DOT__ir) 
							  >> 5U)))
						    : 3U))))
					     : ((2U 
						 & (IData)(vlTOPp->mos6502__DOT__ir))
						 ? 
						((1U 
						  & (IData)(vlTOPp->mos6502__DOT__ir))
						  ? 3U
						  : 0xbU)
						 : 
						((1U 
						  & (IData)(vlTOPp->mos6502__DOT__ir))
						  ? 
						 (0xcU 
						  | (3U 
						     & ((IData)(vlTOPp->mos6502__DOT__ir) 
							>> 5U)))
						  : 3U)))));
	vlTOPp->mos6502__DOT__bit_ins = (0x24U == (0xf7U 
						   & (IData)(vlTOPp->mos6502__DOT__ir)));
	vlTOPp->mos6502__DOT__store = ((0x84U == (0xe5U 
						  & (IData)(vlTOPp->mos6502__DOT__ir))) 
				       | (0x81U == 
					  (0xe3U & (IData)(vlTOPp->mos6502__DOT__ir))));
	vlTOPp->mos6502__DOT__write_back = ((6U == 
					     (0x87U 
					      & (IData)(vlTOPp->mos6502__DOT__ir))) 
					    | (0xc6U 
					       == (0xc7U 
						   & (IData)(vlTOPp->mos6502__DOT__ir))));
	vlTOPp->mos6502__DOT__reg_y = (((0x11U == (0x1fU 
						   & (IData)(vlTOPp->mos6502__DOT__ir))) 
					| (0x96U == 
					   (0xd7U & (IData)(vlTOPp->mos6502__DOT__ir)))) 
				       | (9U == (0xfU 
						 & (IData)(vlTOPp->mos6502__DOT__ir))));
	vlTOPp->mos6502__DOT__load_only = (0xa0U == 
					   (0xe0U & (IData)(vlTOPp->mos6502__DOT__ir)));
	vlTOPp->mos6502__DOT__rotate = ((0x2aU == (0xafU 
						   & (IData)(vlTOPp->mos6502__DOT__ir))) 
					| (0x26U == 
					   (0xa7U & (IData)(vlTOPp->mos6502__DOT__ir))));
	vlTOPp->mos6502__DOT__shift = ((6U == (0x87U 
					       & (IData)(vlTOPp->mos6502__DOT__ir))) 
				       | (0xaU == (0x8fU 
						   & (IData)(vlTOPp->mos6502__DOT__ir))));
	vlTOPp->mos6502__DOT__compare = (((0xc0U == 
					   (0xdbU & (IData)(vlTOPp->mos6502__DOT__ir))) 
					  | (0xccU 
					     == (0xdfU 
						 & (IData)(vlTOPp->mos6502__DOT__ir)))) 
					 | (0xc1U == 
					    (0xe3U 
					     & (IData)(vlTOPp->mos6502__DOT__ir))));
	vlTOPp->mos6502__DOT__inc = ((0xe6U == (0xe7U 
						& (IData)(vlTOPp->mos6502__DOT__ir))) 
				     | (0xc8U == (0xdfU 
						  & (IData)(vlTOPp->mos6502__DOT__ir))));
	vlTOPp->mos6502__DOT__src_reg = ((0x80U & (IData)(vlTOPp->mos6502__DOT__ir))
					  ? ((0x40U 
					      & (IData)(vlTOPp->mos6502__DOT__ir))
					      ? ((0x20U 
						  & (IData)(vlTOPp->mos6502__DOT__ir))
						  ? 
						 ((0x10U 
						   & (IData)(vlTOPp->mos6502__DOT__ir))
						   ? 0U
						   : 
						  ((2U 
						    & (IData)(vlTOPp->mos6502__DOT__ir))
						    ? 0U
						    : 
						   ((1U 
						     & (IData)(vlTOPp->mos6502__DOT__ir))
						     ? 0U
						     : 1U)))
						  : 
						 ((0x10U 
						   & (IData)(vlTOPp->mos6502__DOT__ir))
						   ? 0U
						   : 
						  ((8U 
						    & (IData)(vlTOPp->mos6502__DOT__ir))
						    ? 
						   ((4U 
						     & (IData)(vlTOPp->mos6502__DOT__ir))
						     ? 
						    ((2U 
						      & (IData)(vlTOPp->mos6502__DOT__ir))
						      ? 0U
						      : 
						     ((1U 
						       & (IData)(vlTOPp->mos6502__DOT__ir))
						       ? 0U
						       : 2U))
						     : 
						    ((2U 
						      & (IData)(vlTOPp->mos6502__DOT__ir))
						      ? 
						     ((1U 
						       & (IData)(vlTOPp->mos6502__DOT__ir))
						       ? 0U
						       : 1U)
						      : 
						     ((1U 
						       & (IData)(vlTOPp->mos6502__DOT__ir))
						       ? 0U
						       : 2U)))
						    : 
						   ((2U 
						     & (IData)(vlTOPp->mos6502__DOT__ir))
						     ? 0U
						     : 
						    ((1U 
						      & (IData)(vlTOPp->mos6502__DOT__ir))
						      ? 0U
						      : 2U)))))
					      : ((0x20U 
						  & (IData)(vlTOPp->mos6502__DOT__ir))
						  ? 
						 ((0x10U 
						   & (IData)(vlTOPp->mos6502__DOT__ir))
						   ? 
						  ((8U 
						    & (IData)(vlTOPp->mos6502__DOT__ir))
						    ? 
						   ((4U 
						     & (IData)(vlTOPp->mos6502__DOT__ir))
						     ? 0U
						     : 
						    ((2U 
						      & (IData)(vlTOPp->mos6502__DOT__ir))
						      ? 
						     ((1U 
						       & (IData)(vlTOPp->mos6502__DOT__ir))
						       ? 0U
						       : 3U)
						      : 0U))
						    : 0U)
						   : 0U)
						  : 
						 ((8U 
						   & (IData)(vlTOPp->mos6502__DOT__ir))
						   ? 
						  ((2U 
						    & (IData)(vlTOPp->mos6502__DOT__ir))
						    ? 
						   ((1U 
						     & (IData)(vlTOPp->mos6502__DOT__ir))
						     ? 0U
						     : 1U)
						    : 
						   ((1U 
						     & (IData)(vlTOPp->mos6502__DOT__ir))
						     ? 0U
						     : 2U))
						   : 
						  ((4U 
						    & (IData)(vlTOPp->mos6502__DOT__ir))
						    ? 
						   ((2U 
						     & (IData)(vlTOPp->mos6502__DOT__ir))
						     ? 
						    ((1U 
						      & (IData)(vlTOPp->mos6502__DOT__ir))
						      ? 0U
						      : 1U)
						     : 
						    ((1U 
						      & (IData)(vlTOPp->mos6502__DOT__ir))
						      ? 0U
						      : 2U))
						    : 0U))))
					  : 0U);
	vlTOPp->mos6502__DOT__dst_reg = ((0x80U & (IData)(vlTOPp->mos6502__DOT__ir))
					  ? ((0x40U 
					      & (IData)(vlTOPp->mos6502__DOT__ir))
					      ? ((0x20U 
						  & (IData)(vlTOPp->mos6502__DOT__ir))
						  ? 
						 ((0x10U 
						   & (IData)(vlTOPp->mos6502__DOT__ir))
						   ? 0U
						   : 
						  ((8U 
						    & (IData)(vlTOPp->mos6502__DOT__ir))
						    ? 
						   ((4U 
						     & (IData)(vlTOPp->mos6502__DOT__ir))
						     ? 0U
						     : 
						    ((2U 
						      & (IData)(vlTOPp->mos6502__DOT__ir))
						      ? 0U
						      : 
						     ((1U 
						       & (IData)(vlTOPp->mos6502__DOT__ir))
						       ? 0U
						       : 1U)))
						    : 0U))
						  : 
						 ((0x10U 
						   & (IData)(vlTOPp->mos6502__DOT__ir))
						   ? 0U
						   : 
						  ((8U 
						    & (IData)(vlTOPp->mos6502__DOT__ir))
						    ? 
						   ((4U 
						     & (IData)(vlTOPp->mos6502__DOT__ir))
						     ? 0U
						     : 
						    ((2U 
						      & (IData)(vlTOPp->mos6502__DOT__ir))
						      ? 
						     ((1U 
						       & (IData)(vlTOPp->mos6502__DOT__ir))
						       ? 0U
						       : 1U)
						      : 
						     ((1U 
						       & (IData)(vlTOPp->mos6502__DOT__ir))
						       ? 0U
						       : 2U)))
						    : 0U)))
					      : ((0x20U 
						  & (IData)(vlTOPp->mos6502__DOT__ir))
						  ? 
						 ((0x10U 
						   & (IData)(vlTOPp->mos6502__DOT__ir))
						   ? 
						  ((4U 
						    & (IData)(vlTOPp->mos6502__DOT__ir))
						    ? 
						   ((2U 
						     & (IData)(vlTOPp->mos6502__DOT__ir))
						     ? 
						    ((1U 
						      & (IData)(vlTOPp->mos6502__DOT__ir))
						      ? 0U
						      : 1U)
						     : 
						    ((1U 
						      & (IData)(vlTOPp->mos6502__DOT__ir))
						      ? 0U
						      : 2U))
						    : 
						   ((2U 
						     & (IData)(vlTOPp->mos6502__DOT__ir))
						     ? 
						    ((1U 
						      & (IData)(vlTOPp->mos6502__DOT__ir))
						      ? 0U
						      : 1U)
						     : 0U))
						   : 
						  ((2U 
						    & (IData)(vlTOPp->mos6502__DOT__ir))
						    ? 
						   ((1U 
						     & (IData)(vlTOPp->mos6502__DOT__ir))
						     ? 0U
						     : 1U)
						    : 
						   ((1U 
						     & (IData)(vlTOPp->mos6502__DOT__ir))
						     ? 0U
						     : 2U)))
						  : 
						 ((0x10U 
						   & (IData)(vlTOPp->mos6502__DOT__ir))
						   ? 
						  ((8U 
						    & (IData)(vlTOPp->mos6502__DOT__ir))
						    ? 
						   ((4U 
						     & (IData)(vlTOPp->mos6502__DOT__ir))
						     ? 0U
						     : 
						    ((2U 
						      & (IData)(vlTOPp->mos6502__DOT__ir))
						      ? 
						     ((1U 
						       & (IData)(vlTOPp->mos6502__DOT__ir))
						       ? 0U
						       : 3U)
						      : 0U))
						    : 0U)
						   : 
						  ((8U 
						    & (IData)(vlTOPp->mos6502__DOT__ir))
						    ? 
						   ((4U 
						     & (IData)(vlTOPp->mos6502__DOT__ir))
						     ? 0U
						     : 
						    ((2U 
						      & (IData)(vlTOPp->mos6502__DOT__ir))
						      ? 0U
						      : 
						     ((1U 
						       & (IData)(vlTOPp->mos6502__DOT__ir))
						       ? 0U
						       : 2U)))
						    : 0U))))
					  : ((0x20U 
					      & (IData)(vlTOPp->mos6502__DOT__ir))
					      ? 0U : 
					     ((0x10U 
					       & (IData)(vlTOPp->mos6502__DOT__ir))
					       ? 0U
					       : ((8U 
						   & (IData)(vlTOPp->mos6502__DOT__ir))
						   ? 
						  ((4U 
						    & (IData)(vlTOPp->mos6502__DOT__ir))
						    ? 0U
						    : 
						   ((2U 
						     & (IData)(vlTOPp->mos6502__DOT__ir))
						     ? 0U
						     : 
						    ((1U 
						      & (IData)(vlTOPp->mos6502__DOT__ir))
						      ? 0U
						      : 3U)))
						   : 0U))));
    }
    if (vlTOPp->reset) {
	vlTOPp->mos6502__DOT__res = 1U;
    } else {
	if ((8U == (IData)(vlTOPp->mos6502__DOT__state))) {
	    vlTOPp->mos6502__DOT__res = 0U;
	}
    }
    if ((((IData)(vlTOPp->ready) & (0x12U != (0x1fU 
					      & ((IData)(vlTOPp->mos6502__DOT__state) 
						 >> 3U)))) 
	 & (0x13U != (0x1fU & ((IData)(vlTOPp->mos6502__DOT__state) 
			       >> 3U))))) {
	vlTOPp->mos6502__DOT__addr_l = (0xffU & (IData)(vlTOPp->addr));
	vlTOPp->mos6502__DOT__addr_h = (0xffU & ((IData)(vlTOPp->addr) 
						 >> 8U));
    }
    vlTOPp->mos6502__DOT__nmi_hold = vlTOPp->__Vdly__mos6502__DOT__nmi_hold;
    vlTOPp->mos6502__DOT__nmi_edge = vlTOPp->__Vdly__mos6502__DOT__nmi_edge;
    vlTOPp->mos6502__DOT__Z = vlTOPp->__Vdly__mos6502__DOT__Z;
    vlTOPp->mos6502__DOT__V = vlTOPp->__Vdly__mos6502__DOT__V;
    vlTOPp->mos6502__DOT__N = vlTOPp->__Vdly__mos6502__DOT__N;
    vlTOPp->mos6502__DOT__C = vlTOPp->__Vdly__mos6502__DOT__C;
    // ALWAYS at mos6502/control_signal.v:118
    vlTOPp->mos6502__DOT__adj = ((4U == (((IData)(vlTOPp->mos6502__DOT__adj_bcd) 
					  << 2U) | 
					 (((IData)(vlTOPp->mos6502__DOT__adc_bcd) 
					   << 1U) | (IData)(vlTOPp->mos6502__DOT__HC))))
				  ? 0xaU : ((7U == 
					     (((IData)(vlTOPp->mos6502__DOT__adj_bcd) 
					       << 2U) 
					      | (((IData)(vlTOPp->mos6502__DOT__adc_bcd) 
						  << 1U) 
						 | (IData)(vlTOPp->mos6502__DOT__HC))))
					     ? 6U : 0U));
    vlTOPp->mos6502__DOT__sr_flags = (0x30U | (((IData)(vlTOPp->mos6502__DOT__N) 
						<< 7U) 
					       | (((IData)(vlTOPp->mos6502__DOT__V) 
						   << 6U) 
						  | (((IData)(vlTOPp->mos6502__DOT__D) 
						      << 3U) 
						     | (((IData)(vlTOPp->mos6502__DOT__I) 
							 << 2U) 
							| (((IData)(vlTOPp->mos6502__DOT__Z) 
							    << 1U) 
							   | (IData)(vlTOPp->mos6502__DOT__C)))))));
}

VL_INLINE_OPT void Vmos6502::_combo__TOP__5(Vmos6502__Syms* __restrict vlSymsp) {
    VL_DEBUG_IF(VL_DBG_MSGF("+    Vmos6502::_combo__TOP__5\n"); );
    Vmos6502* __restrict vlTOPp VL_ATTR_UNUSED = vlSymsp->TOPp;
    // Body
    vlTOPp->mos6502__DOT__din_mux = ((IData)(vlTOPp->ready)
				      ? (IData)(vlTOPp->din)
				      : (IData)(vlTOPp->mos6502__DOT__din_hold));
    vlTOPp->mos6502__DOT__ir = ((((~ (IData)(vlTOPp->mos6502__DOT__I)) 
				  & (IData)(vlTOPp->irq)) 
				 | (IData)(vlTOPp->mos6502__DOT__nmi_edge))
				 ? 0U : ((IData)(vlTOPp->mos6502__DOT__ir_hold_valid)
					  ? (IData)(vlTOPp->mos6502__DOT__ir_hold)
					  : (IData)(vlTOPp->mos6502__DOT__din_mux)));
}

VL_INLINE_OPT void Vmos6502::_sequent__TOP__6(Vmos6502__Syms* __restrict vlSymsp) {
    VL_DEBUG_IF(VL_DBG_MSGF("+    Vmos6502::_sequent__TOP__6\n"); );
    Vmos6502* __restrict vlTOPp VL_ATTR_UNUSED = vlSymsp->TOPp;
    // Body
    vlTOPp->mos6502__DOT__state = vlTOPp->__Vdly__mos6502__DOT__state;
    // ALWAYS at mos6502/register.v:22
    vlTOPp->__Vtableidx1 = (((IData)(vlTOPp->mos6502__DOT__plp) 
			     << 9U) | (((IData)(vlTOPp->mos6502__DOT__ld_reg) 
					<< 8U) | (IData)(vlTOPp->mos6502__DOT__state)));
    vlTOPp->mos6502__DOT__wr_reg = vlTOPp->__Vtable1_mos6502__DOT__wr_reg
	[vlTOPp->__Vtableidx1];
    vlTOPp->mos6502__DOT__alu_bcd = ((IData)(vlTOPp->mos6502__DOT__adc_bcd) 
				     & (0U == (IData)(vlTOPp->mos6502__DOT__state)));
    // ALWAYS at mos6502/alu_control.v:83
    vlTOPp->mos6502__DOT__alu_shift_right = ((((0U 
						== (IData)(vlTOPp->mos6502__DOT__state)) 
					       | (0x10U 
						  == (IData)(vlTOPp->mos6502__DOT__state))) 
					      | (0x18U 
						 == (IData)(vlTOPp->mos6502__DOT__state))) 
					     & (IData)(vlTOPp->mos6502__DOT__shift_right));
    // ALWAYS at mos6502/alu_control.v:83
    vlTOPp->mos6502__DOT__alu_op = ((0x80U & (IData)(vlTOPp->mos6502__DOT__state))
				     ? ((0x40U & (IData)(vlTOPp->mos6502__DOT__state))
					 ? 3U : ((0x20U 
						  & (IData)(vlTOPp->mos6502__DOT__state))
						  ? 3U
						  : 
						 ((0x10U 
						   & (IData)(vlTOPp->mos6502__DOT__state))
						   ? 
						  ((8U 
						    & (IData)(vlTOPp->mos6502__DOT__state))
						    ? 3U
						    : 
						   ((4U 
						     & (IData)(vlTOPp->mos6502__DOT__state))
						     ? 3U
						     : 
						    ((2U 
						      & (IData)(vlTOPp->mos6502__DOT__state))
						      ? 3U
						      : 
						     ((1U 
						       & (IData)(vlTOPp->mos6502__DOT__state))
						       ? 7U
						       : 3U))))
						   : 3U)))
				     : ((0x40U & (IData)(vlTOPp->mos6502__DOT__state))
					 ? ((0x20U 
					     & (IData)(vlTOPp->mos6502__DOT__state))
					     ? ((0x10U 
						 & (IData)(vlTOPp->mos6502__DOT__state))
						 ? 3U
						 : 
						((8U 
						  & (IData)(vlTOPp->mos6502__DOT__state))
						  ? 
						 ((4U 
						   & (IData)(vlTOPp->mos6502__DOT__state))
						   ? 3U
						   : 
						  ((2U 
						    & (IData)(vlTOPp->mos6502__DOT__state))
						    ? 3U
						    : 7U))
						  : 
						 ((4U 
						   & (IData)(vlTOPp->mos6502__DOT__state))
						   ? 3U
						   : 
						  ((2U 
						    & (IData)(vlTOPp->mos6502__DOT__state))
						    ? 
						   ((1U 
						     & (IData)(vlTOPp->mos6502__DOT__state))
						     ? 3U
						     : 7U)
						    : 7U))))
					     : ((0x10U 
						 & (IData)(vlTOPp->mos6502__DOT__state))
						 ? 
						((8U 
						  & (IData)(vlTOPp->mos6502__DOT__state))
						  ? 
						 ((4U 
						   & (IData)(vlTOPp->mos6502__DOT__state))
						   ? 3U
						   : 
						  ((2U 
						    & (IData)(vlTOPp->mos6502__DOT__state))
						    ? 3U
						    : 
						   ((1U 
						     & (IData)(vlTOPp->mos6502__DOT__state))
						     ? 
						    ((IData)(vlTOPp->mos6502__DOT__backwards)
						      ? 7U
						      : 3U)
						     : 3U)))
						  : 3U)
						 : 3U))
					 : ((0x20U 
					     & (IData)(vlTOPp->mos6502__DOT__state))
					     ? 3U : 
					    ((0x10U 
					      & (IData)(vlTOPp->mos6502__DOT__state))
					      ? ((8U 
						  & (IData)(vlTOPp->mos6502__DOT__state))
						  ? 
						 ((4U 
						   & (IData)(vlTOPp->mos6502__DOT__state))
						   ? 3U
						   : 
						  ((2U 
						    & (IData)(vlTOPp->mos6502__DOT__state))
						    ? 3U
						    : 
						   ((1U 
						     & (IData)(vlTOPp->mos6502__DOT__state))
						     ? 3U
						     : (IData)(vlTOPp->mos6502__DOT__op))))
						  : 
						 ((4U 
						   & (IData)(vlTOPp->mos6502__DOT__state))
						   ? 3U
						   : 
						  ((2U 
						    & (IData)(vlTOPp->mos6502__DOT__state))
						    ? 3U
						    : 
						   ((1U 
						     & (IData)(vlTOPp->mos6502__DOT__state))
						     ? 3U
						     : (IData)(vlTOPp->mos6502__DOT__op)))))
					      : ((8U 
						  & (IData)(vlTOPp->mos6502__DOT__state))
						  ? 3U
						  : 
						 ((4U 
						   & (IData)(vlTOPp->mos6502__DOT__state))
						   ? 3U
						   : 
						  ((2U 
						    & (IData)(vlTOPp->mos6502__DOT__state))
						    ? 3U
						    : 
						   ((1U 
						     & (IData)(vlTOPp->mos6502__DOT__state))
						     ? 3U
						     : (IData)(vlTOPp->mos6502__DOT__op)))))))));
    // ALWAYS at mos6502/alu_control.v:83
    vlTOPp->mos6502__DOT__carry_in = (1U & ((0x80U 
					     & (IData)(vlTOPp->mos6502__DOT__state))
					     ? ((~ 
						 ((IData)(vlTOPp->mos6502__DOT__state) 
						  >> 6U)) 
						& ((~ 
						    ((IData)(vlTOPp->mos6502__DOT__state) 
						     >> 5U)) 
						   & ((~ 
						       ((IData)(vlTOPp->mos6502__DOT__state) 
							>> 4U)) 
						      & ((8U 
							  & (IData)(vlTOPp->mos6502__DOT__state))
							  ? 
							 ((~ 
							   ((IData)(vlTOPp->mos6502__DOT__state) 
							    >> 2U)) 
							  & (~ 
							     ((IData)(vlTOPp->mos6502__DOT__state) 
							      >> 1U)))
							  : 
							 ((~ 
							   ((IData)(vlTOPp->mos6502__DOT__state) 
							    >> 2U)) 
							  & ((~ 
							      ((IData)(vlTOPp->mos6502__DOT__state) 
							       >> 1U)) 
							     | (~ (IData)(vlTOPp->mos6502__DOT__state))))))))
					     : ((0x40U 
						 & (IData)(vlTOPp->mos6502__DOT__state))
						 ? 
						((~ 
						  ((IData)(vlTOPp->mos6502__DOT__state) 
						   >> 5U)) 
						 & ((0x10U 
						     & (IData)(vlTOPp->mos6502__DOT__state))
						     ? 
						    (((IData)(vlTOPp->mos6502__DOT__state) 
						      >> 3U) 
						     & ((~ 
							 ((IData)(vlTOPp->mos6502__DOT__state) 
							  >> 2U)) 
							& ((~ 
							    ((IData)(vlTOPp->mos6502__DOT__state) 
							     >> 1U)) 
							   & ((IData)(vlTOPp->mos6502__DOT__state) 
							      & (IData)(vlTOPp->mos6502__DOT__carry_out)))))
						     : 
						    ((~ 
						      ((IData)(vlTOPp->mos6502__DOT__state) 
						       >> 3U)) 
						     & ((~ 
							 ((IData)(vlTOPp->mos6502__DOT__state) 
							  >> 2U)) 
							& ((2U 
							    & (IData)(vlTOPp->mos6502__DOT__state))
							    ? 
							   ((~ (IData)(vlTOPp->mos6502__DOT__state)) 
							    & (IData)(vlTOPp->mos6502__DOT__carry_out))
							    : 
							   (~ (IData)(vlTOPp->mos6502__DOT__state)))))))
						 : 
						((0x20U 
						  & (IData)(vlTOPp->mos6502__DOT__state))
						  ? 
						 (((IData)(vlTOPp->mos6502__DOT__state) 
						   >> 4U) 
						  & ((8U 
						      & (IData)(vlTOPp->mos6502__DOT__state))
						      ? 
						     ((~ 
						       ((IData)(vlTOPp->mos6502__DOT__state) 
							>> 2U)) 
						      & ((~ 
							  ((IData)(vlTOPp->mos6502__DOT__state) 
							   >> 1U)) 
							 & (IData)(vlTOPp->mos6502__DOT__state)))
						      : 
						     ((~ 
						       ((IData)(vlTOPp->mos6502__DOT__state) 
							>> 2U)) 
						      & ((~ 
							  ((IData)(vlTOPp->mos6502__DOT__state) 
							   >> 1U)) 
							 & ((IData)(vlTOPp->mos6502__DOT__state) 
							    & (IData)(vlTOPp->mos6502__DOT__carry_out))))))
						  : 
						 ((0x10U 
						   & (IData)(vlTOPp->mos6502__DOT__state))
						   ? 
						  ((~ 
						    ((IData)(vlTOPp->mos6502__DOT__state) 
						     >> 2U)) 
						   & ((~ 
						       ((IData)(vlTOPp->mos6502__DOT__state) 
							>> 1U)) 
						      & ((~ (IData)(vlTOPp->mos6502__DOT__state)) 
							 & ((IData)(vlTOPp->mos6502__DOT__rotate)
							     ? (IData)(vlTOPp->mos6502__DOT__C)
							     : 
							    ((IData)(vlTOPp->mos6502__DOT__shift)
							      ? 0U
							      : (IData)(vlTOPp->mos6502__DOT__inc))))))
						   : 
						  ((~ 
						    ((IData)(vlTOPp->mos6502__DOT__state) 
						     >> 3U)) 
						   & ((~ 
						       ((IData)(vlTOPp->mos6502__DOT__state) 
							>> 2U)) 
						      & ((~ 
							  ((IData)(vlTOPp->mos6502__DOT__state) 
							   >> 1U)) 
							 & ((~ (IData)(vlTOPp->mos6502__DOT__state)) 
							    & ((IData)(vlTOPp->mos6502__DOT__rotate)
							        ? (IData)(vlTOPp->mos6502__DOT__C)
							        : 
							       ((IData)(vlTOPp->mos6502__DOT__compare)
								 ? 1U
								 : 
								(((IData)(vlTOPp->mos6502__DOT__shift) 
								  | (IData)(vlTOPp->mos6502__DOT__load_only))
								  ? 0U
								  : (IData)(vlTOPp->mos6502__DOT__C)))))))))))));
    // ALWAYS at mos6502/control_signal.v:118
    vlTOPp->mos6502__DOT__reg_idx = ((0x80U & (IData)(vlTOPp->mos6502__DOT__state))
				      ? ((0x40U & (IData)(vlTOPp->mos6502__DOT__state))
					  ? (IData)(vlTOPp->mos6502__DOT__src_reg)
					  : ((0x20U 
					      & (IData)(vlTOPp->mos6502__DOT__state))
					      ? (IData)(vlTOPp->mos6502__DOT__src_reg)
					      : ((0x10U 
						  & (IData)(vlTOPp->mos6502__DOT__state))
						  ? 
						 ((8U 
						   & (IData)(vlTOPp->mos6502__DOT__state))
						   ? 
						  ((4U 
						    & (IData)(vlTOPp->mos6502__DOT__state))
						    ? (IData)(vlTOPp->mos6502__DOT__src_reg)
						    : 
						   ((2U 
						     & (IData)(vlTOPp->mos6502__DOT__state))
						     ? (IData)(vlTOPp->mos6502__DOT__src_reg)
						     : 3U))
						   : 
						  ((4U 
						    & (IData)(vlTOPp->mos6502__DOT__state))
						    ? (IData)(vlTOPp->mos6502__DOT__src_reg)
						    : 
						   ((2U 
						     & (IData)(vlTOPp->mos6502__DOT__state))
						     ? (IData)(vlTOPp->mos6502__DOT__src_reg)
						     : 
						    ((1U 
						      & (IData)(vlTOPp->mos6502__DOT__state))
						      ? 3U
						      : (IData)(vlTOPp->mos6502__DOT__src_reg)))))
						  : 
						 ((8U 
						   & (IData)(vlTOPp->mos6502__DOT__state))
						   ? 
						  ((4U 
						    & (IData)(vlTOPp->mos6502__DOT__state))
						    ? (IData)(vlTOPp->mos6502__DOT__src_reg)
						    : 
						   ((1U 
						     & (IData)(vlTOPp->mos6502__DOT__state))
						     ? (IData)(vlTOPp->mos6502__DOT__src_reg)
						     : 3U))
						   : 
						  ((4U 
						    & (IData)(vlTOPp->mos6502__DOT__state))
						    ? (IData)(vlTOPp->mos6502__DOT__src_reg)
						    : 
						   ((2U 
						     & (IData)(vlTOPp->mos6502__DOT__state))
						     ? 
						    ((1U 
						      & (IData)(vlTOPp->mos6502__DOT__state))
						      ? 3U
						      : (IData)(vlTOPp->mos6502__DOT__src_reg))
						     : 
						    ((1U 
						      & (IData)(vlTOPp->mos6502__DOT__state))
						      ? (IData)(vlTOPp->mos6502__DOT__src_reg)
						      : 3U)))))))
				      : ((0x40U & (IData)(vlTOPp->mos6502__DOT__state))
					  ? ((0x20U 
					      & (IData)(vlTOPp->mos6502__DOT__state))
					      ? ((0x10U 
						  & (IData)(vlTOPp->mos6502__DOT__state))
						  ? (IData)(vlTOPp->mos6502__DOT__src_reg)
						  : 
						 ((8U 
						   & (IData)(vlTOPp->mos6502__DOT__state))
						   ? 
						  ((4U 
						    & (IData)(vlTOPp->mos6502__DOT__state))
						    ? (IData)(vlTOPp->mos6502__DOT__src_reg)
						    : 
						   ((1U 
						     & (IData)(vlTOPp->mos6502__DOT__state))
						     ? (IData)(vlTOPp->mos6502__DOT__src_reg)
						     : 3U))
						   : 
						  ((4U 
						    & (IData)(vlTOPp->mos6502__DOT__state))
						    ? (IData)(vlTOPp->mos6502__DOT__src_reg)
						    : 
						   ((2U 
						     & (IData)(vlTOPp->mos6502__DOT__state))
						     ? 
						    ((1U 
						      & (IData)(vlTOPp->mos6502__DOT__state))
						      ? 3U
						      : (IData)(vlTOPp->mos6502__DOT__src_reg))
						     : 
						    ((1U 
						      & (IData)(vlTOPp->mos6502__DOT__state))
						      ? (IData)(vlTOPp->mos6502__DOT__src_reg)
						      : 3U)))))
					      : ((8U 
						  & (IData)(vlTOPp->mos6502__DOT__state))
						  ? (IData)(vlTOPp->mos6502__DOT__src_reg)
						  : 
						 ((4U 
						   & (IData)(vlTOPp->mos6502__DOT__state))
						   ? (IData)(vlTOPp->mos6502__DOT__src_reg)
						   : 
						  ((2U 
						    & (IData)(vlTOPp->mos6502__DOT__state))
						    ? (IData)(vlTOPp->mos6502__DOT__src_reg)
						    : 
						   ((1U 
						     & (IData)(vlTOPp->mos6502__DOT__state))
						     ? (IData)(vlTOPp->mos6502__DOT__src_reg)
						     : 
						    ((IData)(vlTOPp->mos6502__DOT__reg_y)
						      ? 2U
						      : 1U))))))
					  : ((0x20U 
					      & (IData)(vlTOPp->mos6502__DOT__state))
					      ? ((0x10U 
						  & (IData)(vlTOPp->mos6502__DOT__state))
						  ? 
						 ((4U 
						   & (IData)(vlTOPp->mos6502__DOT__state))
						   ? (IData)(vlTOPp->mos6502__DOT__src_reg)
						   : 
						  ((2U 
						    & (IData)(vlTOPp->mos6502__DOT__state))
						    ? (IData)(vlTOPp->mos6502__DOT__src_reg)
						    : 
						   ((1U 
						     & (IData)(vlTOPp->mos6502__DOT__state))
						     ? (IData)(vlTOPp->mos6502__DOT__src_reg)
						     : 
						    ((IData)(vlTOPp->mos6502__DOT__reg_y)
						      ? 2U
						      : 1U))))
						  : (IData)(vlTOPp->mos6502__DOT__src_reg))
					      : ((0x10U 
						  & (IData)(vlTOPp->mos6502__DOT__state))
						  ? (IData)(vlTOPp->mos6502__DOT__src_reg)
						  : 
						 ((8U 
						   & (IData)(vlTOPp->mos6502__DOT__state))
						   ? 
						  ((4U 
						    & (IData)(vlTOPp->mos6502__DOT__state))
						    ? (IData)(vlTOPp->mos6502__DOT__src_reg)
						    : 
						   ((2U 
						     & (IData)(vlTOPp->mos6502__DOT__state))
						     ? (IData)(vlTOPp->mos6502__DOT__src_reg)
						     : 
						    ((1U 
						      & (IData)(vlTOPp->mos6502__DOT__state))
						      ? (IData)(vlTOPp->mos6502__DOT__src_reg)
						      : (IData)(vlTOPp->mos6502__DOT__dst_reg))))
						   : (IData)(vlTOPp->mos6502__DOT__src_reg))))));
    vlTOPp->mos6502__DOT__reg_val = vlTOPp->mos6502__DOT__AXYS
	[vlTOPp->mos6502__DOT__reg_idx];
}

VL_INLINE_OPT void Vmos6502::_combo__TOP__7(Vmos6502__Syms* __restrict vlSymsp) {
    VL_DEBUG_IF(VL_DBG_MSGF("+    Vmos6502::_combo__TOP__7\n"); );
    Vmos6502* __restrict vlTOPp VL_ATTR_UNUSED = vlSymsp->TOPp;
    // Body
    // ALWAYS at mos6502/pc.v:3
    if ((0x80U & (IData)(vlTOPp->mos6502__DOT__state))) {
	if ((0x40U & (IData)(vlTOPp->mos6502__DOT__state))) {
	    vlTOPp->mos6502__DOT__pc_mux = vlTOPp->mos6502__DOT__pc;
	    vlTOPp->mos6502__DOT__pc_inc = 0U;
	} else {
	    if ((0x20U & (IData)(vlTOPp->mos6502__DOT__state))) {
		vlTOPp->mos6502__DOT__pc_mux = vlTOPp->mos6502__DOT__pc;
		vlTOPp->mos6502__DOT__pc_inc = 0U;
	    } else {
		if ((0x10U & (IData)(vlTOPp->mos6502__DOT__state))) {
		    vlTOPp->mos6502__DOT__pc_mux = vlTOPp->mos6502__DOT__pc;
		    vlTOPp->mos6502__DOT__pc_inc = 0U;
		} else {
		    if ((8U & (IData)(vlTOPp->mos6502__DOT__state))) {
			if ((4U & (IData)(vlTOPp->mos6502__DOT__state))) {
			    vlTOPp->mos6502__DOT__pc_mux 
				= vlTOPp->mos6502__DOT__pc;
			    vlTOPp->mos6502__DOT__pc_inc = 0U;
			} else {
			    if ((2U & (IData)(vlTOPp->mos6502__DOT__state))) {
				if ((1U & (IData)(vlTOPp->mos6502__DOT__state))) {
				    vlTOPp->mos6502__DOT__pc_mux 
					= (((IData)(vlTOPp->mos6502__DOT__din_mux) 
					    << 8U) 
					   | (IData)(vlTOPp->mos6502__DOT__alu_out));
				    vlTOPp->mos6502__DOT__pc_inc = 1U;
				} else {
				    vlTOPp->mos6502__DOT__pc_mux 
					= vlTOPp->mos6502__DOT__pc;
				    vlTOPp->mos6502__DOT__pc_inc = 0U;
				}
			    } else {
				vlTOPp->mos6502__DOT__pc_mux 
				    = vlTOPp->mos6502__DOT__pc;
				vlTOPp->mos6502__DOT__pc_inc = 0U;
			    }
			}
		    } else {
			if ((4U & (IData)(vlTOPp->mos6502__DOT__state))) {
			    if ((2U & (IData)(vlTOPp->mos6502__DOT__state))) {
				vlTOPp->mos6502__DOT__pc_mux 
				    = vlTOPp->mos6502__DOT__pc;
				vlTOPp->mos6502__DOT__pc_inc = 0U;
			    } else {
				if ((1U & (IData)(vlTOPp->mos6502__DOT__state))) {
				    vlTOPp->mos6502__DOT__pc_mux 
					= vlTOPp->mos6502__DOT__pc;
				    vlTOPp->mos6502__DOT__pc_inc = 0U;
				} else {
				    vlTOPp->mos6502__DOT__pc_mux 
					= (((IData)(vlTOPp->mos6502__DOT__din_mux) 
					    << 8U) 
					   | (IData)(vlTOPp->mos6502__DOT__alu_out));
				    vlTOPp->mos6502__DOT__pc_inc = 1U;
				}
			    }
			} else {
			    vlTOPp->mos6502__DOT__pc_mux 
				= vlTOPp->mos6502__DOT__pc;
			    vlTOPp->mos6502__DOT__pc_inc = 0U;
			}
		    }
		}
	    }
	}
    } else {
	if ((0x40U & (IData)(vlTOPp->mos6502__DOT__state))) {
	    if ((0x20U & (IData)(vlTOPp->mos6502__DOT__state))) {
		if ((0x10U & (IData)(vlTOPp->mos6502__DOT__state))) {
		    if ((4U & (IData)(vlTOPp->mos6502__DOT__state))) {
			vlTOPp->mos6502__DOT__pc_mux 
			    = vlTOPp->mos6502__DOT__pc;
			vlTOPp->mos6502__DOT__pc_inc = 0U;
		    } else {
			if ((2U & (IData)(vlTOPp->mos6502__DOT__state))) {
			    vlTOPp->mos6502__DOT__pc_mux 
				= vlTOPp->mos6502__DOT__pc;
			    vlTOPp->mos6502__DOT__pc_inc = 0U;
			} else {
			    if ((1U & (IData)(vlTOPp->mos6502__DOT__state))) {
				vlTOPp->mos6502__DOT__pc_mux 
				    = (((IData)(vlTOPp->mos6502__DOT__din_mux) 
					<< 8U) | (IData)(vlTOPp->mos6502__DOT__alu_out));
				vlTOPp->mos6502__DOT__pc_inc = 1U;
			    } else {
				vlTOPp->mos6502__DOT__pc_mux 
				    = vlTOPp->mos6502__DOT__pc;
				vlTOPp->mos6502__DOT__pc_inc = 0U;
			    }
			}
		    }
		} else {
		    if ((8U & (IData)(vlTOPp->mos6502__DOT__state))) {
			if ((4U & (IData)(vlTOPp->mos6502__DOT__state))) {
			    vlTOPp->mos6502__DOT__pc_mux 
				= vlTOPp->mos6502__DOT__pc;
			    vlTOPp->mos6502__DOT__pc_inc = 0U;
			} else {
			    if ((2U & (IData)(vlTOPp->mos6502__DOT__state))) {
				if ((1U & (IData)(vlTOPp->mos6502__DOT__state))) {
				    vlTOPp->mos6502__DOT__pc_mux 
					= (((IData)(vlTOPp->mos6502__DOT__din_mux) 
					    << 8U) 
					   | (IData)(vlTOPp->mos6502__DOT__alu_out));
				    vlTOPp->mos6502__DOT__pc_inc = 0U;
				} else {
				    vlTOPp->mos6502__DOT__pc_mux 
					= vlTOPp->mos6502__DOT__pc;
				    vlTOPp->mos6502__DOT__pc_inc = 0U;
				}
			    } else {
				vlTOPp->mos6502__DOT__pc_mux 
				    = vlTOPp->mos6502__DOT__pc;
				vlTOPp->mos6502__DOT__pc_inc = 0U;
			    }
			}
		    } else {
			if ((4U & (IData)(vlTOPp->mos6502__DOT__state))) {
			    vlTOPp->mos6502__DOT__pc_mux 
				= vlTOPp->mos6502__DOT__pc;
			    vlTOPp->mos6502__DOT__pc_inc = 0U;
			} else {
			    if ((2U & (IData)(vlTOPp->mos6502__DOT__state))) {
				if ((1U & (IData)(vlTOPp->mos6502__DOT__state))) {
				    vlTOPp->mos6502__DOT__pc_mux 
					= vlTOPp->mos6502__DOT__pc;
				    vlTOPp->mos6502__DOT__pc_inc = 1U;
				} else {
				    vlTOPp->mos6502__DOT__pc_mux 
					= ((IData)(vlTOPp->mos6502__DOT__res)
					    ? 0xfffcU
					    : ((IData)(vlTOPp->mos6502__DOT__nmi_edge)
					        ? 0xfffaU
					        : 0xfffeU));
				    vlTOPp->mos6502__DOT__pc_inc = 0U;
				}
			    } else {
				vlTOPp->mos6502__DOT__pc_mux 
				    = vlTOPp->mos6502__DOT__pc;
				vlTOPp->mos6502__DOT__pc_inc = 0U;
			    }
			}
		    }
		}
	    } else {
		if ((0x10U & (IData)(vlTOPp->mos6502__DOT__state))) {
		    if ((8U & (IData)(vlTOPp->mos6502__DOT__state))) {
			if ((4U & (IData)(vlTOPp->mos6502__DOT__state))) {
			    vlTOPp->mos6502__DOT__pc_mux 
				= vlTOPp->mos6502__DOT__pc;
			    vlTOPp->mos6502__DOT__pc_inc = 0U;
			} else {
			    if ((2U & (IData)(vlTOPp->mos6502__DOT__state))) {
				if ((1U & (IData)(vlTOPp->mos6502__DOT__state))) {
				    vlTOPp->mos6502__DOT__pc_mux 
					= vlTOPp->mos6502__DOT__pc;
				    vlTOPp->mos6502__DOT__pc_inc = 0U;
				} else {
				    vlTOPp->mos6502__DOT__pc_mux 
					= (((IData)(vlTOPp->mos6502__DOT__alu_out) 
					    << 8U) 
					   | (0xffU 
					      & (IData)(vlTOPp->mos6502__DOT__pc)));
				    vlTOPp->mos6502__DOT__pc_inc = 1U;
				}
			    } else {
				if ((1U & (IData)(vlTOPp->mos6502__DOT__state))) {
				    vlTOPp->mos6502__DOT__pc_mux 
					= (((IData)(vlTOPp->mos6502__DOT__addr_h) 
					    << 8U) 
					   | (IData)(vlTOPp->mos6502__DOT__alu_out));
				    vlTOPp->mos6502__DOT__pc_inc 
					= (1U & ((IData)(vlTOPp->mos6502__DOT__carry_out) 
						 ^ 
						 (~ (IData)(vlTOPp->mos6502__DOT__backwards))));
				} else {
				    vlTOPp->mos6502__DOT__pc_mux 
					= vlTOPp->mos6502__DOT__pc;
				    vlTOPp->mos6502__DOT__pc_inc = 1U;
				}
			    }
			}
		    } else {
			vlTOPp->mos6502__DOT__pc_mux 
			    = vlTOPp->mos6502__DOT__pc;
			vlTOPp->mos6502__DOT__pc_inc = 0U;
		    }
		} else {
		    vlTOPp->mos6502__DOT__pc_mux = vlTOPp->mos6502__DOT__pc;
		    vlTOPp->mos6502__DOT__pc_inc = 0U;
		}
	    }
	} else {
	    if ((0x20U & (IData)(vlTOPp->mos6502__DOT__state))) {
		if ((0x10U & (IData)(vlTOPp->mos6502__DOT__state))) {
		    if ((8U & (IData)(vlTOPp->mos6502__DOT__state))) {
			vlTOPp->mos6502__DOT__pc_mux 
			    = vlTOPp->mos6502__DOT__pc;
			vlTOPp->mos6502__DOT__pc_inc = 0U;
		    } else {
			if ((4U & (IData)(vlTOPp->mos6502__DOT__state))) {
			    vlTOPp->mos6502__DOT__pc_mux 
				= vlTOPp->mos6502__DOT__pc;
			    vlTOPp->mos6502__DOT__pc_inc = 0U;
			} else {
			    if ((2U & (IData)(vlTOPp->mos6502__DOT__state))) {
				vlTOPp->mos6502__DOT__pc_mux 
				    = vlTOPp->mos6502__DOT__pc;
				vlTOPp->mos6502__DOT__pc_inc = 0U;
			    } else {
				if ((1U & (IData)(vlTOPp->mos6502__DOT__state))) {
				    vlTOPp->mos6502__DOT__pc_mux 
					= vlTOPp->mos6502__DOT__pc;
				    vlTOPp->mos6502__DOT__pc_inc = 0U;
				} else {
				    vlTOPp->mos6502__DOT__pc_mux 
					= vlTOPp->mos6502__DOT__pc;
				    vlTOPp->mos6502__DOT__pc_inc = 1U;
				}
			    }
			}
		    }
		} else {
		    if ((8U & (IData)(vlTOPp->mos6502__DOT__state))) {
			if ((4U & (IData)(vlTOPp->mos6502__DOT__state))) {
			    vlTOPp->mos6502__DOT__pc_mux 
				= vlTOPp->mos6502__DOT__pc;
			    vlTOPp->mos6502__DOT__pc_inc = 0U;
			} else {
			    if ((2U & (IData)(vlTOPp->mos6502__DOT__state))) {
				vlTOPp->mos6502__DOT__pc_mux 
				    = vlTOPp->mos6502__DOT__pc;
				vlTOPp->mos6502__DOT__pc_inc = 0U;
			    } else {
				if ((1U & (IData)(vlTOPp->mos6502__DOT__state))) {
				    vlTOPp->mos6502__DOT__pc_mux 
					= vlTOPp->mos6502__DOT__pc;
				    vlTOPp->mos6502__DOT__pc_inc = 0U;
				} else {
				    vlTOPp->mos6502__DOT__pc_mux 
					= vlTOPp->mos6502__DOT__pc;
				    vlTOPp->mos6502__DOT__pc_inc = 1U;
				}
			    }
			}
		    } else {
			vlTOPp->mos6502__DOT__pc_mux 
			    = vlTOPp->mos6502__DOT__pc;
			vlTOPp->mos6502__DOT__pc_inc = 0U;
		    }
		}
	    } else {
		if ((0x10U & (IData)(vlTOPp->mos6502__DOT__state))) {
		    vlTOPp->mos6502__DOT__pc_mux = vlTOPp->mos6502__DOT__pc;
		    vlTOPp->mos6502__DOT__pc_inc = 0U;
		} else {
		    if ((8U & (IData)(vlTOPp->mos6502__DOT__state))) {
			if ((4U & (IData)(vlTOPp->mos6502__DOT__state))) {
			    vlTOPp->mos6502__DOT__pc_mux 
				= vlTOPp->mos6502__DOT__pc;
			    vlTOPp->mos6502__DOT__pc_inc = 0U;
			} else {
			    if ((2U & (IData)(vlTOPp->mos6502__DOT__state))) {
				vlTOPp->mos6502__DOT__pc_mux 
				    = vlTOPp->mos6502__DOT__pc;
				vlTOPp->mos6502__DOT__pc_inc = 0U;
			    } else {
				if ((1U & (IData)(vlTOPp->mos6502__DOT__state))) {
				    vlTOPp->mos6502__DOT__pc_mux 
					= vlTOPp->mos6502__DOT__pc;
				    vlTOPp->mos6502__DOT__pc_inc = 0U;
				} else {
				    if ((((~ (IData)(vlTOPp->mos6502__DOT__I)) 
					  & (IData)(vlTOPp->irq)) 
					 | (IData)(vlTOPp->mos6502__DOT__nmi_edge))) {
					vlTOPp->mos6502__DOT__pc_mux 
					    = (((IData)(vlTOPp->mos6502__DOT__addr_h) 
						<< 8U) 
					       | (IData)(vlTOPp->mos6502__DOT__addr_l));
					vlTOPp->mos6502__DOT__pc_inc = 0U;
				    } else {
					vlTOPp->mos6502__DOT__pc_mux 
					    = vlTOPp->mos6502__DOT__pc;
					vlTOPp->mos6502__DOT__pc_inc = 1U;
				    }
				}
			    }
			}
		    } else {
			if ((4U & (IData)(vlTOPp->mos6502__DOT__state))) {
			    vlTOPp->mos6502__DOT__pc_mux 
				= vlTOPp->mos6502__DOT__pc;
			    vlTOPp->mos6502__DOT__pc_inc = 0U;
			} else {
			    if ((2U & (IData)(vlTOPp->mos6502__DOT__state))) {
				vlTOPp->mos6502__DOT__pc_mux 
				    = vlTOPp->mos6502__DOT__pc;
				vlTOPp->mos6502__DOT__pc_inc = 0U;
			    } else {
				if ((1U & (IData)(vlTOPp->mos6502__DOT__state))) {
				    vlTOPp->mos6502__DOT__pc_mux 
					= vlTOPp->mos6502__DOT__pc;
				    vlTOPp->mos6502__DOT__pc_inc = 0U;
				} else {
				    vlTOPp->mos6502__DOT__pc_mux 
					= vlTOPp->mos6502__DOT__pc;
				    vlTOPp->mos6502__DOT__pc_inc = 1U;
				}
			    }
			}
		    }
		}
	    }
	}
    }
    // ALWAYS at mos6502/alu_control.v:83
    vlTOPp->mos6502__DOT__alu_B = (0xffU & ((0x80U 
					     & (IData)(vlTOPp->mos6502__DOT__state))
					     ? ((0x40U 
						 & (IData)(vlTOPp->mos6502__DOT__state))
						 ? (IData)(vlTOPp->mos6502__DOT__din_mux)
						 : 
						((0x20U 
						  & (IData)(vlTOPp->mos6502__DOT__state))
						  ? (IData)(vlTOPp->mos6502__DOT__din_mux)
						  : 
						 ((0x10U 
						   & (IData)(vlTOPp->mos6502__DOT__state))
						   ? 
						  ((8U 
						    & (IData)(vlTOPp->mos6502__DOT__state))
						    ? 
						   ((4U 
						     & (IData)(vlTOPp->mos6502__DOT__state))
						     ? (IData)(vlTOPp->mos6502__DOT__din_mux)
						     : 
						    ((2U 
						      & (IData)(vlTOPp->mos6502__DOT__state))
						      ? (IData)(vlTOPp->mos6502__DOT__din_mux)
						      : 
						     ((1U 
						       & (IData)(vlTOPp->mos6502__DOT__state))
						       ? (IData)(vlTOPp->mos6502__DOT__din_mux)
						       : 0U)))
						    : 
						   ((4U 
						     & (IData)(vlTOPp->mos6502__DOT__state))
						     ? (IData)(vlTOPp->mos6502__DOT__din_mux)
						     : 
						    ((2U 
						      & (IData)(vlTOPp->mos6502__DOT__state))
						      ? (IData)(vlTOPp->mos6502__DOT__din_mux)
						      : 0U)))
						   : 
						  ((8U 
						    & (IData)(vlTOPp->mos6502__DOT__state))
						    ? 
						   ((4U 
						     & (IData)(vlTOPp->mos6502__DOT__state))
						     ? (IData)(vlTOPp->mos6502__DOT__din_mux)
						     : 
						    ((2U 
						      & (IData)(vlTOPp->mos6502__DOT__state))
						      ? (IData)(vlTOPp->mos6502__DOT__din_mux)
						      : 0U))
						    : 
						   ((4U 
						     & (IData)(vlTOPp->mos6502__DOT__state))
						     ? (IData)(vlTOPp->mos6502__DOT__din_mux)
						     : 
						    ((2U 
						      & (IData)(vlTOPp->mos6502__DOT__state))
						      ? 
						     ((1U 
						       & (IData)(vlTOPp->mos6502__DOT__state))
						       ? (IData)(vlTOPp->mos6502__DOT__din_mux)
						       : 0U)
						      : 0U))))))
					     : ((0x40U 
						 & (IData)(vlTOPp->mos6502__DOT__state))
						 ? 
						((0x20U 
						  & (IData)(vlTOPp->mos6502__DOT__state))
						  ? 
						 ((0x10U 
						   & (IData)(vlTOPp->mos6502__DOT__state))
						   ? (IData)(vlTOPp->mos6502__DOT__din_mux)
						   : 
						  ((4U 
						    & (IData)(vlTOPp->mos6502__DOT__state))
						    ? (IData)(vlTOPp->mos6502__DOT__din_mux)
						    : 
						   ((2U 
						     & (IData)(vlTOPp->mos6502__DOT__state))
						     ? 
						    ((1U 
						      & (IData)(vlTOPp->mos6502__DOT__state))
						      ? (IData)(vlTOPp->mos6502__DOT__din_mux)
						      : 0U)
						     : 0U)))
						  : 
						 ((0x10U 
						   & (IData)(vlTOPp->mos6502__DOT__state))
						   ? 
						  ((8U 
						    & (IData)(vlTOPp->mos6502__DOT__state))
						    ? 
						   ((4U 
						     & (IData)(vlTOPp->mos6502__DOT__state))
						     ? (IData)(vlTOPp->mos6502__DOT__din_mux)
						     : 
						    ((2U 
						      & (IData)(vlTOPp->mos6502__DOT__state))
						      ? (IData)(vlTOPp->mos6502__DOT__din_mux)
						      : 
						     ((1U 
						       & (IData)(vlTOPp->mos6502__DOT__state))
						       ? 0U
						       : (IData)(vlTOPp->mos6502__DOT__pc))))
						    : (IData)(vlTOPp->mos6502__DOT__din_mux))
						   : (IData)(vlTOPp->mos6502__DOT__din_mux)))
						 : 
						((0x20U 
						  & (IData)(vlTOPp->mos6502__DOT__state))
						  ? 
						 ((0x10U 
						   & (IData)(vlTOPp->mos6502__DOT__state))
						   ? 
						  ((8U 
						    & (IData)(vlTOPp->mos6502__DOT__state))
						    ? 
						   ((4U 
						     & (IData)(vlTOPp->mos6502__DOT__state))
						     ? (IData)(vlTOPp->mos6502__DOT__din_mux)
						     : 
						    ((2U 
						      & (IData)(vlTOPp->mos6502__DOT__state))
						      ? (IData)(vlTOPp->mos6502__DOT__din_mux)
						      : 
						     ((1U 
						       & (IData)(vlTOPp->mos6502__DOT__state))
						       ? 0U
						       : (IData)(vlTOPp->mos6502__DOT__din_mux))))
						    : (IData)(vlTOPp->mos6502__DOT__din_mux))
						   : 
						  ((8U 
						    & (IData)(vlTOPp->mos6502__DOT__state))
						    ? 
						   ((4U 
						     & (IData)(vlTOPp->mos6502__DOT__state))
						     ? (IData)(vlTOPp->mos6502__DOT__din_mux)
						     : 
						    ((2U 
						      & (IData)(vlTOPp->mos6502__DOT__state))
						      ? (IData)(vlTOPp->mos6502__DOT__din_mux)
						      : 
						     ((1U 
						       & (IData)(vlTOPp->mos6502__DOT__state))
						       ? 0U
						       : (IData)(vlTOPp->mos6502__DOT__din_mux))))
						    : (IData)(vlTOPp->mos6502__DOT__din_mux)))
						  : 
						 ((0x10U 
						   & (IData)(vlTOPp->mos6502__DOT__state))
						   ? 
						  ((4U 
						    & (IData)(vlTOPp->mos6502__DOT__state))
						    ? (IData)(vlTOPp->mos6502__DOT__din_mux)
						    : 
						   ((2U 
						     & (IData)(vlTOPp->mos6502__DOT__state))
						     ? (IData)(vlTOPp->mos6502__DOT__din_mux)
						     : 
						    ((1U 
						      & (IData)(vlTOPp->mos6502__DOT__state))
						      ? (IData)(vlTOPp->mos6502__DOT__din_mux)
						      : 0U)))
						   : 
						  ((8U 
						    & (IData)(vlTOPp->mos6502__DOT__state))
						    ? 
						   ((4U 
						     & (IData)(vlTOPp->mos6502__DOT__state))
						     ? (IData)(vlTOPp->mos6502__DOT__din_mux)
						     : 
						    ((2U 
						      & (IData)(vlTOPp->mos6502__DOT__state))
						      ? (IData)(vlTOPp->mos6502__DOT__din_mux)
						      : 
						     ((1U 
						       & (IData)(vlTOPp->mos6502__DOT__state))
						       ? (IData)(vlTOPp->mos6502__DOT__din_mux)
						       : 0U)))
						    : (IData)(vlTOPp->mos6502__DOT__din_mux)))))));
    // ALWAYS at mos6502/data_out.v:1
    vlTOPp->wr_en = (1U & ((0x80U & (IData)(vlTOPp->mos6502__DOT__state))
			    ? ((~ ((IData)(vlTOPp->mos6502__DOT__state) 
				   >> 6U)) & ((~ ((IData)(vlTOPp->mos6502__DOT__state) 
						  >> 5U)) 
					      & (((IData)(vlTOPp->mos6502__DOT__state) 
						  >> 4U) 
						 & ((~ 
						     ((IData)(vlTOPp->mos6502__DOT__state) 
						      >> 3U)) 
						    & ((~ 
							((IData)(vlTOPp->mos6502__DOT__state) 
							 >> 2U)) 
						       & ((~ 
							   ((IData)(vlTOPp->mos6502__DOT__state) 
							    >> 1U)) 
							  & (IData)(vlTOPp->mos6502__DOT__state)))))))
			    : ((0x40U & (IData)(vlTOPp->mos6502__DOT__state))
			        ? ((0x20U & (IData)(vlTOPp->mos6502__DOT__state))
				    ? ((~ ((IData)(vlTOPp->mos6502__DOT__state) 
					   >> 4U)) 
				       & ((8U & (IData)(vlTOPp->mos6502__DOT__state))
					   ? ((~ ((IData)(vlTOPp->mos6502__DOT__state) 
						  >> 2U)) 
					      & (~ 
						 ((IData)(vlTOPp->mos6502__DOT__state) 
						  >> 1U)))
					   : ((~ ((IData)(vlTOPp->mos6502__DOT__state) 
						  >> 2U)) 
					      & ((~ 
						  ((IData)(vlTOPp->mos6502__DOT__state) 
						   >> 1U)) 
						 | (~ (IData)(vlTOPp->mos6502__DOT__state))))))
				    : ((0x10U & (IData)(vlTOPp->mos6502__DOT__state))
				        ? ((~ ((IData)(vlTOPp->mos6502__DOT__state) 
					       >> 3U)) 
					   & ((~ ((IData)(vlTOPp->mos6502__DOT__state) 
						  >> 2U)) 
					      & ((~ 
						  ((IData)(vlTOPp->mos6502__DOT__state) 
						   >> 1U)) 
						 & ((IData)(vlTOPp->mos6502__DOT__state) 
						    & (IData)(vlTOPp->mos6502__DOT__store)))))
				        : ((8U & (IData)(vlTOPp->mos6502__DOT__state))
					    ? ((~ ((IData)(vlTOPp->mos6502__DOT__state) 
						   >> 2U)) 
					       & ((~ 
						   ((IData)(vlTOPp->mos6502__DOT__state) 
						    >> 1U)) 
						  & ((~ (IData)(vlTOPp->mos6502__DOT__state)) 
						     & (IData)(vlTOPp->mos6502__DOT__store))))
					    : ((~ ((IData)(vlTOPp->mos6502__DOT__state) 
						   >> 2U)) 
					       & (((IData)(vlTOPp->mos6502__DOT__state) 
						   >> 1U) 
						  & ((IData)(vlTOPp->mos6502__DOT__state) 
						     & (IData)(vlTOPp->mos6502__DOT__store)))))))
			        : (((IData)(vlTOPp->mos6502__DOT__state) 
				    >> 5U) & ((0x10U 
					       & (IData)(vlTOPp->mos6502__DOT__state))
					       ? ((8U 
						   & (IData)(vlTOPp->mos6502__DOT__state))
						   ? 
						  ((~ 
						    ((IData)(vlTOPp->mos6502__DOT__state) 
						     >> 2U)) 
						   & (((IData)(vlTOPp->mos6502__DOT__state) 
						       >> 1U) 
						      & ((IData)(vlTOPp->mos6502__DOT__state) 
							 & (IData)(vlTOPp->mos6502__DOT__store))))
						   : 
						  ((~ 
						    ((IData)(vlTOPp->mos6502__DOT__state) 
						     >> 2U)) 
						   & (((IData)(vlTOPp->mos6502__DOT__state) 
						       >> 1U) 
						      & ((~ (IData)(vlTOPp->mos6502__DOT__state)) 
							 & (IData)(vlTOPp->mos6502__DOT__store)))))
					       : ((8U 
						   & (IData)(vlTOPp->mos6502__DOT__state))
						   ? 
						  ((~ 
						    ((IData)(vlTOPp->mos6502__DOT__state) 
						     >> 2U)) 
						   & ((~ 
						       ((IData)(vlTOPp->mos6502__DOT__state) 
							>> 1U)) 
						      & ((IData)(vlTOPp->mos6502__DOT__state) 
							 & (IData)(vlTOPp->mos6502__DOT__store))))
						   : 
						  ((~ 
						    ((IData)(vlTOPp->mos6502__DOT__state) 
						     >> 2U)) 
						   & ((~ 
						       ((IData)(vlTOPp->mos6502__DOT__state) 
							>> 1U)) 
						      & (~ (IData)(vlTOPp->mos6502__DOT__state))))))))));
    vlTOPp->dout = (0xffU & ((0x80U & (IData)(vlTOPp->mos6502__DOT__state))
			      ? ((0x40U & (IData)(vlTOPp->mos6502__DOT__state))
				  ? (IData)(vlTOPp->mos6502__DOT__reg_val)
				  : ((0x20U & (IData)(vlTOPp->mos6502__DOT__state))
				      ? (IData)(vlTOPp->mos6502__DOT__reg_val)
				      : ((0x10U & (IData)(vlTOPp->mos6502__DOT__state))
					  ? ((8U & (IData)(vlTOPp->mos6502__DOT__state))
					      ? (IData)(vlTOPp->mos6502__DOT__reg_val)
					      : ((4U 
						  & (IData)(vlTOPp->mos6502__DOT__state))
						  ? (IData)(vlTOPp->mos6502__DOT__reg_val)
						  : 
						 ((2U 
						   & (IData)(vlTOPp->mos6502__DOT__state))
						   ? (IData)(vlTOPp->mos6502__DOT__reg_val)
						   : 
						  ((1U 
						    & (IData)(vlTOPp->mos6502__DOT__state))
						    ? 
						   ((IData)(vlTOPp->mos6502__DOT__php)
						     ? (IData)(vlTOPp->mos6502__DOT__sr_flags)
						     : (IData)(vlTOPp->mos6502__DOT__alu_out))
						    : (IData)(vlTOPp->mos6502__DOT__reg_val)))))
					  : (IData)(vlTOPp->mos6502__DOT__reg_val))))
			      : ((0x40U & (IData)(vlTOPp->mos6502__DOT__state))
				  ? ((0x20U & (IData)(vlTOPp->mos6502__DOT__state))
				      ? ((0x10U & (IData)(vlTOPp->mos6502__DOT__state))
					  ? (IData)(vlTOPp->mos6502__DOT__reg_val)
					  : ((8U & (IData)(vlTOPp->mos6502__DOT__state))
					      ? ((4U 
						  & (IData)(vlTOPp->mos6502__DOT__state))
						  ? (IData)(vlTOPp->mos6502__DOT__reg_val)
						  : 
						 ((2U 
						   & (IData)(vlTOPp->mos6502__DOT__state))
						   ? (IData)(vlTOPp->mos6502__DOT__reg_val)
						   : 
						  ((1U 
						    & (IData)(vlTOPp->mos6502__DOT__state))
						    ? (IData)(vlTOPp->mos6502__DOT__pc)
						    : 
						   ((IData)(vlTOPp->mos6502__DOT__pc) 
						    >> 8U))))
					      : ((4U 
						  & (IData)(vlTOPp->mos6502__DOT__state))
						  ? (IData)(vlTOPp->mos6502__DOT__reg_val)
						  : 
						 ((2U 
						   & (IData)(vlTOPp->mos6502__DOT__state))
						   ? 
						  ((1U 
						    & (IData)(vlTOPp->mos6502__DOT__state))
						    ? (IData)(vlTOPp->mos6502__DOT__reg_val)
						    : 
						   (((IData)(vlTOPp->irq) 
						     | (IData)(vlTOPp->mos6502__DOT__nmi_edge))
						     ? 
						    (0xefU 
						     & (IData)(vlTOPp->mos6502__DOT__sr_flags))
						     : (IData)(vlTOPp->mos6502__DOT__sr_flags)))
						   : 
						  ((1U 
						    & (IData)(vlTOPp->mos6502__DOT__state))
						    ? (IData)(vlTOPp->mos6502__DOT__pc)
						    : 
						   ((IData)(vlTOPp->mos6502__DOT__pc) 
						    >> 8U))))))
				      : (IData)(vlTOPp->mos6502__DOT__reg_val))
				  : ((0x20U & (IData)(vlTOPp->mos6502__DOT__state))
				      ? ((0x10U & (IData)(vlTOPp->mos6502__DOT__state))
					  ? (IData)(vlTOPp->mos6502__DOT__reg_val)
					  : ((8U & (IData)(vlTOPp->mos6502__DOT__state))
					      ? (IData)(vlTOPp->mos6502__DOT__reg_val)
					      : ((4U 
						  & (IData)(vlTOPp->mos6502__DOT__state))
						  ? (IData)(vlTOPp->mos6502__DOT__reg_val)
						  : 
						 ((2U 
						   & (IData)(vlTOPp->mos6502__DOT__state))
						   ? (IData)(vlTOPp->mos6502__DOT__reg_val)
						   : 
						  ((1U 
						    & (IData)(vlTOPp->mos6502__DOT__state))
						    ? (IData)(vlTOPp->mos6502__DOT__reg_val)
						    : (IData)(vlTOPp->mos6502__DOT__alu_out))))))
				      : (IData)(vlTOPp->mos6502__DOT__reg_val)))));
    vlTOPp->addr = ((0x80U & (IData)(vlTOPp->mos6502__DOT__state))
		     ? ((0x40U & (IData)(vlTOPp->mos6502__DOT__state))
			 ? (IData)(vlTOPp->mos6502__DOT__pc)
			 : ((0x20U & (IData)(vlTOPp->mos6502__DOT__state))
			     ? (IData)(vlTOPp->mos6502__DOT__pc)
			     : ((0x10U & (IData)(vlTOPp->mos6502__DOT__state))
				 ? ((8U & (IData)(vlTOPp->mos6502__DOT__state))
				     ? ((4U & (IData)(vlTOPp->mos6502__DOT__state))
					 ? (IData)(vlTOPp->mos6502__DOT__pc)
					 : ((2U & (IData)(vlTOPp->mos6502__DOT__state))
					     ? (IData)(vlTOPp->mos6502__DOT__pc)
					     : ((1U 
						 & (IData)(vlTOPp->mos6502__DOT__state))
						 ? 
						(0x100U 
						 | (IData)(vlTOPp->mos6502__DOT__alu_out))
						 : (IData)(vlTOPp->mos6502__DOT__pc))))
				     : ((4U & (IData)(vlTOPp->mos6502__DOT__state))
					 ? (IData)(vlTOPp->mos6502__DOT__pc)
					 : ((2U & (IData)(vlTOPp->mos6502__DOT__state))
					     ? (IData)(vlTOPp->mos6502__DOT__pc)
					     : ((1U 
						 & (IData)(vlTOPp->mos6502__DOT__state))
						 ? 
						(0x100U 
						 | (IData)(vlTOPp->mos6502__DOT__reg_val))
						 : (IData)(vlTOPp->mos6502__DOT__pc)))))
				 : ((8U & (IData)(vlTOPp->mos6502__DOT__state))
				     ? ((4U & (IData)(vlTOPp->mos6502__DOT__state))
					 ? (IData)(vlTOPp->mos6502__DOT__pc)
					 : ((2U & (IData)(vlTOPp->mos6502__DOT__state))
					     ? ((1U 
						 & (IData)(vlTOPp->mos6502__DOT__state))
						 ? (IData)(vlTOPp->mos6502__DOT__pc)
						 : 
						(0x100U 
						 | (IData)(vlTOPp->mos6502__DOT__alu_out)))
					     : ((1U 
						 & (IData)(vlTOPp->mos6502__DOT__state))
						 ? 
						(0x100U 
						 | (IData)(vlTOPp->mos6502__DOT__alu_out))
						 : 
						(0x100U 
						 | (IData)(vlTOPp->mos6502__DOT__reg_val)))))
				     : ((4U & (IData)(vlTOPp->mos6502__DOT__state))
					 ? ((2U & (IData)(vlTOPp->mos6502__DOT__state))
					     ? (IData)(vlTOPp->mos6502__DOT__pc)
					     : ((1U 
						 & (IData)(vlTOPp->mos6502__DOT__state))
						 ? (IData)(vlTOPp->mos6502__DOT__pc)
						 : 
						(((IData)(vlTOPp->mos6502__DOT__din_mux) 
						  << 8U) 
						 | (IData)(vlTOPp->mos6502__DOT__alu_out))))
					 : ((2U & (IData)(vlTOPp->mos6502__DOT__state))
					     ? (0x100U 
						| (IData)(vlTOPp->mos6502__DOT__alu_out))
					     : ((1U 
						 & (IData)(vlTOPp->mos6502__DOT__state))
						 ? 
						(0x100U 
						 | (IData)(vlTOPp->mos6502__DOT__alu_out))
						 : 
						(0x100U 
						 | (IData)(vlTOPp->mos6502__DOT__reg_val)))))))))
		     : ((0x40U & (IData)(vlTOPp->mos6502__DOT__state))
			 ? ((0x20U & (IData)(vlTOPp->mos6502__DOT__state))
			     ? ((0x10U & (IData)(vlTOPp->mos6502__DOT__state))
				 ? ((4U & (IData)(vlTOPp->mos6502__DOT__state))
				     ? (IData)(vlTOPp->mos6502__DOT__pc)
				     : ((2U & (IData)(vlTOPp->mos6502__DOT__state))
					 ? (IData)(vlTOPp->mos6502__DOT__pc)
					 : ((1U & (IData)(vlTOPp->mos6502__DOT__state))
					     ? (((IData)(vlTOPp->mos6502__DOT__din_mux) 
						 << 8U) 
						| (IData)(vlTOPp->mos6502__DOT__alu_out))
					     : (IData)(vlTOPp->mos6502__DOT__pc))))
				 : ((8U & (IData)(vlTOPp->mos6502__DOT__state))
				     ? ((4U & (IData)(vlTOPp->mos6502__DOT__state))
					 ? (IData)(vlTOPp->mos6502__DOT__pc)
					 : ((2U & (IData)(vlTOPp->mos6502__DOT__state))
					     ? (IData)(vlTOPp->mos6502__DOT__pc)
					     : ((1U 
						 & (IData)(vlTOPp->mos6502__DOT__state))
						 ? 
						(0x100U 
						 | (IData)(vlTOPp->mos6502__DOT__alu_out))
						 : 
						(0x100U 
						 | (IData)(vlTOPp->mos6502__DOT__reg_val)))))
				     : ((4U & (IData)(vlTOPp->mos6502__DOT__state))
					 ? (IData)(vlTOPp->mos6502__DOT__pc)
					 : ((2U & (IData)(vlTOPp->mos6502__DOT__state))
					     ? ((1U 
						 & (IData)(vlTOPp->mos6502__DOT__state))
						 ? (IData)(vlTOPp->mos6502__DOT__pc)
						 : 
						(0x100U 
						 | (IData)(vlTOPp->mos6502__DOT__alu_out)))
					     : ((1U 
						 & (IData)(vlTOPp->mos6502__DOT__state))
						 ? 
						(0x100U 
						 | (IData)(vlTOPp->mos6502__DOT__alu_out))
						 : 
						(0x100U 
						 | (IData)(vlTOPp->mos6502__DOT__reg_val)))))))
			     : ((0x10U & (IData)(vlTOPp->mos6502__DOT__state))
				 ? ((8U & (IData)(vlTOPp->mos6502__DOT__state))
				     ? ((4U & (IData)(vlTOPp->mos6502__DOT__state))
					 ? (IData)(vlTOPp->mos6502__DOT__pc)
					 : ((2U & (IData)(vlTOPp->mos6502__DOT__state))
					     ? ((1U 
						 & (IData)(vlTOPp->mos6502__DOT__state))
						 ? (IData)(vlTOPp->mos6502__DOT__pc)
						 : 
						(((IData)(vlTOPp->mos6502__DOT__alu_out) 
						  << 8U) 
						 | (IData)(vlTOPp->mos6502__DOT__addr_l)))
					     : ((1U 
						 & (IData)(vlTOPp->mos6502__DOT__state))
						 ? 
						(((IData)(vlTOPp->mos6502__DOT__addr_h) 
						  << 8U) 
						 | (IData)(vlTOPp->mos6502__DOT__alu_out))
						 : (IData)(vlTOPp->mos6502__DOT__pc))))
				     : ((4U & (IData)(vlTOPp->mos6502__DOT__state))
					 ? (IData)(vlTOPp->mos6502__DOT__pc)
					 : ((2U & (IData)(vlTOPp->mos6502__DOT__state))
					     ? (IData)(vlTOPp->mos6502__DOT__pc)
					     : ((1U 
						 & (IData)(vlTOPp->mos6502__DOT__state))
						 ? (IData)(vlTOPp->mos6502__DOT__alu_out)
						 : (IData)(vlTOPp->mos6502__DOT__pc)))))
				 : ((8U & (IData)(vlTOPp->mos6502__DOT__state))
				     ? ((4U & (IData)(vlTOPp->mos6502__DOT__state))
					 ? (IData)(vlTOPp->mos6502__DOT__pc)
					 : ((2U & (IData)(vlTOPp->mos6502__DOT__state))
					     ? (IData)(vlTOPp->mos6502__DOT__pc)
					     : ((1U 
						 & (IData)(vlTOPp->mos6502__DOT__state))
						 ? (IData)(vlTOPp->mos6502__DOT__pc)
						 : (IData)(vlTOPp->mos6502__DOT__din_mux))))
				     : ((4U & (IData)(vlTOPp->mos6502__DOT__state))
					 ? (IData)(vlTOPp->mos6502__DOT__pc)
					 : ((2U & (IData)(vlTOPp->mos6502__DOT__state))
					     ? ((1U 
						 & (IData)(vlTOPp->mos6502__DOT__state))
						 ? 
						(((IData)(vlTOPp->mos6502__DOT__alu_out) 
						  << 8U) 
						 | (IData)(vlTOPp->mos6502__DOT__addr_l))
						 : 
						(((IData)(vlTOPp->mos6502__DOT__din_mux) 
						  << 8U) 
						 | (IData)(vlTOPp->mos6502__DOT__alu_out)))
					     : ((1U 
						 & (IData)(vlTOPp->mos6502__DOT__state))
						 ? (IData)(vlTOPp->mos6502__DOT__alu_out)
						 : (IData)(vlTOPp->mos6502__DOT__din_mux)))))))
			 : ((0x20U & (IData)(vlTOPp->mos6502__DOT__state))
			     ? ((0x10U & (IData)(vlTOPp->mos6502__DOT__state))
				 ? ((8U & (IData)(vlTOPp->mos6502__DOT__state))
				     ? ((4U & (IData)(vlTOPp->mos6502__DOT__state))
					 ? (IData)(vlTOPp->mos6502__DOT__pc)
					 : ((2U & (IData)(vlTOPp->mos6502__DOT__state))
					     ? ((1U 
						 & (IData)(vlTOPp->mos6502__DOT__state))
						 ? 
						(((IData)(vlTOPp->mos6502__DOT__din_mux) 
						  << 8U) 
						 | (IData)(vlTOPp->mos6502__DOT__alu_out))
						 : (IData)(vlTOPp->mos6502__DOT__alu_out))
					     : ((1U 
						 & (IData)(vlTOPp->mos6502__DOT__state))
						 ? (IData)(vlTOPp->mos6502__DOT__alu_out)
						 : (IData)(vlTOPp->mos6502__DOT__pc))))
				     : ((4U & (IData)(vlTOPp->mos6502__DOT__state))
					 ? (IData)(vlTOPp->mos6502__DOT__pc)
					 : ((2U & (IData)(vlTOPp->mos6502__DOT__state))
					     ? ((1U 
						 & (IData)(vlTOPp->mos6502__DOT__state))
						 ? (IData)(vlTOPp->mos6502__DOT__pc)
						 : 
						(((IData)(vlTOPp->mos6502__DOT__alu_out) 
						  << 8U) 
						 | (IData)(vlTOPp->mos6502__DOT__addr_l)))
					     : ((1U 
						 & (IData)(vlTOPp->mos6502__DOT__state))
						 ? 
						(((IData)(vlTOPp->mos6502__DOT__din_mux) 
						  << 8U) 
						 | (IData)(vlTOPp->mos6502__DOT__alu_out))
						 : (IData)(vlTOPp->mos6502__DOT__pc)))))
				 : ((8U & (IData)(vlTOPp->mos6502__DOT__state))
				     ? ((4U & (IData)(vlTOPp->mos6502__DOT__state))
					 ? (IData)(vlTOPp->mos6502__DOT__pc)
					 : ((2U & (IData)(vlTOPp->mos6502__DOT__state))
					     ? (IData)(vlTOPp->mos6502__DOT__pc)
					     : ((1U 
						 & (IData)(vlTOPp->mos6502__DOT__state))
						 ? 
						(((IData)(vlTOPp->mos6502__DOT__din_mux) 
						  << 8U) 
						 | (IData)(vlTOPp->mos6502__DOT__alu_out))
						 : (IData)(vlTOPp->mos6502__DOT__pc))))
				     : ((4U & (IData)(vlTOPp->mos6502__DOT__state))
					 ? (IData)(vlTOPp->mos6502__DOT__pc)
					 : ((2U & (IData)(vlTOPp->mos6502__DOT__state))
					     ? (IData)(vlTOPp->mos6502__DOT__pc)
					     : ((1U 
						 & (IData)(vlTOPp->mos6502__DOT__state))
						 ? (IData)(vlTOPp->mos6502__DOT__pc)
						 : 
						(((IData)(vlTOPp->mos6502__DOT__addr_h) 
						  << 8U) 
						 | (IData)(vlTOPp->mos6502__DOT__addr_l)))))))
			     : ((0x10U & (IData)(vlTOPp->mos6502__DOT__state))
				 ? ((4U & (IData)(vlTOPp->mos6502__DOT__state))
				     ? (IData)(vlTOPp->mos6502__DOT__pc)
				     : ((2U & (IData)(vlTOPp->mos6502__DOT__state))
					 ? (IData)(vlTOPp->mos6502__DOT__pc)
					 : ((1U & (IData)(vlTOPp->mos6502__DOT__state))
					     ? (IData)(vlTOPp->mos6502__DOT__pc)
					     : (((IData)(vlTOPp->mos6502__DOT__addr_h) 
						 << 8U) 
						| (IData)(vlTOPp->mos6502__DOT__addr_l)))))
				 : (IData)(vlTOPp->mos6502__DOT__pc)))));
    // ALWAYS at mos6502/alu_control.v:83
    vlTOPp->mos6502__DOT__alu_A = (0xffU & ((0x80U 
					     & (IData)(vlTOPp->mos6502__DOT__state))
					     ? ((0x40U 
						 & (IData)(vlTOPp->mos6502__DOT__state))
						 ? 0U
						 : 
						((0x20U 
						  & (IData)(vlTOPp->mos6502__DOT__state))
						  ? 0U
						  : 
						 ((0x10U 
						   & (IData)(vlTOPp->mos6502__DOT__state))
						   ? 
						  ((8U 
						    & (IData)(vlTOPp->mos6502__DOT__state))
						    ? 
						   ((4U 
						     & (IData)(vlTOPp->mos6502__DOT__state))
						     ? 0U
						     : 
						    ((2U 
						      & (IData)(vlTOPp->mos6502__DOT__state))
						      ? 0U
						      : 
						     ((1U 
						       & (IData)(vlTOPp->mos6502__DOT__state))
						       ? 0U
						       : (IData)(vlTOPp->mos6502__DOT__reg_val))))
						    : 
						   ((4U 
						     & (IData)(vlTOPp->mos6502__DOT__state))
						     ? 0U
						     : 
						    ((2U 
						      & (IData)(vlTOPp->mos6502__DOT__state))
						      ? 0U
						      : (IData)(vlTOPp->mos6502__DOT__reg_val))))
						   : 
						  ((8U 
						    & (IData)(vlTOPp->mos6502__DOT__state))
						    ? 
						   ((4U 
						     & (IData)(vlTOPp->mos6502__DOT__state))
						     ? 0U
						     : 
						    ((2U 
						      & (IData)(vlTOPp->mos6502__DOT__state))
						      ? 0U
						      : 
						     ((1U 
						       & (IData)(vlTOPp->mos6502__DOT__state))
						       ? (IData)(vlTOPp->mos6502__DOT__alu_out)
						       : (IData)(vlTOPp->mos6502__DOT__reg_val))))
						    : 
						   ((4U 
						     & (IData)(vlTOPp->mos6502__DOT__state))
						     ? 0U
						     : 
						    ((2U 
						      & (IData)(vlTOPp->mos6502__DOT__state))
						      ? 
						     ((1U 
						       & (IData)(vlTOPp->mos6502__DOT__state))
						       ? 0U
						       : (IData)(vlTOPp->mos6502__DOT__alu_out))
						      : 
						     ((1U 
						       & (IData)(vlTOPp->mos6502__DOT__state))
						       ? (IData)(vlTOPp->mos6502__DOT__alu_out)
						       : (IData)(vlTOPp->mos6502__DOT__reg_val))))))))
					     : ((0x40U 
						 & (IData)(vlTOPp->mos6502__DOT__state))
						 ? 
						((0x20U 
						  & (IData)(vlTOPp->mos6502__DOT__state))
						  ? 
						 ((0x10U 
						   & (IData)(vlTOPp->mos6502__DOT__state))
						   ? 0U
						   : 
						  ((8U 
						    & (IData)(vlTOPp->mos6502__DOT__state))
						    ? 
						   ((4U 
						     & (IData)(vlTOPp->mos6502__DOT__state))
						     ? 0U
						     : 
						    ((2U 
						      & (IData)(vlTOPp->mos6502__DOT__state))
						      ? 
						     ((1U 
						       & (IData)(vlTOPp->mos6502__DOT__state))
						       ? 0U
						       : (IData)(vlTOPp->mos6502__DOT__reg_val))
						      : 
						     ((1U 
						       & (IData)(vlTOPp->mos6502__DOT__state))
						       ? (IData)(vlTOPp->mos6502__DOT__alu_out)
						       : (IData)(vlTOPp->mos6502__DOT__reg_val))))
						    : 
						   ((4U 
						     & (IData)(vlTOPp->mos6502__DOT__state))
						     ? 0U
						     : 
						    ((2U 
						      & (IData)(vlTOPp->mos6502__DOT__state))
						      ? 
						     ((1U 
						       & (IData)(vlTOPp->mos6502__DOT__state))
						       ? 0U
						       : (IData)(vlTOPp->mos6502__DOT__alu_out))
						      : 
						     ((1U 
						       & (IData)(vlTOPp->mos6502__DOT__state))
						       ? (IData)(vlTOPp->mos6502__DOT__alu_out)
						       : (IData)(vlTOPp->mos6502__DOT__reg_val))))))
						  : 
						 ((0x10U 
						   & (IData)(vlTOPp->mos6502__DOT__state))
						   ? 
						  ((8U 
						    & (IData)(vlTOPp->mos6502__DOT__state))
						    ? 
						   ((4U 
						     & (IData)(vlTOPp->mos6502__DOT__state))
						     ? 0U
						     : 
						    ((2U 
						      & (IData)(vlTOPp->mos6502__DOT__state))
						      ? 0U
						      : 
						     ((1U 
						       & (IData)(vlTOPp->mos6502__DOT__state))
						       ? (IData)(vlTOPp->mos6502__DOT__addr_h)
						       : (IData)(vlTOPp->mos6502__DOT__din_mux))))
						    : 
						   ((4U 
						     & (IData)(vlTOPp->mos6502__DOT__state))
						     ? 0U
						     : 
						    ((2U 
						      & (IData)(vlTOPp->mos6502__DOT__state))
						      ? 0U
						      : 
						     ((1U 
						       & (IData)(vlTOPp->mos6502__DOT__state))
						       ? 0U
						       : (IData)(vlTOPp->mos6502__DOT__reg_val)))))
						   : 
						  ((8U 
						    & (IData)(vlTOPp->mos6502__DOT__state))
						    ? 0U
						    : 
						   ((4U 
						     & (IData)(vlTOPp->mos6502__DOT__state))
						     ? 0U
						     : 
						    ((2U 
						      & (IData)(vlTOPp->mos6502__DOT__state))
						      ? 0U
						      : 
						     ((1U 
						       & (IData)(vlTOPp->mos6502__DOT__state))
						       ? (IData)(vlTOPp->mos6502__DOT__reg_val)
						       : 0U))))))
						 : 
						((0x20U 
						  & (IData)(vlTOPp->mos6502__DOT__state))
						  ? 
						 ((0x10U 
						   & (IData)(vlTOPp->mos6502__DOT__state))
						   ? 
						  ((8U 
						    & (IData)(vlTOPp->mos6502__DOT__state))
						    ? 
						   ((4U 
						     & (IData)(vlTOPp->mos6502__DOT__state))
						     ? 0U
						     : 
						    ((2U 
						      & (IData)(vlTOPp->mos6502__DOT__state))
						      ? 0U
						      : 
						     ((1U 
						       & (IData)(vlTOPp->mos6502__DOT__state))
						       ? (IData)(vlTOPp->mos6502__DOT__alu_out)
						       : (IData)(vlTOPp->mos6502__DOT__reg_val))))
						    : 
						   ((4U 
						     & (IData)(vlTOPp->mos6502__DOT__state))
						     ? 0U
						     : 
						    ((2U 
						      & (IData)(vlTOPp->mos6502__DOT__state))
						      ? 0U
						      : 
						     ((1U 
						       & (IData)(vlTOPp->mos6502__DOT__state))
						       ? 0U
						       : (IData)(vlTOPp->mos6502__DOT__reg_val)))))
						   : 0U)
						  : 
						 ((0x10U 
						   & (IData)(vlTOPp->mos6502__DOT__state))
						   ? 
						  ((8U 
						    & (IData)(vlTOPp->mos6502__DOT__state))
						    ? 
						   ((4U 
						     & (IData)(vlTOPp->mos6502__DOT__state))
						     ? 0U
						     : 
						    ((2U 
						      & (IData)(vlTOPp->mos6502__DOT__state))
						      ? 0U
						      : 
						     ((1U 
						       & (IData)(vlTOPp->mos6502__DOT__state))
						       ? 0U
						       : (IData)(vlTOPp->mos6502__DOT__din_mux))))
						    : 
						   ((4U 
						     & (IData)(vlTOPp->mos6502__DOT__state))
						     ? 0U
						     : 
						    ((2U 
						      & (IData)(vlTOPp->mos6502__DOT__state))
						      ? 0U
						      : 
						     ((1U 
						       & (IData)(vlTOPp->mos6502__DOT__state))
						       ? 0U
						       : (IData)(vlTOPp->mos6502__DOT__reg_val)))))
						   : 
						  ((8U 
						    & (IData)(vlTOPp->mos6502__DOT__state))
						    ? 0U
						    : 
						   ((4U 
						     & (IData)(vlTOPp->mos6502__DOT__state))
						     ? 0U
						     : 
						    ((2U 
						      & (IData)(vlTOPp->mos6502__DOT__state))
						      ? 0U
						      : 
						     ((1U 
						       & (IData)(vlTOPp->mos6502__DOT__state))
						       ? 0U
						       : 
						      ((IData)(vlTOPp->mos6502__DOT__load_only)
						        ? 0U
						        : (IData)(vlTOPp->mos6502__DOT__reg_val)))))))))));
    // ALWAYS at mos6502/alu.v:10
    if (vlTOPp->mos6502__DOT__alu_shift_right) {
	vlTOPp->mos6502__DOT__al_A = ((0x100U & ((IData)(vlTOPp->mos6502__DOT__alu_A) 
						 << 8U)) 
				      | (((IData)(vlTOPp->mos6502__DOT__carry_in) 
					  << 7U) | 
					 (0x7fU & ((IData)(vlTOPp->mos6502__DOT__alu_A) 
						   >> 1U))));
    }
    vlTOPp->mos6502__DOT__al_A = ((2U & (IData)(vlTOPp->mos6502__DOT__alu_op))
				   ? ((1U & (IData)(vlTOPp->mos6502__DOT__alu_op))
				       ? (IData)(vlTOPp->mos6502__DOT__alu_A)
				       : ((IData)(vlTOPp->mos6502__DOT__alu_A) 
					  ^ (IData)(vlTOPp->mos6502__DOT__alu_B)))
				   : ((1U & (IData)(vlTOPp->mos6502__DOT__alu_op))
				       ? ((IData)(vlTOPp->mos6502__DOT__alu_A) 
					  & (IData)(vlTOPp->mos6502__DOT__alu_B))
				       : ((IData)(vlTOPp->mos6502__DOT__alu_A) 
					  | (IData)(vlTOPp->mos6502__DOT__alu_B))));
    vlTOPp->mos6502__DOT__al_B = (0xffU & ((8U & (IData)(vlTOPp->mos6502__DOT__alu_op))
					    ? ((4U 
						& (IData)(vlTOPp->mos6502__DOT__alu_op))
					        ? 0U
					        : (IData)(vlTOPp->mos6502__DOT__al_A))
					    : ((4U 
						& (IData)(vlTOPp->mos6502__DOT__alu_op))
					        ? (~ (IData)(vlTOPp->mos6502__DOT__alu_B))
					        : (IData)(vlTOPp->mos6502__DOT__alu_B))));
    vlTOPp->mos6502__DOT__al_O_l = (0x1fU & (((0xfU 
					       & (IData)(vlTOPp->mos6502__DOT__al_A)) 
					      + (0xfU 
						 & (IData)(vlTOPp->mos6502__DOT__al_B))) 
					     + (1U 
						& (((IData)(vlTOPp->mos6502__DOT__alu_shift_right) 
						    | (3U 
						       == 
						       (3U 
							& ((IData)(vlTOPp->mos6502__DOT__alu_op) 
							   >> 2U))))
						    ? 0U
						    : (IData)(vlTOPp->mos6502__DOT__carry_in)))));
    vlTOPp->mos6502__DOT__al_O_h = (0x1fU & ((((IData)(vlTOPp->mos6502__DOT__al_A) 
					       >> 4U) 
					      + (0xfU 
						 & ((IData)(vlTOPp->mos6502__DOT__al_B) 
						    >> 4U))) 
					     + (IData)(vlTOPp->mos6502__DOT__adder_HC)));
    vlTOPp->mos6502__DOT__al_O = (((IData)(vlTOPp->mos6502__DOT__al_O_h) 
				   << 4U) | (0xfU & (IData)(vlTOPp->mos6502__DOT__al_O_l)));
    vlTOPp->mos6502__DOT__adder_HC = (1U & (((IData)(vlTOPp->mos6502__DOT__alu_bcd) 
					     & (5U 
						<= 
						(7U 
						 & ((IData)(vlTOPp->mos6502__DOT__al_O_l) 
						    >> 1U)))) 
					    | ((IData)(vlTOPp->mos6502__DOT__al_O_l) 
					       >> 4U)));
}

void Vmos6502::_eval(Vmos6502__Syms* __restrict vlSymsp) {
    VL_DEBUG_IF(VL_DBG_MSGF("+    Vmos6502::_eval\n"); );
    Vmos6502* __restrict vlTOPp VL_ATTR_UNUSED = vlSymsp->TOPp;
    // Body
    if (((IData)(vlTOPp->clock) & (~ (IData)(vlTOPp->__Vclklast__TOP__clock)))) {
	vlTOPp->_sequent__TOP__1(vlSymsp);
    }
    if ((((IData)(vlTOPp->clock) & (~ (IData)(vlTOPp->__Vclklast__TOP__clock))) 
	 | ((IData)(vlTOPp->reset) & (~ (IData)(vlTOPp->__Vclklast__TOP__reset))))) {
	vlTOPp->_sequent__TOP__2(vlSymsp);
    }
    if (((IData)(vlTOPp->clock) & (~ (IData)(vlTOPp->__Vclklast__TOP__clock)))) {
	vlTOPp->_sequent__TOP__4(vlSymsp);
    }
    vlTOPp->_combo__TOP__5(vlSymsp);
    if ((((IData)(vlTOPp->clock) & (~ (IData)(vlTOPp->__Vclklast__TOP__clock))) 
	 | ((IData)(vlTOPp->reset) & (~ (IData)(vlTOPp->__Vclklast__TOP__reset))))) {
	vlTOPp->_sequent__TOP__6(vlSymsp);
    }
    vlTOPp->_combo__TOP__7(vlSymsp);
    // Final
    vlTOPp->__Vclklast__TOP__clock = vlTOPp->clock;
    vlTOPp->__Vclklast__TOP__reset = vlTOPp->reset;
}

void Vmos6502::_eval_initial(Vmos6502__Syms* __restrict vlSymsp) {
    VL_DEBUG_IF(VL_DBG_MSGF("+    Vmos6502::_eval_initial\n"); );
    Vmos6502* __restrict vlTOPp VL_ATTR_UNUSED = vlSymsp->TOPp;
}

void Vmos6502::final() {
    VL_DEBUG_IF(VL_DBG_MSGF("+    Vmos6502::final\n"); );
    // Variables
    Vmos6502__Syms* __restrict vlSymsp = this->__VlSymsp;
    Vmos6502* __restrict vlTOPp VL_ATTR_UNUSED = vlSymsp->TOPp;
}

void Vmos6502::_eval_settle(Vmos6502__Syms* __restrict vlSymsp) {
    VL_DEBUG_IF(VL_DBG_MSGF("+    Vmos6502::_eval_settle\n"); );
    Vmos6502* __restrict vlTOPp VL_ATTR_UNUSED = vlSymsp->TOPp;
    // Body
    vlTOPp->_settle__TOP__3(vlSymsp);
}

VL_INLINE_OPT QData Vmos6502::_change_request(Vmos6502__Syms* __restrict vlSymsp) {
    VL_DEBUG_IF(VL_DBG_MSGF("+    Vmos6502::_change_request\n"); );
    Vmos6502* __restrict vlTOPp VL_ATTR_UNUSED = vlSymsp->TOPp;
    // Body
    // Change detection
    QData __req = false;  // Logically a bool
    __req |= ((vlTOPp->mos6502__DOT__adder_HC ^ vlTOPp->__Vchglast__TOP__mos6502__DOT__adder_HC));
    VL_DEBUG_IF( if(__req && ((vlTOPp->mos6502__DOT__adder_HC ^ vlTOPp->__Vchglast__TOP__mos6502__DOT__adder_HC))) VL_DBG_MSGF("        CHANGE: mos6502/alu.v:7: mos6502.adder_HC\n"); );
    // Final
    vlTOPp->__Vchglast__TOP__mos6502__DOT__adder_HC 
	= vlTOPp->mos6502__DOT__adder_HC;
    return __req;
}

#ifdef VL_DEBUG
void Vmos6502::_eval_debug_assertions() {
    VL_DEBUG_IF(VL_DBG_MSGF("+    Vmos6502::_eval_debug_assertions\n"); );
    // Body
    if (VL_UNLIKELY((clock & 0xfeU))) {
	Verilated::overWidthError("clock");}
    if (VL_UNLIKELY((ready & 0xfeU))) {
	Verilated::overWidthError("ready");}
    if (VL_UNLIKELY((irq & 0xfeU))) {
	Verilated::overWidthError("irq");}
    if (VL_UNLIKELY((nmi & 0xfeU))) {
	Verilated::overWidthError("nmi");}
    if (VL_UNLIKELY((reset & 0xfeU))) {
	Verilated::overWidthError("reset");}
}
#endif // VL_DEBUG

void Vmos6502::_ctor_var_reset() {
    VL_DEBUG_IF(VL_DBG_MSGF("+    Vmos6502::_ctor_var_reset\n"); );
    // Body
    clock = VL_RAND_RESET_I(1);
    din = VL_RAND_RESET_I(8);
    ready = VL_RAND_RESET_I(1);
    irq = VL_RAND_RESET_I(1);
    nmi = VL_RAND_RESET_I(1);
    reset = VL_RAND_RESET_I(1);
    wr_en = VL_RAND_RESET_I(1);
    dout = VL_RAND_RESET_I(8);
    addr = VL_RAND_RESET_I(16);
    mos6502__DOT__rotate = VL_RAND_RESET_I(1);
    mos6502__DOT__shift = VL_RAND_RESET_I(1);
    mos6502__DOT__compare = VL_RAND_RESET_I(1);
    mos6502__DOT__inc = VL_RAND_RESET_I(1);
    mos6502__DOT__nmi_edge = VL_RAND_RESET_I(1);
    mos6502__DOT__nmi_hold = VL_RAND_RESET_I(1);
    mos6502__DOT__store = VL_RAND_RESET_I(1);
    mos6502__DOT__write_back = VL_RAND_RESET_I(1);
    mos6502__DOT__backwards = VL_RAND_RESET_I(1);
    mos6502__DOT__load_only = VL_RAND_RESET_I(1);
    mos6502__DOT__cond_true = VL_RAND_RESET_I(1);
    mos6502__DOT__adj_bcd = VL_RAND_RESET_I(1);
    mos6502__DOT__adc_bcd = VL_RAND_RESET_I(1);
    mos6502__DOT__adc_sbc = VL_RAND_RESET_I(1);
    mos6502__DOT__wr_reg = VL_RAND_RESET_I(1);
    mos6502__DOT__ld_reg = VL_RAND_RESET_I(1);
    mos6502__DOT__reg_y = VL_RAND_RESET_I(1);
    mos6502__DOT__bit_ins = VL_RAND_RESET_I(1);
    mos6502__DOT__res = VL_RAND_RESET_I(1);
    mos6502__DOT__php = VL_RAND_RESET_I(1);
    mos6502__DOT__clc = VL_RAND_RESET_I(1);
    mos6502__DOT__plp = VL_RAND_RESET_I(1);
    mos6502__DOT__sec = VL_RAND_RESET_I(1);
    mos6502__DOT__cli = VL_RAND_RESET_I(1);
    mos6502__DOT__sei = VL_RAND_RESET_I(1);
    mos6502__DOT__clv = VL_RAND_RESET_I(1);
    mos6502__DOT__cld = VL_RAND_RESET_I(1);
    mos6502__DOT__sed = VL_RAND_RESET_I(1);
    { int __Vi0=0; for (; __Vi0<4; ++__Vi0) {
	    mos6502__DOT__AXYS[__Vi0] = VL_RAND_RESET_I(8);
    }}
    mos6502__DOT__state = VL_RAND_RESET_I(8);
    mos6502__DOT__reg_idx = VL_RAND_RESET_I(2);
    mos6502__DOT__src_reg = VL_RAND_RESET_I(2);
    mos6502__DOT__dst_reg = VL_RAND_RESET_I(2);
    mos6502__DOT__N = VL_RAND_RESET_I(1);
    mos6502__DOT__V = VL_RAND_RESET_I(1);
    mos6502__DOT__D = VL_RAND_RESET_I(1);
    mos6502__DOT__I = VL_RAND_RESET_I(1);
    mos6502__DOT__Z = VL_RAND_RESET_I(1);
    mos6502__DOT__C = VL_RAND_RESET_I(1);
    mos6502__DOT__sr_flags = VL_RAND_RESET_I(8);
    mos6502__DOT__reg_val = VL_RAND_RESET_I(8);
    mos6502__DOT__din_hold = VL_RAND_RESET_I(8);
    mos6502__DOT__din_mux = VL_RAND_RESET_I(8);
    mos6502__DOT__ir_hold = VL_RAND_RESET_I(8);
    mos6502__DOT__ir_hold_valid = VL_RAND_RESET_I(1);
    mos6502__DOT__ir = VL_RAND_RESET_I(8);
    mos6502__DOT__pc = VL_RAND_RESET_I(16);
    mos6502__DOT__pc_mux = VL_RAND_RESET_I(16);
    mos6502__DOT__pc_inc = VL_RAND_RESET_I(1);
    mos6502__DOT__addr_l = VL_RAND_RESET_I(8);
    mos6502__DOT__addr_h = VL_RAND_RESET_I(8);
    mos6502__DOT__adj = VL_RAND_RESET_I(4);
    mos6502__DOT__op = VL_RAND_RESET_I(4);
    mos6502__DOT__alu_op = VL_RAND_RESET_I(4);
    mos6502__DOT__alu_A = VL_RAND_RESET_I(8);
    mos6502__DOT__alu_B = VL_RAND_RESET_I(8);
    mos6502__DOT__carry_in = VL_RAND_RESET_I(1);
    mos6502__DOT__shift_right = VL_RAND_RESET_I(1);
    mos6502__DOT__alu_shift_right = VL_RAND_RESET_I(1);
    mos6502__DOT__HC = VL_RAND_RESET_I(1);
    mos6502__DOT__alu_out = VL_RAND_RESET_I(8);
    mos6502__DOT__carry_out = VL_RAND_RESET_I(1);
    mos6502__DOT__alu_N = VL_RAND_RESET_I(1);
    mos6502__DOT__alu_bcd = VL_RAND_RESET_I(1);
    mos6502__DOT__al_A = VL_RAND_RESET_I(9);
    mos6502__DOT__al_B = VL_RAND_RESET_I(8);
    mos6502__DOT__al_O_h = VL_RAND_RESET_I(5);
    mos6502__DOT__al_O_l = VL_RAND_RESET_I(5);
    mos6502__DOT__al_O = VL_RAND_RESET_I(9);
    mos6502__DOT__adder_HC = VL_RAND_RESET_I(1);
    __Vtableidx1 = VL_RAND_RESET_I(10);
    __Vtable1_mos6502__DOT__wr_reg[0] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[1] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[2] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[3] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[4] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[5] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[6] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[7] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[8] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[9] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[10] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[11] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[12] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[13] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[14] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[15] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[16] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[17] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[18] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[19] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[20] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[21] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[22] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[23] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[24] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[25] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[26] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[27] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[28] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[29] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[30] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[31] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[32] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[33] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[34] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[35] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[36] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[37] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[38] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[39] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[40] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[41] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[42] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[43] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[44] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[45] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[46] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[47] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[48] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[49] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[50] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[51] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[52] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[53] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[54] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[55] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[56] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[57] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[58] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[59] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[60] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[61] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[62] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[63] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[64] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[65] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[66] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[67] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[68] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[69] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[70] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[71] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[72] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[73] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[74] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[75] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[76] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[77] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[78] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[79] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[80] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[81] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[82] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[83] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[84] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[85] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[86] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[87] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[88] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[89] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[90] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[91] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[92] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[93] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[94] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[95] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[96] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[97] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[98] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[99] = 1U;
    __Vtable1_mos6502__DOT__wr_reg[100] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[101] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[102] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[103] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[104] = 1U;
    __Vtable1_mos6502__DOT__wr_reg[105] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[106] = 1U;
    __Vtable1_mos6502__DOT__wr_reg[107] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[108] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[109] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[110] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[111] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[112] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[113] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[114] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[115] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[116] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[117] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[118] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[119] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[120] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[121] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[122] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[123] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[124] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[125] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[126] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[127] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[128] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[129] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[130] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[131] = 1U;
    __Vtable1_mos6502__DOT__wr_reg[132] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[133] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[134] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[135] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[136] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[137] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[138] = 1U;
    __Vtable1_mos6502__DOT__wr_reg[139] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[140] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[141] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[142] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[143] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[144] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[145] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[146] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[147] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[148] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[149] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[150] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[151] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[152] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[153] = 1U;
    __Vtable1_mos6502__DOT__wr_reg[154] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[155] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[156] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[157] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[158] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[159] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[160] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[161] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[162] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[163] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[164] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[165] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[166] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[167] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[168] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[169] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[170] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[171] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[172] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[173] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[174] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[175] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[176] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[177] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[178] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[179] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[180] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[181] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[182] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[183] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[184] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[185] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[186] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[187] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[188] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[189] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[190] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[191] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[192] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[193] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[194] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[195] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[196] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[197] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[198] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[199] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[200] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[201] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[202] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[203] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[204] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[205] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[206] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[207] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[208] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[209] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[210] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[211] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[212] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[213] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[214] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[215] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[216] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[217] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[218] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[219] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[220] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[221] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[222] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[223] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[224] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[225] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[226] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[227] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[228] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[229] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[230] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[231] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[232] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[233] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[234] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[235] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[236] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[237] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[238] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[239] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[240] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[241] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[242] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[243] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[244] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[245] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[246] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[247] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[248] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[249] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[250] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[251] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[252] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[253] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[254] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[255] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[256] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[257] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[258] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[259] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[260] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[261] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[262] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[263] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[264] = 1U;
    __Vtable1_mos6502__DOT__wr_reg[265] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[266] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[267] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[268] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[269] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[270] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[271] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[272] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[273] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[274] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[275] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[276] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[277] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[278] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[279] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[280] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[281] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[282] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[283] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[284] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[285] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[286] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[287] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[288] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[289] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[290] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[291] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[292] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[293] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[294] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[295] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[296] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[297] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[298] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[299] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[300] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[301] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[302] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[303] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[304] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[305] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[306] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[307] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[308] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[309] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[310] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[311] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[312] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[313] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[314] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[315] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[316] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[317] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[318] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[319] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[320] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[321] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[322] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[323] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[324] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[325] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[326] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[327] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[328] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[329] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[330] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[331] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[332] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[333] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[334] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[335] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[336] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[337] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[338] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[339] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[340] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[341] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[342] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[343] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[344] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[345] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[346] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[347] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[348] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[349] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[350] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[351] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[352] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[353] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[354] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[355] = 1U;
    __Vtable1_mos6502__DOT__wr_reg[356] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[357] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[358] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[359] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[360] = 1U;
    __Vtable1_mos6502__DOT__wr_reg[361] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[362] = 1U;
    __Vtable1_mos6502__DOT__wr_reg[363] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[364] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[365] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[366] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[367] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[368] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[369] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[370] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[371] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[372] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[373] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[374] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[375] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[376] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[377] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[378] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[379] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[380] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[381] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[382] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[383] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[384] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[385] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[386] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[387] = 1U;
    __Vtable1_mos6502__DOT__wr_reg[388] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[389] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[390] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[391] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[392] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[393] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[394] = 1U;
    __Vtable1_mos6502__DOT__wr_reg[395] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[396] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[397] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[398] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[399] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[400] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[401] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[402] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[403] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[404] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[405] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[406] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[407] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[408] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[409] = 1U;
    __Vtable1_mos6502__DOT__wr_reg[410] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[411] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[412] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[413] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[414] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[415] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[416] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[417] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[418] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[419] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[420] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[421] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[422] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[423] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[424] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[425] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[426] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[427] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[428] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[429] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[430] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[431] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[432] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[433] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[434] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[435] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[436] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[437] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[438] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[439] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[440] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[441] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[442] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[443] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[444] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[445] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[446] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[447] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[448] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[449] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[450] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[451] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[452] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[453] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[454] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[455] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[456] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[457] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[458] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[459] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[460] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[461] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[462] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[463] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[464] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[465] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[466] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[467] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[468] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[469] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[470] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[471] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[472] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[473] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[474] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[475] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[476] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[477] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[478] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[479] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[480] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[481] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[482] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[483] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[484] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[485] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[486] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[487] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[488] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[489] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[490] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[491] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[492] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[493] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[494] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[495] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[496] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[497] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[498] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[499] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[500] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[501] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[502] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[503] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[504] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[505] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[506] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[507] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[508] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[509] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[510] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[511] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[512] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[513] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[514] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[515] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[516] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[517] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[518] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[519] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[520] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[521] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[522] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[523] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[524] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[525] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[526] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[527] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[528] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[529] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[530] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[531] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[532] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[533] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[534] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[535] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[536] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[537] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[538] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[539] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[540] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[541] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[542] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[543] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[544] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[545] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[546] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[547] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[548] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[549] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[550] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[551] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[552] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[553] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[554] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[555] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[556] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[557] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[558] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[559] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[560] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[561] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[562] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[563] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[564] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[565] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[566] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[567] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[568] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[569] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[570] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[571] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[572] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[573] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[574] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[575] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[576] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[577] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[578] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[579] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[580] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[581] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[582] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[583] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[584] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[585] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[586] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[587] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[588] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[589] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[590] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[591] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[592] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[593] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[594] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[595] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[596] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[597] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[598] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[599] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[600] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[601] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[602] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[603] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[604] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[605] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[606] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[607] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[608] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[609] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[610] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[611] = 1U;
    __Vtable1_mos6502__DOT__wr_reg[612] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[613] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[614] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[615] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[616] = 1U;
    __Vtable1_mos6502__DOT__wr_reg[617] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[618] = 1U;
    __Vtable1_mos6502__DOT__wr_reg[619] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[620] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[621] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[622] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[623] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[624] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[625] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[626] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[627] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[628] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[629] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[630] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[631] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[632] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[633] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[634] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[635] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[636] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[637] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[638] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[639] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[640] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[641] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[642] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[643] = 1U;
    __Vtable1_mos6502__DOT__wr_reg[644] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[645] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[646] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[647] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[648] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[649] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[650] = 1U;
    __Vtable1_mos6502__DOT__wr_reg[651] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[652] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[653] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[654] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[655] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[656] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[657] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[658] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[659] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[660] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[661] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[662] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[663] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[664] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[665] = 1U;
    __Vtable1_mos6502__DOT__wr_reg[666] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[667] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[668] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[669] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[670] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[671] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[672] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[673] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[674] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[675] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[676] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[677] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[678] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[679] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[680] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[681] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[682] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[683] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[684] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[685] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[686] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[687] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[688] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[689] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[690] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[691] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[692] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[693] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[694] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[695] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[696] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[697] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[698] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[699] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[700] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[701] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[702] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[703] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[704] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[705] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[706] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[707] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[708] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[709] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[710] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[711] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[712] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[713] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[714] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[715] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[716] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[717] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[718] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[719] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[720] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[721] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[722] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[723] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[724] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[725] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[726] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[727] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[728] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[729] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[730] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[731] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[732] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[733] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[734] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[735] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[736] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[737] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[738] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[739] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[740] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[741] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[742] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[743] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[744] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[745] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[746] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[747] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[748] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[749] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[750] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[751] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[752] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[753] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[754] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[755] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[756] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[757] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[758] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[759] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[760] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[761] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[762] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[763] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[764] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[765] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[766] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[767] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[768] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[769] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[770] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[771] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[772] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[773] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[774] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[775] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[776] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[777] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[778] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[779] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[780] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[781] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[782] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[783] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[784] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[785] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[786] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[787] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[788] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[789] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[790] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[791] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[792] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[793] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[794] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[795] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[796] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[797] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[798] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[799] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[800] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[801] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[802] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[803] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[804] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[805] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[806] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[807] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[808] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[809] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[810] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[811] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[812] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[813] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[814] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[815] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[816] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[817] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[818] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[819] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[820] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[821] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[822] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[823] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[824] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[825] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[826] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[827] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[828] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[829] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[830] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[831] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[832] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[833] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[834] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[835] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[836] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[837] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[838] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[839] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[840] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[841] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[842] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[843] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[844] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[845] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[846] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[847] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[848] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[849] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[850] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[851] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[852] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[853] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[854] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[855] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[856] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[857] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[858] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[859] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[860] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[861] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[862] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[863] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[864] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[865] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[866] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[867] = 1U;
    __Vtable1_mos6502__DOT__wr_reg[868] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[869] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[870] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[871] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[872] = 1U;
    __Vtable1_mos6502__DOT__wr_reg[873] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[874] = 1U;
    __Vtable1_mos6502__DOT__wr_reg[875] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[876] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[877] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[878] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[879] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[880] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[881] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[882] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[883] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[884] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[885] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[886] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[887] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[888] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[889] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[890] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[891] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[892] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[893] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[894] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[895] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[896] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[897] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[898] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[899] = 1U;
    __Vtable1_mos6502__DOT__wr_reg[900] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[901] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[902] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[903] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[904] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[905] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[906] = 1U;
    __Vtable1_mos6502__DOT__wr_reg[907] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[908] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[909] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[910] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[911] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[912] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[913] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[914] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[915] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[916] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[917] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[918] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[919] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[920] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[921] = 1U;
    __Vtable1_mos6502__DOT__wr_reg[922] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[923] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[924] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[925] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[926] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[927] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[928] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[929] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[930] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[931] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[932] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[933] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[934] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[935] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[936] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[937] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[938] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[939] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[940] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[941] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[942] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[943] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[944] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[945] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[946] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[947] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[948] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[949] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[950] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[951] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[952] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[953] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[954] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[955] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[956] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[957] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[958] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[959] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[960] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[961] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[962] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[963] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[964] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[965] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[966] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[967] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[968] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[969] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[970] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[971] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[972] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[973] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[974] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[975] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[976] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[977] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[978] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[979] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[980] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[981] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[982] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[983] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[984] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[985] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[986] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[987] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[988] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[989] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[990] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[991] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[992] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[993] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[994] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[995] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[996] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[997] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[998] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[999] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[1000] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[1001] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[1002] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[1003] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[1004] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[1005] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[1006] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[1007] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[1008] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[1009] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[1010] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[1011] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[1012] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[1013] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[1014] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[1015] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[1016] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[1017] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[1018] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[1019] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[1020] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[1021] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[1022] = 0U;
    __Vtable1_mos6502__DOT__wr_reg[1023] = 0U;
    __Vdly__mos6502__DOT__nmi_hold = VL_RAND_RESET_I(1);
    __Vdly__mos6502__DOT__nmi_edge = VL_RAND_RESET_I(1);
    __Vdly__mos6502__DOT__N = VL_RAND_RESET_I(1);
    __Vdly__mos6502__DOT__Z = VL_RAND_RESET_I(1);
    __Vdly__mos6502__DOT__C = VL_RAND_RESET_I(1);
    __Vdly__mos6502__DOT__V = VL_RAND_RESET_I(1);
    __Vdly__mos6502__DOT__state = VL_RAND_RESET_I(8);
    __Vclklast__TOP__clock = VL_RAND_RESET_I(1);
    __Vclklast__TOP__reset = VL_RAND_RESET_I(1);
    __Vchglast__TOP__mos6502__DOT__adder_HC = VL_RAND_RESET_I(1);
}
