// Verilated -*- C++ -*-
// DESCRIPTION: Verilator output: Primary design header
//
// This header should be included by all source files instantiating the design.
// The class here is then constructed to instantiate the design.
// See the Verilator manual for examples.

#ifndef _Vbus_H_
#define _Vbus_H_

#include "verilated_heavy.h"

class Vbus__Syms;

//----------

VL_MODULE(Vbus) {
  public:
    
    // PORTS
    // The application code writes and reads these signals to
    // propagate new values into/out from the Verilated model.
    VL_IN8(cpu_clock,0,0);
    VL_IN8(reset,0,0);
    VL_IN8(ppu_clock,0,0);
    VL_OUT8(ppu_reg_idx,2,0);
    VL_OUT8(ppu_reg_wr_en,0,0);
    VL_OUT8(ppu_reg_din,7,0);
    VL_IN8(ppu_reg_dout,7,0);
    VL_IN8(rom_ppu_wr_en,0,0);
    VL_IN8(rom_ppu_dout,7,0);
    VL_OUT8(rom_ppu_din,7,0);
    VL_OUT8(dma_dout,7,0);
    VL_OUT8(dma_en,0,0);
    VL_IN8(cpu_nmi,0,0);
    VL_IN8(cpu_ready,0,0);
    VL_IN16(rom_ppu_addr,15,0);
    
    // LOCAL SIGNALS
    // Internals; generally not touched by application code
    // Anonymous structures to workaround compiler member-count bugs
    struct {
	VL_SIG8(bus__DOT__cpu_wr_en,0,0);
	VL_SIG8(bus__DOT__cpu_irq,0,0);
	VL_SIG8(bus__DOT__cpu_din,7,0);
	VL_SIG8(bus__DOT__cpu_dout,7,0);
	VL_SIG8(bus__DOT__ram_dout,7,0);
	VL_SIG8(bus__DOT__rom_cpu_dout,7,0);
	VL_SIG8(bus__DOT__dma_addr_hi,7,0);
	VL_SIG8(bus__DOT__dma_addr_lo,7,0);
	VL_SIG8(bus__DOT__addr_mod,1,0);
	VL_SIG8(bus__DOT__mos6502__DOT__rotate,0,0);
	VL_SIG8(bus__DOT__mos6502__DOT__shift,0,0);
	VL_SIG8(bus__DOT__mos6502__DOT__compare,0,0);
	VL_SIG8(bus__DOT__mos6502__DOT__inc,0,0);
	VL_SIG8(bus__DOT__mos6502__DOT__nmi_edge,0,0);
	VL_SIG8(bus__DOT__mos6502__DOT__nmi_hold,0,0);
	VL_SIG8(bus__DOT__mos6502__DOT__store,0,0);
	VL_SIG8(bus__DOT__mos6502__DOT__write_back,0,0);
	VL_SIG8(bus__DOT__mos6502__DOT__backwards,0,0);
	VL_SIG8(bus__DOT__mos6502__DOT__load_only,0,0);
	VL_SIG8(bus__DOT__mos6502__DOT__cond_true,0,0);
	VL_SIG8(bus__DOT__mos6502__DOT__adj_bcd,0,0);
	VL_SIG8(bus__DOT__mos6502__DOT__adc_bcd,0,0);
	VL_SIG8(bus__DOT__mos6502__DOT__adc_sbc,0,0);
	VL_SIG8(bus__DOT__mos6502__DOT__wr_reg,0,0);
	VL_SIG8(bus__DOT__mos6502__DOT__ld_reg,0,0);
	VL_SIG8(bus__DOT__mos6502__DOT__reg_y,0,0);
	VL_SIG8(bus__DOT__mos6502__DOT__bit_ins,0,0);
	VL_SIG8(bus__DOT__mos6502__DOT__res,0,0);
	VL_SIG8(bus__DOT__mos6502__DOT__php,0,0);
	VL_SIG8(bus__DOT__mos6502__DOT__clc,0,0);
	VL_SIG8(bus__DOT__mos6502__DOT__plp,0,0);
	VL_SIG8(bus__DOT__mos6502__DOT__sec,0,0);
	VL_SIG8(bus__DOT__mos6502__DOT__cli,0,0);
	VL_SIG8(bus__DOT__mos6502__DOT__sei,0,0);
	VL_SIG8(bus__DOT__mos6502__DOT__clv,0,0);
	VL_SIG8(bus__DOT__mos6502__DOT__cld,0,0);
	VL_SIG8(bus__DOT__mos6502__DOT__sed,0,0);
	VL_SIG8(bus__DOT__mos6502__DOT__state,7,0);
	VL_SIG8(bus__DOT__mos6502__DOT__reg_idx,1,0);
	VL_SIG8(bus__DOT__mos6502__DOT__src_reg,1,0);
	VL_SIG8(bus__DOT__mos6502__DOT__dst_reg,1,0);
	VL_SIG8(bus__DOT__mos6502__DOT__N,0,0);
	VL_SIG8(bus__DOT__mos6502__DOT__V,0,0);
	VL_SIG8(bus__DOT__mos6502__DOT__D,0,0);
	VL_SIG8(bus__DOT__mos6502__DOT__I,0,0);
	VL_SIG8(bus__DOT__mos6502__DOT__Z,0,0);
	VL_SIG8(bus__DOT__mos6502__DOT__C,0,0);
	VL_SIG8(bus__DOT__mos6502__DOT__sr_flags,7,0);
	VL_SIG8(bus__DOT__mos6502__DOT__reg_val,7,0);
	VL_SIG8(bus__DOT__mos6502__DOT__din_hold,7,0);
	VL_SIG8(bus__DOT__mos6502__DOT__din_mux,7,0);
	VL_SIG8(bus__DOT__mos6502__DOT__ir_hold,7,0);
	VL_SIG8(bus__DOT__mos6502__DOT__ir_hold_valid,0,0);
	VL_SIG8(bus__DOT__mos6502__DOT__ir,7,0);
	VL_SIG8(bus__DOT__mos6502__DOT__pc_inc,0,0);
	VL_SIG8(bus__DOT__mos6502__DOT__addr_l,7,0);
	VL_SIG8(bus__DOT__mos6502__DOT__addr_h,7,0);
	VL_SIG8(bus__DOT__mos6502__DOT__adj,3,0);
	VL_SIG8(bus__DOT__mos6502__DOT__op,3,0);
	VL_SIG8(bus__DOT__mos6502__DOT__alu_op,3,0);
	VL_SIG8(bus__DOT__mos6502__DOT__alu_A,7,0);
	VL_SIG8(bus__DOT__mos6502__DOT__alu_B,7,0);
	VL_SIG8(bus__DOT__mos6502__DOT__carry_in,0,0);
	VL_SIG8(bus__DOT__mos6502__DOT__shift_right,0,0);
    };
    struct {
	VL_SIG8(bus__DOT__mos6502__DOT__alu_shift_right,0,0);
	VL_SIG8(bus__DOT__mos6502__DOT__HC,0,0);
	VL_SIG8(bus__DOT__mos6502__DOT__alu_out,7,0);
	VL_SIG8(bus__DOT__mos6502__DOT__carry_out,0,0);
	VL_SIG8(bus__DOT__mos6502__DOT__alu_N,0,0);
	VL_SIG8(bus__DOT__mos6502__DOT__alu_bcd,0,0);
	VL_SIG8(bus__DOT__mos6502__DOT__al_B,7,0);
	VL_SIG8(bus__DOT__mos6502__DOT__al_O_h,4,0);
	VL_SIG8(bus__DOT__mos6502__DOT__al_O_l,4,0);
	VL_SIG8(bus__DOT__mos6502__DOT__adder_HC,0,0);
	VL_SIG8(bus__DOT__rom__DOT__char,7,0);
	VL_SIG16(bus__DOT__cpu_addr,15,0);
	VL_SIG16(bus__DOT__bus_addr,15,0);
	VL_SIG16(bus__DOT__mos6502__DOT__pc,15,0);
	VL_SIG16(bus__DOT__mos6502__DOT__pc_mux,15,0);
	VL_SIG16(bus__DOT__mos6502__DOT__al_A,8,0);
	VL_SIG16(bus__DOT__mos6502__DOT__al_O,8,0);
	VL_SIG(bus__DOT__rom__DOT__nesfile,31,0);
	VL_SIG(bus__DOT__rom__DOT__i,31,0);
	VL_SIG(bus__DOT__rom__DOT__size,31,0);
	VL_SIG8(bus__DOT__mos6502__DOT__AXYS[4],7,0);
	VL_SIG8(bus__DOT__ram__DOT__data[4096],7,0);
	VL_SIG8(bus__DOT__rom__DOT__header[16],7,0);
	VL_SIG8(bus__DOT__rom__DOT__rpgrom[32768],7,0);
	VL_SIG8(bus__DOT__rom__DOT__chrrom[8192],7,0);
	VL_SIG8(bus__DOT__rom__DOT__rpgram__DOT__data[16384],7,0);
    };
    
    // LOCAL VARIABLES
    // Internals; generally not touched by application code
    VL_SIG8(bus__DOT____Vcellinp__mos6502__ready,0,0);
    VL_SIG8(__Vdly__bus__DOT__mos6502__DOT__state,7,0);
    VL_SIG8(__Vclklast__TOP__cpu_clock,0,0);
    VL_SIG8(__Vclklast__TOP__reset,0,0);
    VL_SIG8(__Vchglast__TOP__bus__DOT__mos6502__DOT__adder_HC,0,0);
    VL_SIG16(__Vtableidx1,9,0);
    static VL_ST_SIG8(__Vtable1_bus__DOT__mos6502__DOT__wr_reg[1024],0,0);
    
    // INTERNAL VARIABLES
    // Internals; generally not touched by application code
    Vbus__Syms* __VlSymsp;  // Symbol table
    
    // PARAMETERS
    // Parameters marked /*verilator public*/ for use by application code
    
    // CONSTRUCTORS
  private:
    VL_UNCOPYABLE(Vbus);  ///< Copying not allowed
  public:
    /// Construct the model; called by application code
    /// The special name  may be used to make a wrapper with a
    /// single model invisible with respect to DPI scope names.
    Vbus(const char* name="TOP");
    /// Destroy the model; called (often implicitly) by application code
    ~Vbus();
    
    // API METHODS
    /// Evaluate the model.  Application must call when inputs change.
    void eval();
    /// Simulation complete, run final blocks.  Application must call on completion.
    void final();
    
    // INTERNAL METHODS
  private:
    static void _eval_initial_loop(Vbus__Syms* __restrict vlSymsp);
  public:
    void __Vconfigure(Vbus__Syms* symsp, bool first);
  private:
    static QData _change_request(Vbus__Syms* __restrict vlSymsp);
  public:
    static void _combo__TOP__6(Vbus__Syms* __restrict vlSymsp);
    static void _combo__TOP__8(Vbus__Syms* __restrict vlSymsp);
  private:
    void _ctor_var_reset();
  public:
    static void _eval(Vbus__Syms* __restrict vlSymsp);
  private:
#ifdef VL_DEBUG
    void _eval_debug_assertions();
#endif // VL_DEBUG
  public:
    static void _eval_initial(Vbus__Syms* __restrict vlSymsp);
    static void _eval_settle(Vbus__Syms* __restrict vlSymsp);
    static void _initial__TOP__1(Vbus__Syms* __restrict vlSymsp);
    static void _sequent__TOP__2(Vbus__Syms* __restrict vlSymsp);
    static void _sequent__TOP__3(Vbus__Syms* __restrict vlSymsp);
    static void _sequent__TOP__5(Vbus__Syms* __restrict vlSymsp);
    static void _sequent__TOP__7(Vbus__Syms* __restrict vlSymsp);
    static void _settle__TOP__4(Vbus__Syms* __restrict vlSymsp);
} VL_ATTR_ALIGNED(128);

#endif // guard
