// Verilated -*- C++ -*-
// DESCRIPTION: Verilator output: Symbol table internal header
//
// Internal details; most calling programs do not need this header

#ifndef _Vmos6502__Syms_H_
#define _Vmos6502__Syms_H_

#include "verilated_heavy.h"

// INCLUDE MODULE CLASSES
#include "Vmos6502.h"

// SYMS CLASS
class Vmos6502__Syms : public VerilatedSyms {
  public:
    
    // LOCAL STATE
    const char* __Vm_namep;
    bool __Vm_didInit;
    
    // SUBCELL STATE
    Vmos6502*                      TOPp;
    
    // CREATORS
    Vmos6502__Syms(Vmos6502* topp, const char* namep);
    ~Vmos6502__Syms() {}
    
    // METHODS
    inline const char* name() { return __Vm_namep; }
    
} VL_ATTR_ALIGNED(64);

#endif // guard
