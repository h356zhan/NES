/*
0000 0000 0000 0000 - 0000 0111 1111 1111 RAM
0000 1000 0000 0000 - 0000 1111 1111 1111 RAM Mirror
0001 0000 0000 0000 - 0001 0111 1111 1111 RAM Mirror
0001 1000 0000 0000 - 0001 1111 1111 1111 RAM Mirror
0010 0000 0000 0000 - 0010 0000 0000 0111 PPU Reg
0010 0000 0000 1000 - 0011 1111 1111 1111 PPU Reg Mirror
0100 0000 0000 0000 - 0100 0000 0001 0111 APU and I/O
0100 0000 0001 1000 - 0100 0000 0001 1111 disabled
0100 0000 0010 0000 - 1111 1111 1111 1111 cartridge
*/

module bus (
    input ppu_clock,

    output ppu_reg_idx,
    output ppu_reg_wr_en,
    output ppu_reg_din,
    input  ppu_reg_dout,

    input  rom_ppu_wr_en,
    input  rom_ppu_addr,
    input  rom_ppu_din,
    output rom_ppu_dout,

    output dma_dout,
    output dma_en,

    input cpu_nmi,
    input cpu_ready,
    input cpu_clock,
    input reset
);

wire        reset;

wire        cpu_clock;
wire        cpu_ready;
wire        cpu_wr_en;
wire        cpu_irq;
wire        cpu_nmi;
wire [15:0] cpu_addr;
wire [15:0] bus_addr;
reg  [7:0] cpu_din;
wire [7:0] cpu_dout;

reg        ram_wr_en;
reg [11:0] ram_addr;
reg  [7:0] ram_din;
wire  [7:0] ram_dout;

wire        rom_cpu_wr_en;
wire [15:0] rom_cpu_addr;
wire  [7:0] rom_cpu_din;
wire  [7:0] rom_cpu_dout;

wire        rom_ppu_wr_en;
wire [15:0] rom_ppu_addr;
wire  [7:0] rom_ppu_din;
wire  [7:0] rom_ppu_dout;

wire       ppu_reg_wr_en;
wire [2:0] ppu_reg_idx;
wire [7:0] ppu_reg_din;
wire [7:0] ppu_reg_dout;

reg         dma_en;
reg   [7:0] dma_addr_hi;
reg   [7:0] dma_addr_lo;
wire  [7:0] dma_dout;

wire        ppu_clock;

wire [1:0] addr_mod;

assign bus_addr = dma_en ? {dma_addr_hi,dma_addr_lo} : cpu_addr;
assign dma_dout = addr_mod == 0? ram_dout : rom_cpu_dout;

always @(posedge cpu_clock) begin
    if (cpu_addr == 16'h4014 && cpu_wr_en) begin
        dma_en <= 1;
        dma_addr_hi <= cpu_dout;
        dma_addr_lo <= 0;
    end
    if (dma_en) begin
        if (dma_addr_lo == 8'hff) dma_en <= 0;
        dma_addr_lo <= dma_addr_lo + 1;
    end
end

assign addr_mod = (bus_addr[15:13] == 3'b000) ? 0 : // RAM
(bus_addr[15:13] == 3'b001) ? 1 :   //PPU Reg
2;  //ROM


always @(posedge cpu_clock)
    if (addr_mod == 0) cpu_din <= ram_dout;
    else if (addr_mod == 1) cpu_din <= ppu_reg_dout;
    else cpu_din <= rom_cpu_dout;

mos6502 mos6502(
    .clock(cpu_clock),
    .din(cpu_din),
    .ready(cpu_ready & ~dma_en),
    .irq(cpu_irq),
    .nmi(cpu_nmi),
    .reset(reset),
    .wr_en(cpu_wr_en),
    .dout(cpu_dout),
    .addr(cpu_addr)
);

assign ram_wr_en = addr_mod == 0? cpu_wr_en : 0;
assign ram_addr = bus_addr[11:0];
assign ram_din = cpu_dout;

memory #(
    .DATA_WIDTH(8),
    .ADDR_WIDTH(12)
)ram(
    .clock(cpu_clock),
    .wr_en(ram_wr_en),
    .addr(ram_addr),
    .din(ram_din),
    .dout(ram_dout)
);

assign ppu_reg_wr_en = addr_mod == 1? cpu_wr_en : 0;
assign ppu_reg_idx = bus_addr[2:0];
assign ppu_reg_din = cpu_dout;

assign rom_cpu_wr_en = addr_mod == 2? cpu_wr_en : 0;
assign rom_cpu_addr = bus_addr;
assign rom_cpu_din = cpu_dout;


rom rom(
    .cpu_clock(cpu_clock),
    .cpu_wr_en(rom_cpu_wr_en),
    .cpu_addr(rom_cpu_addr),
    .cpu_din(rom_cpu_din),
    .cpu_dout(rom_cpu_dout),

    .ppu_clock(ppu_clock),
    .ppu_wr_en(rom_ppu_wr_en),
    .ppu_addr(rom_ppu_addr),
    .ppu_din(rom_ppu_din),
    .ppu_dout(rom_ppu_dout)
);
/*
always @(posedge cpu_clock) begin
    $display("CPU: WE: %b addr: %04h DI: %04h DO:%04h", cpu_wr_en, cpu_addr, cpu_din, cpu_dout);
    $display("RAM: WE: %b addr: %04h DI: %04h DO:%04h", ram_wr_en, ram_addr, ram_din, ram_dout);
    $display("ROM: WE: %b addr: %04h DI: %04h DO:%04h", rom_cpu_wr_en, rom_cpu_addr, rom_cpu_din, rom_cpu_dout);
    for (int i = 0; i < 16; i ++)
        $write("%02h ", ram.data[16'h0200+i]);
    $write("\n");
end
*/
endmodule
