#include "Vbus.h"

#include <verilated.h>
#include "../ppu/ppu.hpp"
#include <iostream>
#include <stdlib.h>
#include <vector>

PPU ppu;

bool nmi_state;

std::vector <int> DMA(Vbus *nes) {
    std::vector <int> data;
    while (nes->dma_en) {
        nes->cpu_clock = 1; nes->eval();
        nes->cpu_clock = 0; nes->eval();
        data.push_back((int)nes->dma_dout);
        std::cerr << "DMA OUT " << (int)nes->dma_dout << std::endl;
    }
    std::cerr << "DMA size " << data.size() << std::endl;
    for (size_t i = 0; i < data.size(); i ++)
        std::cerr << std::hex << data[i] << " " << std::endl;
    std::cerr << std::endl;
    return data;
}

Vbus *nes;
int clk_phase = 0;

std::vector <uint8_t> dma_data, dma_buffer;

void tick(bool &set_nmi, bool &should_render) {
    clk_phase = (clk_phase + 1) % 6;
    if (clk_phase % 2 == 1) nes->ppu_clock = 1, ppu.cycle(set_nmi, should_render);
    else nes->ppu_clock = 0;
    if (clk_phase % 6 == 1) nes->cpu_clock = 1;
    if (clk_phase % 6 == 4) nes->cpu_clock = 0;
    nes->eval();
}

bool dma() {
    if (clk_phase != 4) return false;
    if (nes->dma_en) {
        dma_buffer.push_back(nes->dma_dout);
    } else if (dma_buffer.size()) {
        dma_data = dma_buffer;
        dma_buffer.clear();
        return true;
    }
    return false;
}

void set_nmi() {
	if (clk_phase % 6 != 1) return;
	nmi_state = 1;
	nes->cpu_nmi = true;
}

void reset_nmi() {
	if (clk_phase % 6 != 1) return;
	if (nmi_state) nes->cpu_nmi = 0;
}

void ppu_rom() {
	return;
	/*
    if (clk_phase % 2 != 1) return;
    if (want to write) {
        rom_ppu_wr_en = 1;
        rom_ppu_addr = ???;
        rom_ppu_din = ???;
    } else {    //read mode
        rom_ppu_wr_en = 0;
        rom_ppu_addr = ???;
        data will be available at rom_ppu_dout AFTER NEXT 2 tick(), i.e. ppu_clock = 1 again
    } 
	*/
}

void ppu_reg() {
	if (nes->ppu_reg_wr_en) ppu.write_ppu_ext_register(nes->ppu_reg_idx, nes->ppu_reg_din);
	else nes->ppu_reg_dout = ppu.read_ppu_ext_register(nes->ppu_reg_idx);
	//std::cout<<"Write to PPU reg "<<int(nes->ppu_reg_idx)<<" with data "<<nes->ppu_reg_din<<std::endl;
}

int main(int argc, char *argv[]) {
    Verilated::commandArgs(argc, argv);
    nes = new Vbus("nes");

	bool quit, change_nmi, do_render;
	nmi_state = 0;

    nes->cpu_clock = 1; nes->reset = 1; nes->eval();
    nes->cpu_clock = 0; nes->reset = 0; nes->cpu_ready = 1; nes->eval();

	for (int i = 0; ;i++ ) {
		change_nmi = do_render = quit = false;
		tick(change_nmi, do_render);

		if (change_nmi) {
			set_nmi();
		}

		ppu_reg();
		reset_nmi();
		if (dma()) {
			std::cerr << "DMA size: " << dma_data.size() << std::endl;
			for (size_t i = 0; i < dma_data.size(); i ++) {
				std::cerr << std::hex << (int)dma_data[i] << " ";
				ppu.write_ppu_ext_register(ppu.OAMDATA, dma_data[i]);
			}
			std::cerr << std::endl;

		}
		//std::cout<<i<<std::endl;
		if (do_render) {
			//std::cout<<"NEW FRAME"<<std::endl;	
			ppu.render_current(quit);
		}
	
		if (i>10000)
			i=0;
		if (quit)
			break;
	}

    delete nes;

	SDL_Quit();

    return 0;
}
