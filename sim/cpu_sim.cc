#include "Vmos6502.h"
#include <verilated.h>
#include <iostream>
#include <fstream>
#include <sstream>
#include <string>

uint8_t mem[0x10000];

void load_prog(char* filename) {
    std::ifstream fin(filename);
    std::string line;
    int pc = 0xc000;
    while (getline(fin, line)) {
        std::istringstream strin(line);
        int hexnum;
        while(strin >> std::hex >> hexnum) mem[pc ++] = hexnum;
    }
    fin.close();
}

void printmem() {
    for (int i = 0; i < 16; i ++) {
        for (int j = 0; j < 16; j ++) {
            printf("%02x ", mem[0x200+i*16+j]);
        }
        printf("\n");
        return;
    }
}

int main(int argc, char **argv) {
    Verilated::commandArgs(argc, argv);
    Vmos6502 *cpu;
    cpu = new Vmos6502("cpu");

    mem[0xfffc] = 0x00;
    mem[0xfffd] = 0xc0;

    if (argc > 1) load_prog(argv[1]);

    cpu->reset = 1;
    cpu->clock = 1;
    cpu->eval();

    cpu->clock = 0;
    cpu->eval();
    cpu->reset = 0;
    cpu->ready = 1;
    printf("READY\n");

    while (!Verilated::gotFinish()) {
        cpu->clock = 1;
        cpu->eval();

        cpu->clock = 0;
        cpu->eval();


        if (cpu->wr_en) {
            mem[cpu->addr] = cpu->dout;
            printf("W %02x -> [%04x]\n", cpu->dout, cpu->addr);
        } else {
            cpu->din = mem[cpu->addr];
            printf("R %02x <- [%04x]\n", mem[cpu->addr], cpu->addr);
        }

        printmem();
        sleep(1);
    }

    delete cpu;
}
