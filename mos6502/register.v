always @(posedge clock) begin
    if (ready && wr_reg)
        AXYS[reg_idx] <= (state == JSR0) ? din_mux : {alu_out[7:4]+adj, alu_out[3:0]+adj};

    if (ready && state == DECODE)
        casex (ir)
            8'b0xx01010,
            8'b0xxxxx01,
            8'b10100xx0,8'b10101010,8'b10101100,8'b10101110,
            8'b10111010,
            8'b10001010,
            8'b10011000,
            8'b10011010,
            8'b1011x1x0,
            8'b11001010,
            8'b1x1xxx01,
            8'bxxx01000: ld_reg <= 1;
            default: ld_reg <= 0;
        endcase
end

always @*
    case (state)
        DECODE: wr_reg = ld_reg & ~plp;
        PL1,RTS2,RTI3,BRK3,JSR0,JSR2: wr_reg = 1;
        default wr_reg = 0;
    endcase


