module rom (
    input  wire        cpu_clock,
    input  wire        cpu_wr_en,
    input  wire [15:0] cpu_addr,
    input  wire  [7:0] cpu_din,
    output wire  [7:0] cpu_dout,

    input  wire        ppu_clock,
    input  wire        ppu_wr_en,
    input  wire [15:0] ppu_addr,
    input  wire  [7:0] ppu_din,
    output wire  [7:0] ppu_dout
);

reg [7:0] header[0:15];
reg [7:0] char;
integer nesfile, status, i, size;

reg  [7:0] rpgrom[0:2**15-1];
wire [7:0] rpgrom_dout;
wire [7:0] rpgram_dout;

reg  [7:0] chrrom[0:2**13-1];
wire [7:0] chrrom_dout;

memory #(
    .DATA_WIDTH(8),
    .ADDR_WIDTH(14)
) rpgram (
    .clock(cpu_clock),
    .wr_en(cpu_wr_en & ~cpu_addr[15]),
    .addr(cpu_addr[13:0]),
    .din(cpu_din),
    .dout(rpgram_dout)
);

assign rpgrom_dout = (header[4] == 2)? 
rpgrom[cpu_addr[14:0]] : rpgrom[{1'b0, cpu_addr[13:0]}];

assign cpu_dout = cpu_addr[15]  ? rpgrom_dout : rpgram_dout;

/*
always @(posedge cpu_clock) 
    if (cpu_addr[15]) cpu_dout <= rpgrom_dout;
    else cpu_dout <= rpgram_dout;
*/

assign chrrom_dout = chrrom_dout[ppu_addr[12:0]];

initial begin
    nesfile = $fopen("rom.nes", "rb");
    for (i = 0; i < 16; i ++) begin
        char = $fgetc(nesfile);
        header[i] = char;
    end
    $display("RPG ROM size: %d*16kb", header[4]);
    $display("CHR ROM size: %d*8kb", header[5]);
    size = (2**14) * header[4];
    for (i = 0; i < size; i ++) begin
        char = $fgetc(nesfile);
        rpgrom[i] = char;
    end
    $display("MMI    : %h%h", rpgrom[16384*header[4]-5],rpgrom[16384*header[4]-6]);
    $display("Reset  : %h%h", rpgrom[16384*header[4]-3],rpgrom[16384*header[4]-4]);
    $display("IRQ/BRK: %h%h", rpgrom[16384*header[4]-1],rpgrom[16384*header[4]-2]);
    size = (2**13) * header[5];
    for (i = 0; i < size; i ++) begin
        char = $fgetc(nesfile);
        chrrom[i] = char;
    end
end

endmodule
